import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Injectable} from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = window.localStorage.getItem('token');
        let nodeToken = window.localStorage.getItem('nutoken');
        if (token) {
          if(nodeToken){
            request = request.clone({
              setHeaders: {
                utoken:  token,
                nutoken:nodeToken
              }
            });
          }else {
          request = request.clone({
            setHeaders: {
              utoken:  token
            }
          });
          }
        }
        return next.handle(request);
      }
    
}
