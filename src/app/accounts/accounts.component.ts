import { Component, OnInit, ViewChild, ElementRef, Renderer, OnDestroy, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute  } from "@angular/router";
import { GridOptions, CellClickedEvent } from "ag-grid-community";
import { AccountApiService } from "src/services/accounts.services";
import { ContactApiService } from "../../services/contacts.services";
import { AccountsActionsComponent } from "../custom_components/accounts-actions/accounts-actions.component";
import { UserService } from "src/services/userservice.services";
import { teammanagementservice } from "src/services/teamsmanagement.services";
import { AuthService } from "../guards/authguard/auth.service";
import { UtilitiesService } from "src/services/utilities.services";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFieldsService } from 'src/services/commonfields.services';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: "app-accounts",
  templateUrl: "./accounts.component.html",
  styleUrls: ["./accounts.component.css"]
})

export class AccountsComponent implements OnInit, OnDestroy  {
 
  accountForm: FormGroup;
  contactForm: FormGroup;
  submitted = false;
  accounts: any;
  Users: any;
  selectedOption: any;
  selectedAccount: any;
  accountResponseInfo:string;
  accountid: any;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  public stageInfo: any;
  selectedStage: string;
  usersData: any;
  selectedOwnerId: string;  
  selectedDesignationId:number;
  selectedDesignationCat:string;
  designationCategoryList:any;
  getContactsData:any;
  loggedInUserRole: string;
  mainTab:string = "engaged";
  engSelect:string = "thisweek";
  crdSelect :string = "thisweek";
  stgSelect:string = "all";
  IsMember: boolean;
  userInfo: any;
  selectedIndustryId:string;
  selIndustryInfo:any;
  selectedSource:string;
  IndustriesList:any;
  designationsList:any;
  accountIntId:number;
  subscriptions: Subscription [] = [];
  industryintiderror:boolean = false;
  loading = false;
  selectedType:string;

  get f() {
    return this.accountForm.controls;
  }
  @ViewChild("closeAddAccountModal", { static: false })
  closeAddAccountModal: ElementRef;
  infoModalRef: BsModalRef;
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;
  get cf() {
    return this.contactForm.controls;
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: AccountApiService,
   // private modalService: NgbModal,
    private modalService: BsModalService,
    private contactsService: ContactApiService,
    private userService: UserService,
    private teamService: teammanagementservice,
    private auth: AuthService,
    private render: Renderer,
    private spinner: NgxSpinnerService,
    public utilities: UtilitiesService,
    private route:ActivatedRoute,
    private commonService:CommonFieldsService
  ) {
    this.sideBar = {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ],
      defaultToolPanel: ""
    };

    this.columnDefs = [
      {
        headerName: "ID",
        field: "Id",
        hide: true
      },
      {
        headerName: "Account Name",
        field: "AccountName",
      //  checkboxSelection: true,
        cellRenderer: function(params) {
          return (
            '<a href="accounts/viewaccount/' +
            params.data.Id +
            '""target="_blank">' +
            params.value +
            "</a>"
          );
        }
      },
      {
        headerName: "Industry",
        field: "industry"
      },
      {
        headerName: "Stage",
        field: "Stage"
      },
      // {
      //   headerName: "Email",
      //   field: "Email",
      // },
      // {
      //   headerName: "Contact No",
      //   field: "mobilenumber",

      // },
      {
        headerName: "Last Contacted Date",
        field: "LastContactedDate"
      },
      {
        headerName: "Last Contact Type",
        field: "LastContactType"
      },
      {
        headerName: "No Of Contacted",
        field: "NoOfContacted"
      },{
        headerName: "Assigned To",
        field: "AssigneeName"
      },
      {
        headerName: "Created On",
        field: "SyncDate"
      },
      {
        headerName: "Actions",
        pinned: "right",
        //  width: 250,
        editable: false,
        cellStyle: { "padding-top": "2px" },
        cellRendererFramework: AccountsActionsComponent
      }
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      //width: 200,
      field: "accountname",
      valueGetter: function(params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true
      //  width: 250
    };
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "false";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }else {
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }
  }
  gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52
  };
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);
    }
  }
  StageChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();
   let accountsSubscription = this.apiService.getAccounts().subscribe(data => {
      let accountsMasterData = JSON.parse(data.Data);
      this.accounts = accountsMasterData.ListOfAccounts;
      if (selectedValue == "all") {
        this.rowData = this.accounts;
      } else {
        this.rowData = this.accounts.filter(a => a.Stage.toLowerCase() == selectedValue);
      }
    });
    if(accountsSubscription){
      this.subscriptions.push(accountsSubscription);
    }
  }

  EngagedChangedHandler(selectedValue:string){
    selectedValue = selectedValue.toLowerCase();    
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(); 
        if (selectedValue == "all") {
          this.rowData = this.accounts;
          
        }else if(selectedValue == "today") {
          let currentDate = new Date();
          let todayDate = this.utilities.dateConvertion(currentDate);      
          
          this.rowData = this.accounts.filter(
            i=>i.LastContactedDate === todayDate
          );
        }else if(selectedValue == "thisweek") {
          var thisWeekDates= this.utilities.thisWeekDates();          
          let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
          let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
          this.rowData = this.accounts.filter(
            i=>new Date(i.LastContactedDate) >= new Date(thisWeekStartDate) && new Date(i.LastContactedDate) <= new Date(thisWeekEndDate)
          );
        } else if(selectedValue == "lastweek") {
          var lastWeekDates= this.utilities.lastWeekDates();          
          let lastWeekStartDate = this.utilities.dateConvertion(lastWeekDates.startDate);
          let lastWeekEndDate = this.utilities.dateConvertion(lastWeekDates.endDate); 
          this.rowData = this.accounts.filter(
            i=>new Date(i.LastContactedDate) >= new Date(lastWeekStartDate) && new Date(i.LastContactedDate) <= new Date(lastWeekEndDate)
          );
        }else if (selectedValue === "thismonth"){
          var currentMonthFirstDay = new Date(y, m, 1);
          var currentMonthLastDay = new Date(y, m + 1, 0);
          var currentMonthStartDate = this.utilities.dateConvertion(currentMonthFirstDay);
          var currentMonthEndDate = this.utilities.dateConvertion(currentMonthLastDay);
          this.rowData = this.accounts.filter(
            i=>new Date(i.LastContactedDate) >= new Date(currentMonthStartDate) && new Date(i.LastContactedDate) <= new Date(currentMonthEndDate)
          );
        }else if (selectedValue === "lastmonth"){
          var lastMonthFirstDay = new Date(y, m-1, 1);
          var lastMonthLastDay = new Date(y, m, 0);
          let lastMonthStartDate = this.utilities.dateConvertion(lastMonthFirstDay);
          let lastMonthEndDate = this.utilities.dateConvertion(lastMonthLastDay); 
     
          this.rowData = this.accounts.filter(        
            i=>new Date(i.LastContactedDate) >= new Date(lastMonthStartDate) && new Date(i.LastContactedDate) <= new Date(lastMonthEndDate)
          );
        }else if (selectedValue === "last3months"){
          var last3MonthFirstDay = new Date(y, m-3, 1);
          var last3MonthLastDay = new Date(y, m, 0);
          let last3MonthStartDate = this.utilities.dateConvertion(last3MonthFirstDay);
          let last3MonthEndDate = this.utilities.dateConvertion(last3MonthLastDay); 
     
          this.rowData = this.accounts.filter(       
            i=> new Date(i.LastContactedDate) < new Date(last3MonthStartDate) 
          );
        } else if(selectedValue == "notengaged") {         
          this.rowData = this.accounts.filter(
            i=>i.LastContactedDate === "-"
          );
        }
  }

  mainFilterTabsChanged(clickedText) {
    clickedText = clickedText.toLowerCase();
    if(clickedText === "all"){
      this.stgSelect = "all";
      this.rowData = this.accounts;
    }else{
      var thisWeekDates= this.utilities.thisWeekDates();          
      let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
      let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
       if(clickedText === "thisweek-created"){ 
        this.crdSelect = "thisweek";
      this.rowData = this.accounts.filter(        
        i=>new Date(i.SyncDate) >= new Date(thisWeekStartDate) && new Date(i.SyncDate) <= new Date(thisWeekEndDate)
      );
    }else{
      this.engSelect = "thisweek";
      this.rowData = this.accounts.filter(
        i=>new Date(i.LastContactedDate) >= new Date(thisWeekStartDate) && new Date(i.LastContactedDate) <= new Date(thisWeekEndDate)
      );
    }
  }
  }
  

  CreatedChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();   
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(); 
        if (selectedValue == "all") {
          this.rowData = this.accounts;
          
        }else if (selectedValue === "thismonth"){
          var currentMonthFirstDay = new Date(y, m, 1);
          var currentMonthLastDay = new Date(y, m + 1, 0);
          var currentMonthStartDate = this.utilities.dateConvertion(currentMonthFirstDay);
          var currentMonthEndDate = this.utilities.dateConvertion(currentMonthLastDay);
          this.rowData = this.accounts.filter(
            i=>new Date(i.SyncDate) >= new Date(currentMonthStartDate) && new Date(i.SyncDate) <= new Date(currentMonthEndDate)
          );
        }else if (selectedValue === "thisweek"){
          var thisWeekDates= this.utilities.thisWeekDates();          
          let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
          let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
     
          this.rowData = this.accounts.filter(        
            i=>new Date(i.SyncDate) >= new Date(thisWeekStartDate) && new Date(i.SyncDate) <= new Date(thisWeekEndDate)
          );
        } else if(selectedValue == "lastweek") {
          var lastWeekDates= this.utilities.lastWeekDates();          
          let lastWeekStartDate = this.utilities.dateConvertion(lastWeekDates.startDate);
          let lastWeekEndDate = this.utilities.dateConvertion(lastWeekDates.endDate); 
          this.rowData = this.accounts.filter(
            i=>new Date(i.SyncDate) >= new Date(lastWeekStartDate) && new Date(i.SyncDate) <= new Date(lastWeekEndDate)
          );
        }else if (selectedValue === "today"){
          let currentDate = new Date();
          let todayDate = this.utilities.dateConvertion(currentDate);      
          this.rowData = this.accounts.filter(        
            i=>i.SyncDate === todayDate
          );
        }     
    
  }

  industryChangeHandler(selectedObject:any, actionFrom){
    let industryArray =[];
       if(selectedObject || selectedObject.term.length >= 3 ){
         let searchString = '';
         if(selectedObject.term && selectedObject.term.length >= 3 && actionFrom=='search'){
          searchString = selectedObject.term;
         }else if(selectedObject && actionFrom=='update') {
          searchString = selectedObject;
         }
       let industrysSub = this.commonService.getCommonFields(1,searchString).subscribe(data => {
        if(data.Status == "Success"){
         let industries = JSON.parse(data.Data);
         industries.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };       
            industryArray.push(singleItem);
        });
        this.IndustriesList = industryArray; 
          if(actionFrom=='update')   {
            this.selIndustryInfo = industryArray[0];
          }   
        }
       });
       this.subscriptions.push(industrysSub);
        }   
    }
    selectTypeChangeHandler(selectedValue: string) {  
      this.selectedType = selectedValue;    
    }

  selectStageChangeHandler(selectedValue: string) {  
    this.selectedStage = selectedValue;
  }

  selectIndustry(){  
    console.log(this.selIndustryInfo);
   // this.accountUpdateForm.markAsDirty();
    this.industryintiderror = false;
    console.log(new Date);
  }

  selectIndustryChangeHandler(selectedValue: string) {  
   this.selectedIndustryId = selectedValue;
  }
  selectSourceChangeHandler(selectedValue: string) {  
    this.selectedSource = selectedValue;
  }

  sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }

  selectOwnerChangeHandler(selectedValue: string) {
    this.selectedOwnerId = selectedValue;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // this.gridOptions.api.sizeColumnsToFit(); // newly added for table full width
    this.getAccounts();
  }
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    this.getIndustriesList();
    this.getDesignationsList();
    this.getDesignationCategoryList();
    if (!window.localStorage.getItem("token")) {
      this.router.navigate(["login"]);
    }
   let teamsSubscription = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
   if(teamsSubscription){
    this.subscriptions.push(teamsSubscription);
   }

    this.accountForm = this.formBuilder.group({
      accountname: ["", Validators.compose([Validators.required])],
      industryid:[this.selectedIndustryId,Validators.compose([Validators.required])],
      subindustry: [""],
      accountsource:[this.selectedSource],
      website: ["",
      [
         Validators.required, 
         Validators.pattern("[www]+\.([a-z.]{2,6})[/\\w .-]*/?")
        // Validators.pattern("([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")
       ]],
      companylinkedinurl: [""],
      email: [
        "",
        [
         // Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      stage: [this.selectedStage,Validators.compose([Validators.required])],
      AssignedToIntId: ["", Validators.required],
      AssignedTeamIntId: [],
      TeamId: [],
      noofemployees:[],
      revenue:[],
      companyaddress:[]
    });

    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],
      TeamId: [],
      designationid:["",this.selectedDesignationId],
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: []
    });

    this.route.params.subscribe(params=>{
      let dashboardFilter = params['type'];
      if(dashboardFilter != undefined && dashboardFilter != ""){
        switch (dashboardFilter) {
          case 'en-thisweek' || undefined:
            this.engSelect = "thisweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thisweek");
            break;
          case 'en-thismonth':
            this.engSelect = "thismonth";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thismonth");
            break;
          case 'en-today':
            this.engSelect = "today";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("today");
            break;
          case 'en-lastweek':
            this.engSelect = "lastweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("lastweek");
            break;
          case 'cr-today':
            this.crdSelect = "today";
            this.mainTab = 'created';
            this.CreatedChangedHandler("today");
            break;
          case 'cr-thisweek':
            this.crdSelect = "thisweek";
            this.mainTab = 'created';
            this.CreatedChangedHandler("thisweek");
            break;
          case 'cr-lastweek':
            this.crdSelect = "lastweek";
            this.mainTab = 'created';
            this.CreatedChangedHandler("lastweek");
            break;     
          case 'cr-thismonth':
            this.crdSelect = "thismonth";
            this.mainTab = 'created';
            this.CreatedChangedHandler("thismonth");
            break;     
          default:
            this.engSelect = "thisweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thisweek");
            break;
        }
      }
     });

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  

  getIndustriesList(){
    let commonSubscribtion = this.commonService.getCommonFields(1,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.IndustriesList = JSON.parse(data.Data);
      }
     });
     if(commonSubscribtion){
      this.subscriptions.push(commonSubscribtion);
     }
  }

  getDesignationsList(){
    this.commonService.getCommonFields(2,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationsList = JSON.parse(data.Data);
      }
     });
  }

  getDesignationCategoryList(){
    this.commonService.getCommonFields(4,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationCategoryList = JSON.parse(data.Data);
      }
     });
  }

  addAccount(template: TemplateRef<any>) {
    
    this.submitted = true;  
    if (this.accountForm.invalid) {
      return;
    } 
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.accountForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.accountForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    
    this.apiService.AddAccount(this.accountForm.value).subscribe(data => {
      if (data.Status === "Success") {       
        this.getAccounts();
      } else if(data.Code == "FAL-409"){
       this.accountResponseInfo = "<h6>Account already exists..!</h6>";
        this.infoModalRef = this.modalService.show(template, { class: "modal-m" });
      }
      this.onReset();
      this.closeAddAccountModal.nativeElement.click();
   });
  }

  
onCellClicked($event: CellClickedEvent) {
  if($event.colDef.headerName == "Actions"){
  this.accountIntId = $event.data.Id;
  }
}

selectDesignationChangeHandler(selectedValue: number) {  
  this.selectedDesignationId = selectedValue;
 }

  addContact() {
    this.submitted = true;

    if (this.contactForm.invalid) {
      return;
    }

    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.contactForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.contactForm.patchValue({ accountintid: this.accountIntId });

    if (this.accountIntId != null) {
      this.contactsService
        .AddContact(this.contactForm.value)
        .subscribe(data => {
          if (data.Status === "Success") {
            this.closeAddContactModal.nativeElement.click();          
            this.onContactReset();
           this.router.navigate(['/accounts/viewaccount/'+this.accountIntId]);
          }
        });
    }
  }

  onContactReset() {
    this.submitted = false;
    this.contactForm.reset();
  }

  getAccounts() {
   let accountSubscription = this.apiService.getAccounts().subscribe(data => {
     if(data.Status == "Success"){
      let accountsMasterData = JSON.parse(data.Data);
      this.accounts = accountsMasterData.ListOfAccounts;
      if(this.accounts != null){
     var thisWeekDates= this.utilities.thisWeekDates();          
     let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
     let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
     this.rowData = this.accounts.filter(
       i=>new Date(i.LastContactedDate) >= new Date(thisWeekStartDate) && new Date(i.LastContactedDate) <= new Date(thisWeekEndDate)
     );
      this.stageInfo = accountsMasterData.ListOfAccountsCountInfo;
     }else{
      this.rowData = [];
      this.stageInfo = [];
     }
    }else{
      this.rowData = [];
      this.stageInfo = [];
    }
    });    
  //  this.subscriptions.push(accountSubscription);   
  
  }

  onReset() {
    this.submitted = false;
    this.accountForm.reset();
  } 
  
}
