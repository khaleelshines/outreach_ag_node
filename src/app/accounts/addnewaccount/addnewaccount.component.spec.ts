import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewaccountComponent } from './addnewaccount.component';

describe('AddnewaccountComponent', () => {
  let component: AddnewaccountComponent;
  let fixture: ComponentFixture<AddnewaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnewaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
