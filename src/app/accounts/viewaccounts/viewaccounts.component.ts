import { Component, OnInit, OnDestroy, ElementRef, ViewChild, TemplateRef } from '@angular/core';
import { AccountApiService } from 'src/services/accounts.services';
import { Router, ActivatedRoute } from '@angular/router';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { NotesApiService } from 'src/services/notes.services';
import { CallsApiService } from 'src/services/calls.services';
import { TaskApiService } from 'src/services/taskservice.services';
import { UtilitiesService } from 'src/services/utilities.services';
import { Subscription } from 'rxjs';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactApiService } from 'src/services/contacts.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import Froalaeditor from 'froala-editor';
import { templateService } from 'src/services/templates.services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { Location } from '@angular/common';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { MeetingApiService } from 'src/services/meeting.services';
import { UserService } from 'src/services/userservice.services';
import { EmailApiService } from 'src/services/emails.services';
import { stringify } from 'querystring';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppSettings } from 'src/app/shared/app.settings';

@Component({
  selector: 'app-viewaccounts',
  templateUrl: './viewaccounts.component.html',
  styleUrls: ['./viewaccounts.component.css'],
  providers: [ContactsComponent, SequenceComponent],
})
export class ViewaccountsComponent implements OnInit,OnDestroy {
  contactForm: FormGroup;
  submitted = false;
  notesForm: FormGroup;
  callsForm: FormGroup;
  MeetingsForm: FormGroup;
  id: any;
  activityresponse: any;
  accountId: any;
  accountEmail: any;
  notescount: any;
  callscount: any;
  taskscount: any;
  emailscount: any;
  meetingscount: any;
  notes: any;
  calls: any;
  activeTab: any = 'contacts';
  tasks: any;
  emails: any;
  meetingList: any;
  designationsList:any;  
  selDesigInfo:any;
  selectedDesignationCat:string;
  designationCategoryList:any;
  usersData:any;
  invitiesusersData:any;
  selectedStage: string;
  selectedDesignationId:number;
  getContactsData:any;
  loggedInUserRole: string;
  responseInfo:string;
  IsMember: boolean;
  userInfo: any;
  emailOptions:any;
  templates: any;
  nonFilteredTemplates: any;
  ExistingTemplatesModal: BsModalRef;
  mailResponseTemplate:BsModalRef;
  contactEmail:string;
  contactId:string;
  modalResponseRef:BsModalRef;  
  accountContactsList:any;
   //#region dropdown list declaration
   campaigntypeList:any;
   selectedCampaign:string;
   //#endregion
  subscriptions:Subscription [] = [];
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;
  @ViewChild('closeSendEmailModal', { static: false }) closeSendEmailModal: ElementRef;
  ManualEmailForm:FormGroup;
  ErrorMessageInfo:string;
    //#region Desgination declaration
    IsNewDesignChecked:boolean = false;
    IsDesignExists:boolean = false;
    IsContactExists:boolean = false; 
    //#endregion
  loading:boolean = false;

  //#region add task declaration
  TaskForm: FormGroup;
  selContactInfo:any;
  clickedId = "";
  //#endregion  

  //#region meetings
  meetingId:number;
  //#endregion
  designationArr:any = [];
  accountNameArr:any = [];
  
  meetingtime: Date = new Date();
  Hours: any;
  Mins: any;
  Days: any;
  NoInvitees:boolean = false;
  selectedUser: any;  
  bsValue = new Date();
  monthDays = [];
  hourMins = [];
  dayHours = [];
  get nf() { return this.notesForm.controls; }
  get cf() { return this.callsForm.controls; }
  get mf() { return this.MeetingsForm.controls; }
  @ViewChild('closeAddNotesModal', { static: false }) closeAddNotesModal: ElementRef;
  @ViewChild('closeAddCallsModal', { static: false }) closeAddCallsModal: ElementRef;
  @ViewChild('closeMeetingsModal', { static: false }) closeMeetingsModal: ElementRef;

  get ef() { return this.ManualEmailForm.controls; }
  get f() { return this.contactForm.controls; }

  constructor(private accountService: AccountApiService, private route: ActivatedRoute,
    public utilities: UtilitiesService,
    private teamService: teammanagementservice,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private contactsService: ContactApiService,
    private templateApiService:templateService,
    private modalService: BsModalService,
    private notesService: NotesApiService, 
    private callsService: CallsApiService,
     private taskService: TaskApiService,
     private sequenceService: SequenceApiService,
     private modalBsService: BsModalService,
    private commonfieldsService:CommonFieldsService, 
    private meetingService: MeetingApiService,
    private emailService:EmailApiService,
    private spinner: NgxSpinnerService,
    private location: Location,
    private appsettings:AppSettings) {
      this.loading = false;
      this.monthDays = new Array(30).fill(1).map((x,i)=>i);
      this.dayHours = new Array(24).fill(1).map((x,i)=>i);
      this.hourMins = new Array(59).fill(1).map((x,i)=>i);

      this.loggedInUserRole = this.auth.getRole();
      if (this.loggedInUserRole === "member") {
        this.IsMember = true;
        let userDetails = localStorage.getItem("userInfo");
        let userDetails1 = JSON.parse(userDetails);
        this.userInfo = {
          name: userDetails1.Name,
          IntUserId: userDetails1.IntUserId,
          companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
          companyId :userDetails1.CompanyInfo[0].CompanyId
        };
      }else {
        let userDetails = localStorage.getItem("userInfo");
        let userDetails1 = JSON.parse(userDetails);
        this.userInfo = {
          name: userDetails1.Name,
          IntUserId: userDetails1.IntUserId,
          companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
          companyId :userDetails1.CompanyInfo[0].CompanyId
        };
      }
     }

  ngOnInit() {
    this.notescount = 0;
    this.callscount = 0;
    this.taskscount = 0;
    this.emailscount = 0;
    this.meetingscount = 0;

    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],
      TeamId: [],
      designationid:[],
      designation:[],
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: [],
      campaigntype:["",this.selectedCampaign]
    });
    this.ManualEmailForm = this.formBuilder.group({
      To: [],
      CC: [],
      Subject: ['', Validators.required],
      HtmlBody: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      SequenceDetailId:[],
      ContactId:[],
      BCC:[]
    });

    //#region  Create task form validation
    
    this.TaskForm = this.formBuilder.group({
      ObjectId: [],
      Priority: [],
      Title: [],
      AssignedTo: [],
      DueDate: [],
      DueTime: [],
      Description: [],
      AccountId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      ObjectIntId: [],
      AssignedToIntId: []
    });

    this.notesForm = this.formBuilder.group({
      Description: ['',Validators.compose([Validators.required])],
      ObjectId: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });
    this.callsForm = this.formBuilder.group({
      Description: [],
      Title: ['',Validators.compose([Validators.required])],
      ObjectId: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });

    this.MeetingsForm = this.formBuilder.group({
      Invitees:  ['',Validators.compose([Validators.required])],
      Title:  ['',Validators.compose([Validators.required])],
      Description: [],
      ObjectId: [],
      RemainderDate: [],
      ObjectIntId: ['',Validators.compose([Validators.required])],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      Days: [],
      Hours: [],
      Mins: [],
      Date: []
    });
    
    this.route.params.subscribe(params => {
      this.id = params["id"];
      this.clickedId = this.id;
      this.spinner.show();
    let accountSubscription =  this.accountService.getAccountActivityCount(this.id).subscribe(data => {
        if (data.Status === 'Success') {
          this.activityresponse = JSON.parse(data.Data);
         this.getContactsData = this.activityresponse.ContactList;

         let contactsArray = [];          
          this.getContactsData.forEach(element => {
            let singleItem: any;
            singleItem = {
              id: element.id,
              name: element.contactname,
              accountid: this.id
            };
            contactsArray.push(singleItem);
          });
          this.accountContactsList = contactsArray;
          this.accountEmail = this.activityresponse.AccountInfo.email;
          this.accountId = this.activityresponse.AccountInfo.contactid;
          for (var singleActivityResponse of this.activityresponse.ActivityCountInfo) {
            if (singleActivityResponse.ContactType == "notes") {
              this.notescount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.ContactType == "calls") {
              this.callscount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.ContactType == "tasks") {
              this.taskscount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.ContactType == "emails") {
              this.emailscount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.ContactType == "meetings") {
              this.meetingscount = singleActivityResponse.Count;
            }
          }
        }
      });
      this.subscriptions.push(accountSubscription);
      this.spinner.hide();
    });

    let teamSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
    this.subscriptions.push(teamSub);
    let userSubscription = this.userService.getUsers1(this.userInfo)
    .subscribe(data => {
      this.invitiesusersData = JSON.parse(data.Data);
    });
  this.subscriptions.push(userSubscription);
   // this.getDesignationsList();
    this.getDesignationCategoryList();
    this.getCampaignTypeList();

    Froalaeditor.DefineIcon('custom-field', { NAME: 'cog', SVG_KEY: 'cogs' });
    Froalaeditor.RegisterCommand('custom-field', {
      title: 'Personalize',
      type: 'dropdown',
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        '{{Contact: First Name}}': 'First Name',
        '{{Contact: Last Name}}': 'Last Name',
        '{{Contact: Designation}}': 'Designation',
        '{{Contact: PhoneNumber}}': 'PhoneNumber',
        "{{Account: AccountName}}": "AccountName",
      },
      callback: function (cmd, val) {
        this.html.insert(val);
      }
    });

    this.emailOptions = this.appsettings.editorOptions;

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  OpenExistingTemplates(template: TemplateRef<any>) {
    this.getTemplates();
    this.ExistingTemplatesModal = this.modalService.show(template, { class: 'modal-lg' });
  }
  sendEmailBtn(clickedEmail:string,clickedContactId){
 this.contactEmail = clickedEmail;
 this.contactId = clickedContactId;
  }

  sendManualEmail(template: TemplateRef<any>) { 
    this.responseInfo = '';  
    this.submitted = true;    
    if (this.ManualEmailForm.invalid) {
      return;
    }
    this.loading = true;
    this.ManualEmailForm.value.To = this.contactEmail;
    let ccList = this.ManualEmailForm.value.CC;
    let ccArray = [];
    if (ccList != "") {
      ccArray.push(ccList);
    }
    this.ManualEmailForm.value.CC = ccArray;
    this.ManualEmailForm.value.ContactId = this.contactId;
    this.ManualEmailForm.value.BCC = [];
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.ManualEmailForm.value.AssignedTeam = TeamInfo.TeamId;
    this.ManualEmailForm.value.AssignedTeamIntId = TeamInfo.Id; 
    this.ManualEmailForm.value.SequenceDetailId = 0;

    this.sequenceService.sendManualEmailToContact(this.ManualEmailForm.value).subscribe(data => {
      this.loading = false;
      this.closeSendEmailModal.nativeElement.click();
      if (data.Code === 'SUC-200') {   
        this.ErrorMessageInfo = "Email sent successfully..!";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });
   
        this.getEmails();
        
      }else if(data.Code === "SUC-307"){
        this.ErrorMessageInfo = "This contact is unsubscribed mails from Outreach";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });

      }else if(data.Code === "E-404"){
        this.ErrorMessageInfo = "Email address not found";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });
        
      }
      this.onReset();
     
    });
  }

  SelectTemplate(template: TemplateRef<any>, templateId: string) {
    let templatesList = this.nonFilteredTemplates;
    let singleTemplateData = templatesList.filter(a => a.TemplateId === templateId);
    let formFields = { Subject: singleTemplateData[0].Subject, HtmlBody: singleTemplateData[0].HtmlTemplateBody};

    this.ManualEmailForm.patchValue(formFields);
    this.ExistingTemplatesModal.hide();
  }

  SearchTemplateData(event: any) {
    let searchTerm = event.target.value;
    if (searchTerm.length >= 2) {
      this.templates = this.nonFilteredTemplates.filter(a => {
        const term = searchTerm.toLowerCase();
        return a.TemplateName.toLowerCase().includes(term)
          || a.Subject.toLowerCase().includes(term)
          || a.ElapsedDuration.toLowerCase().includes(term);
      });
    } else {
      this.templates = this.nonFilteredTemplates;
    }
  }

  clearSearch(event: any) {
    if (event.type === "mouseup") {
      this.templates = this.nonFilteredTemplates;
    }
  }

  getTemplates(): any {
    this.templateApiService.getTemplates()
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.templates = JSON.parse(data.Data);
          this.nonFilteredTemplates = this.templates;
        } else {
          this.templates = undefined;
        }
      });
  }

  getDesignationCategoryList(){
    this.commonfieldsService.getCommonFields(4,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationCategoryList = JSON.parse(data.Data);
      }
     });
  }

  getDesignationsList(){
    this.commonfieldsService.getCommonFields(2,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationsList = JSON.parse(data.Data);
      }
     });
  }
  
  resetdesigs(){
    this.designationsList = []; 
    this.selDesigInfo = null;
  }

    //#region contact checking
    focusOutEmailTxt(event:any,template: TemplateRef<any>){
      let emailValue = event.target.value;
      if(emailValue != "" && emailValue != undefined){
        this.IsContactExists = false;
        this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let isContactExisting = JSON.parse(data.Data);
              if(parseInt(isContactExisting) > 0){ 
                this.IsContactExists = true;            
                this.responseInfo = `<div class="alert alert-danger mb-0">     
                  <h6>Email already exist. Please try with another..!</h6> 
                </div> `; 
                this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
                setTimeout(() => {
                  this.modalResponseRef.hide();
                }, 3000);
              }
          }
        });
      }
    }
     //#endregion

  //#region  common dropdowns
  getCampaignTypeList(){
    this.commonfieldsService.getCommonFields(3,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.campaigntypeList = JSON.parse(data.Data);
      }
    });
  }

  compTypeChangeHandler(selectedValue: string) {
    this.selectedCampaign = selectedValue;    
  }
  //#endregion

  sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }

   
  addNotes() {
    this.submitted = true;
    if (this.notesForm.invalid) {
      return;
    }
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.notesForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.notesForm.patchValue({ ObjectIntId: this.activityresponse.ContactInfo.id });
    this.notesForm.patchValue({ AccountId: this.id });
    this.notesForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.notesForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });

    this.notesService.addNotes(this.notesForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        console.log('Completed');
        this.closeAddNotesModal.nativeElement.click();
        this.getNotes();
        this.notesForm.reset();
      }
    });
  }

  addCalls() {
    this.submitted = true;

    if (this.callsForm.invalid) {
      return;
    }
   this.spinner.show();
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.callsForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.callsForm.patchValue({ AccountId: this.id });
    this.callsForm.patchValue({ ObjectIntId: this.selContactInfo });
    this.callsForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.callsForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });

    this.callsService.addCalls(this.callsForm.value).subscribe(data => {    
      if (data.Status === 'Success') {
        this.closeAddCallsModal.nativeElement.click();
        this.getCalls();
        this.callsForm.reset();
      }
      this.spinner.hide();
    });
  }
 
  onselectedhours(event) {
    this.Hours = event.target.value;
  }

  onselecteddays(event) {
    this.Days = event.target.value;
  }

  onselectedmins(event) {
    this.Mins = event.target.value;
  }

  inviteesClick(){
    this.NoInvitees = false;
  }

    //#region  Meetings
  markAsCompleted(meetingIntId: number, template: TemplateRef<any>) {
    this.meetingId = meetingIntId;
    this.responseInfo = `<div class="alert alert-success mb-0">     
       <h6>Do you want to complete this meeting..!</h6></div>
        `;
    this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop: 'static' });


  }

  completeConfirmation() {
    let inputObj = {
      MeetingId: this.meetingId
    };
    this.meetingService.markMeetingDone(inputObj).subscribe(data => {
      this.closeMeetingsModal.nativeElement.click();
      if (data.Status === 'Success') {

        this.getMeetings();
      }
      this.mailResponseTemplate.hide();
    });
  }

  addMeeting() {
    this.submitted = true;
    let array = [];

    if (this.selectedUser != undefined && this.selectedUser.length > 0) {
      array = JSON.parse("[" + this.selectedUser + "]");
    } else {
      this.NoInvitees = true;
    }
   
    this.MeetingsForm.patchValue({ ObjectIntId: this.selContactInfo });
    if (this.MeetingsForm.invalid) {
      return;
    }
   this.spinner.show();
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.meetingtime.getHours(), this.meetingtime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.MeetingsForm.patchValue({ Invitees: array });
    this.MeetingsForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.MeetingsForm.patchValue({ AccountId: this.id });
    
    this.MeetingsForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.MeetingsForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.MeetingsForm.patchValue({ Date: timestamp });

    this.meetingService.addMeeting(this.MeetingsForm.value).subscribe(data => {
      this.closeMeetingsModal.nativeElement.click();
      if (data.Status === 'Success') {
        this.getActivityCountData();
        this.getMeetings();
        this.onMeetingReset();
      }
      this.spinner.hide();
    });
  }

  onMeetingReset() {
    this.submitted = false;
    this.MeetingsForm.reset();
    this.NoInvitees = false;
  }
  //#endregion

  //#region  common call
  getActivityCountData() {
    this.accountService.getAccountActivityData(this.id).subscribe(data => {
      if (data.Status === 'Success') {
        let activityresponseData = JSON.parse(data.Data);
        for (var singleActivityResponse of activityresponseData) {
          if (singleActivityResponse.ContactType == "notes") {
            this.notescount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "calls") {
            this.callscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "tasks") {
            this.taskscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "emails") {
            this.emailscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "meetings") {
            this.meetingscount = singleActivityResponse.Count;
          }
        }
      }
    });
  }
  //#endregion
  getNotes() {
    this.notesService.getNotesByAccountId(this.id)
      .subscribe(data => {
        this.notes = data;
      });
  }

  getContactsList(){
    this.accountService.getAccountActivityCount(this.id).subscribe(data => {
      if (data.Status === 'Success') {
        this.activityresponse = JSON.parse(data.Data);
       this.getContactsData = this.activityresponse.ContactList;
      }
      });    
  }

  getCalls() {
    this.callsService.getCallsByAccountId(this.id).subscribe(data => {
      this.calls = data;
    });
  }

  gettasks() {
    this.taskService.getTaskByAccountId(this.id)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.tasks = JSON.parse(data.Data);         
        }
      });
  }
  
//#region Emails
getEmails() {
  this.emailService.getEmailsByAccountId(this.id).subscribe(data => {
    if(data.Code == "SUC-200"){
      this.emails = JSON.parse(data.Data);
    }      
  });
}
//#endregion

  selectContactChangeHandler(event) {
    this.selContactInfo = event.target.value;
  }

  selectStageChangeHandler(selectedValue: string) {
    this.selectedStage = selectedValue;
  }
  // selectDesignationChangeHandler(selectedValue: number) {  
  //   this.selectedDesignationId = selectedValue;
  //  }

  addContact(template: TemplateRef<any>) {
    this.submitted = true;
    if (this.contactForm.invalid) {
      return;
    }
    if(this.IsNewDesignChecked && (this.contactForm.value.designation != null || this.contactForm.value.designation != undefined)){
      this.designationArr = [];
      let newDesignationValue = this.contactForm.value.designation;
      let object ={
        typeid:2,
        labeltitle:newDesignationValue
      };    
      this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
        if(data.Code == "SUC-200"){
          let isDesignExisting = JSON.parse(data.Data);
          if(parseInt(isDesignExisting) > 0){
            this.IsDesignExists = true;
          }else {
            let emailValue = this.contactForm.value.email;  
            this.loading = true;
            if(emailValue){
                this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
                  if(data.Code == "SUC-200"){
                      let isContactExisting = JSON.parse(data.Data);
                      if(parseInt(isContactExisting) > 0){
                        this.IsContactExists = true;        
                        this.loading = false;
                      }else {
                        this.proceedtoCreateContact(this.contactForm, template);
                    }
                  }else {
                      this.proceedtoCreateContact(this.contactForm, template);
                  }
                });
            }else {
              this.proceedtoCreateContact(this.contactForm, template);
            }
          }
        }
      });
    }else {
      let emailValue = this.contactForm.value.email;  
      this.loading = true;
      if(emailValue){
        this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let isContactExisting = JSON.parse(data.Data);
              if(parseInt(isContactExisting) > 0){
                this.IsContactExists = true;        
                this.loading = false;
              }else {
                this.proceedtoCreateContact(this.contactForm, template);
            }
          }else {
              this.proceedtoCreateContact(this.contactForm, template);
          }
        });
      }else {
        this.proceedtoCreateContact(this.contactForm, template);
      }
    }
    
    if(this.IsContactExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Email already exist. Please try with another..!</h6> 
              </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 3000);
      return;
    }

    if(this.IsDesignExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Designation already exist. Please select from list..!</h6> 
      </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
    } 


    if(this.designationArr[0] && this.designationArr[0].designation==1){
      
    }
  }

  proceedtoCreateContact(contactForm, template: TemplateRef<any>){
    let designationId = "";
    if(this.selDesigInfo == undefined || this.selDesigInfo == null){
      designationId = "";
    }else{
      designationId = this.selDesigInfo.id;
    }

    if(this.IsNewDesignChecked){
      designationId = "";
    }

    this.loading = true;
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    contactForm.patchValue({ TeamId: TeamInfo.TeamId });
    contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    contactForm.patchValue({ accountintid: this.id });
    contactForm.patchValue({ designationid: designationId });

    if (this.id != null) {
      this.contactsService
        .AddContact(contactForm.value)
        .subscribe(data => {
          this.loading = false;
          if (data.Status === "Success") {
            this.responseInfo = `<div class="alert alert-success mb-0">     
            <h6>Contact added successfully..!</h6> 
          </div> `;
        }else{
            this.responseInfo = `<div class="alert alert-danger mb-0">     
            <h6>Failed to add contact..!</h6> 
          </div> `;             
          }
            this.closeAddContactModal.nativeElement.click();
            this.getContactsList();
            this.resetdesigs();
            this.onReset();
        });
    }
  }

  //#region  Designations code block
  selectDesignation(){     
    console.log(this.selDesigInfo);     
  }
  desigSelectHandler(event:any){
    let desigArray =[];
    if(event.term.length >= 3){
    this.commonfieldsService.getCommonFields(2,event.term).subscribe(data=>{
      if (data.Status === "Success") {
        let designations = JSON.parse(data.Data);
        designations.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };         
            desigArray.push(singleItem);
        });
        this.designationsList = desigArray; 
      }
     });
    }
  }
  checkDesignValue(event: any) {
    let isChecked = event.target.checked;
    event.target.value = null;
    if (isChecked === true) {
      this.IsNewDesignChecked = true;
      this.designationsList = [];
      this.selDesigInfo = null;

      if(this.contactForm.value.designation != ""){
        let newDesignationValue = this.contactForm.value.designation;
        this.IsDesignExists = false;
        let object ={
          typeid:2,
          labeltitle:newDesignationValue
        };    
        this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
          if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
              if(parseInt(isDesignExisting) > 0){
                this.IsDesignExists = true;
              }
          }
        });
      }
    } else {
      this.IsDesignExists = false; 
      this.IsNewDesignChecked = false;     
    }    
  } 

  focusOutDesignTxt(event:any,template: TemplateRef<any>){
    let newDesignationValue = event.target.value;
    if(newDesignationValue != "" && newDesignationValue != undefined){
      this.IsDesignExists = false;
    let object ={
      typeid:2,
      labeltitle:newDesignationValue
    };    
    this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
       if(data.Code == "SUC-200"){
         let isDesignExisting = JSON.parse(data.Data);
          if(parseInt(isDesignExisting) > 0){
            this.IsDesignExists = true;
            this.responseInfo = `<div class="alert alert-danger mb-0">     
            <h6>Designation already exist. Please select from list..!</h6> 
          </div>`; 
          this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
          setTimeout(() => {
            this.modalResponseRef.hide();
          }, 2000);
          }
       }
    });
  }
  }
   //#endregion

  onReset() {
    this.submitted = false;
    this.contactForm.reset();
    this.IsContactExists = false; 
  }

  manualTemplateModal() {

  }
  sendEmailBtnClick() {

  }
  getMeetings() {
    this.meetingService.getMeetingsByAccountId(this.id).subscribe(data => {
      if(data.Code == "SUC-200"){
        let ResponseInfo = JSON.parse(data.Data);
        this.meetingList = ResponseInfo;
      }
    });
  }

  previousPage(){
    this.location.back();
  }
  
}