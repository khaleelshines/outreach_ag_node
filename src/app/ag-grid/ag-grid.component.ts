import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactApiService } from '../../services/contacts.services';
import { GridOptions } from 'ag-grid-community';
import { CellCustomComponent } from '../custom_components/cell-custom/cell-custom.component';
import { array } from '@amcharts/amcharts4/core';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.css']
})
export class AgGridComponent implements OnInit, OnDestroy {
 
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public rowData: any;
  public contactsData:any;
  paginationPageSize: number;
  cacheBlockSize: number;
  public stageInfo:any;
  subscriptions:Subscription [] = [];
  columns: Array<any> = [
    { name: 'contactname', label: 'Contact Name' },
    { name: 'firstname', label: 'First Name' },
    { name: 'lastname', label: 'Last Name' },
    { name: 'stage', label: 'Stage' }
  ];

  
  ngOnInit() {

   let contactSubscription = this.contactsService.getContactsStageInfo()
    .subscribe(data => {     
      this.stageInfo = JSON.parse(data.Data); 
    }); 
    this.subscriptions.push(contactSubscription);
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  } 
 


  constructor(private http: HttpClient,private contactsService:ContactApiService) {
    
    // this.columnDefs = [
    //   {
    //     headerName: "Athlete",
    //     field: "athlete",
    //     width: 150,
    //     checkboxSelection: function(params) {
    //       return params.columnApi.getRowGroupColumns().length === 0;
    //     },
    //     headerCheckboxSelection: function(params) {
    //       return params.columnApi.getRowGroupColumns().length === 0;
    //     }
    //   },
    //   {
    //     headerName: "Age",
    //     field: "age",
    //     width: 90
    //   },
    //   {
    //     headerName: "Country",
    //     field: "country",
    //     width: 120
    //   },
    //   {
    //     headerName: "Year",
    //     field: "year",
    //     width: 90
    //   },
    //   {
    //     headerName: "Date",
    //     field: "date",
    //     width: 110
    //   },
    //   {
    //     headerName: "Sport",
    //     field: "sport",
    //     width: 110
    //   },
    //   {
    //     headerName: "Gold",
    //     field: "gold",
    //     width: 100
    //   },
    //   {
    //     headerName: "Silver",
    //     field: "silver",
    //     width: 100
    //   },
    //   {
    //     headerName: "Bronze",
    //     field: "bronze",
    //     width: 100
    //   },
    //   {
    //     headerName: "Total",
    //     field: "total",
    //     width: 100
    //   }
    // ];
    // this.columnDefs = this.prepareColumns();
    this.columnDefs = [
      {
        headerName: "Contact Name",
        field: "contactname",       
        checkboxSelection: function(params) {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: function(params) {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      },
      {
        headerName: "First Name",
        field: "firstname",
       
      },{
        headerName: "Last Name",
        field: "lastname",
       
      },
      {
        headerName: "Stage",
        field: "stage"      
      },
      {
        headerName: "Designation",
        field: "designation"      
      },
      {
        headerName: "Email",
        field: "email",       
      },
      {
        headerName: "Contact No",
        field: "mobilenumber"        
      },
      { headerName: 'Actions', pinned: 'right',width:150,editable:false,
      cellRendererFramework: CellCustomComponent,
        // cellRenderer: (data) => {         
        //   // if (data.data.stage === "interested") 
        //   // {
        //         return '<div class"text-center"><span class="far fa-trash-alt mr-2" (click)="deleteClicked()" title="Delete entry"></span>' +
        //                '<span class="fab fa-nintendo-switch" title="Promote entry"></span></div>';
        //  // }
        // }
    }
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      width: 200,
      field: "customername",
      valueGetter: function(params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,
      width: 200
    
    };
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "always";
    this.pivotPanelShow = "always";
    this.cacheBlockSize= 10;
    this.paginationPageSize= 10;
  }

  gridOptions: GridOptions = {
    rowHeight:37
  };

  prepareColumns(){
    var ColumnsArray = [];
    for(let singleCol of this.columns) {
      let colDef = {
        headerName:singleCol.label,
        field:singleCol.name
      };
      ColumnsArray.push(colDef);      
    }
    let actionCol = {
      headerName:"Action",
      pinned: 'right',width:150,editable:false,
      cellRendererFramework: CellCustomComponent,
    };
    ColumnsArray.push(actionCol);
    return ColumnsArray;
  }

  applystagefilter(clickedText){   
   clickedText = clickedText.toLowerCase();
    this.contactsService.getContactsrawdata()
    .subscribe(data => {     
      this.contactsData = JSON.parse(data.Data);    
      if(clickedText == "all"){
        this.rowData = this.contactsData;   
      }else{
        this.rowData = this.contactsData.filter(a=>a.stage == clickedText);   
      }   
           
    }); 
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.contactsService.getContactsrawdata()
    .subscribe(data => {     
      this.contactsData = JSON.parse(data.Data);       
      this.rowData = this.contactsData;   
     // this.rowData.filter = "interested";      
    }); 
    

    
  }
}