import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
  NgbModalModule,
  NgbAlertModule,
  NgbPaginationModule,
  NgbModule
} from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BsDatepickerModule} from "ngx-bootstrap/datepicker";
import { TimepickerModule} from "ngx-bootstrap/timepicker";
import { PopoverModule} from "ngx-bootstrap/popover";

import { UserService } from "../services/userservice.services";
import { SequenceApiService } from "../services/sequenceservice.services";
import { ContactApiService } from "../services/contacts.services";
import { AccountApiService } from "../services/accounts.services";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { RegisterComponent } from "./register/register.component";
import { AdminComponent } from "./dashboard/admin/admin.component";
import { MemberComponent } from "./dashboard/member/member.component";
import { Routes, RouterModule } from "@angular/router";
import { HeaderComponent } from "./layout/header/header.component";
import { FooterComponent } from "./layout/footer/footer.component";
import { LogoutComponent } from "./profile/logout/logout.component";
import { AccessdeniedComponent } from "./security/accessdenied/accessdenied.component";
import { HeaderwithoutmenuComponent } from "./layout/headerwithoutmenu/headerwithoutmenu.component";
import { ContactsComponent } from "./contacts/contacts.component";
import { AuthGuard } from "./guards/authguard/auth.guard";
import { RoleGuard } from "./guards/roleguard/role-guard.service";
import {CustomGuard} from "./guards/customguard/custom-guard.service"
import { ActivitiesComponent } from "./activities/activities.component";
import { TaskComponent } from "./task/task.component";
import { FormsModule } from "@angular/forms";
import { SequenceComponent } from "./sequence/sequence.component";
import { TokenInterceptor } from "src/_helpers/interceptor";
import { ResetpasswordComponent } from "./resetpassword/resetpassword.component";
import { ForgotpasswordComponent } from "./forgotpassword/forgotpassword.component";
import { ViewsequenceComponent } from "./sequence/viewsequence/viewsequence.component";
import { DataService } from "../services/DataService";
import { CompanyprofileComponent } from "./profile/companyprofile/companyprofile.component";
import { ProfileApiService } from "src/services/profileservice.services";
import { AccountsComponent } from "./accounts/accounts.component";
import { AgGridModule } from "ag-grid-angular";
import { AgGridComponent } from "./ag-grid/ag-grid.component";
import "ag-grid-enterprise";
import { ContactswithaggridComponent } from "./contactswithaggrid/contactswithaggrid.component";
import { TaskApiService } from "src/services/taskservice.services";
import { NotesApiService } from "../services/notes.services";
import { CellCustomComponent } from "./custom_components/cell-custom/cell-custom.component";
import { NotFoundComponent } from "./custom_components/not-found/not-found.component";
import { OpportunitiesComponent } from "./opportunities/opportunities.component";
import { AccountsActionsComponent } from "./custom_components/accounts-actions/accounts-actions.component";
import { ViewcontactComponent } from "./contacts/viewcontact/viewcontact.component";
import { EmailProcessApiService } from "src/services/emailintegration.services";
import { TasksActionsComponent } from "./custom_components/tasks-actions/tasks-actions.component";
import { CallsApiService } from "src/services/calls.services";
import { TeamComponent } from "./team/team.component";
import { TasksComponent } from "./dashboard/charts/tasks/tasks.component";
import { TemplatesComponent } from "./templates/templates.component";
import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";
import { templateService } from "src/services/templates.services";
import { Dashboard360ViewComponent } from "./dashboard/dashboard360-view/dashboard360-view.component";
import { UsermanagementComponent } from "./usermanagement/usermanagement.component";
import { RoleApiService } from "src/services/roles.services";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { UsersActionsComponent } from "./custom_components/users-actions/users-actions.component";
import { dashboardAPIService } from "src/services/dashboard.services";
import { OrgChartModule } from "@mondal/org-chart";
import { TeammanagementComponent } from "./teams/teammanagement/teammanagement.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { teammanagementservice } from "src/services/teamsmanagement.services";
import { OpportunityApiService } from "src/services/opportunities.services";
import { EmailApiService } from "src/services/emails.services";
import { CountryService } from "./team/country.service";
import { DecimalPipe, DatePipe} from "@angular/common";
import { DatatablesComponent } from "./team/datatables/datatables.component";
import { NgbdSortableHeader } from "./team/sortable.directive";
import { NgSelectModule } from "@ng-select/ng-select";
import { MeetingApiService } from "src/services/meeting.services";
import { CountryService1 } from "./team/datatables/country1.service";
import { OpportunityService1 } from "./opportunities/opportunity.service";
import { ViewaccountsComponent } from "./accounts/viewaccounts/viewaccounts.component";
import { AdminheaderComponent } from "./layout/adminheader/adminheader.component";
import { ViewopportunityComponent } from "./opportunities/viewopportunity/viewopportunity.component";
import { OauthEmailComponent } from "./oauth-email/oauth-email.component";
import { AddtaskComponent } from "./task/addtask/addtask.component";
import { UtilitiesService } from "src/services/utilities.services";
import { UserprofileComponent } from "./profile/userprofile/userprofile.component";
import { ViewcontactsequenceComponent } from "./contacts/viewcontactsequence/viewcontactsequence.component";
import { TaskService } from "./task/task.service";
import { NgxSpinnerModule } from "ngx-spinner";
import { LibrariesComponent } from "./libraries/libraries.component";
import { UnsubscribeComponent } from "./emails/unsubscribe/unsubscribe.component";
import { UsersequenceComponent } from './member/usersequence/usersequence.component';
import { ViewusersequenceComponent } from './member/viewusersequence/viewusersequence.component';
import { EmailcontentComponent } from './emails/emailcontent/emailcontent.component';
import { ActivitiesChartComponent } from './dashboard/charts/ActivitiesChart/activitieschart.component';
import { SequenceinitiateComponent } from './sequence/sequenceinitiate/sequenceinitiate.component';
import { TitleCaseDirective } from './title-case.directive';
import { BouncedemailsComponent } from './bouncedemails/bouncedemails.component';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { ContactsPophoverComponent } from './custom_components/contacts-pophover/contacts-pophover.component';
import { ViewsequencestepcontactsComponent } from './contacts/viewsequencestepcontacts/viewsequencestepcontacts.component';
import { ContactsActionsComponent } from './custom_components/contacts-actions/contacts-actions.component';
import { AccountslistComponent } from './serverside/accountslist/accountslist.component';
import { AccountsActionItemsComponent } from './custom_components/accounts-action-items/accounts-action-items.component';
import { ActivitieslistComponent } from './serverside/activitieslist/activitieslist.component';
import { AddnewcontactComponent } from './contacts/addnewcontact/addnewcontact.component';
import { TemplategridactionsComponent } from './templates/templategridactions/templategridactions.component';
import { AddnewaccountComponent } from './accounts/addnewaccount/addnewaccount.component';
import { AddnewtaskComponent } from './task/addnewtask/addnewtask.component';
import { TeamperformanceComponent } from './teams/teamperformance/teamperformance.component';
import { teamPerformanceService } from 'src/services/teamperformance.service';
import { MastertypesComponent } from './reports/mastertypes/mastertypes.component';
import { reportsService } from 'src/services/reports.services';
import { SequencereportsComponent } from './reports/sequencereports/sequencereports.component';
import { ViewtaskComponent } from './task/viewtask/viewtask.component';
import { SequencereportComponent } from './reports/sequencereport/sequencereport.component';
import { OpportunitieslistComponent } from './serverside/opportunitieslist/opportunitieslist.component';
import { TasklistComponent } from './serverside/tasklist/tasklist.component';
import { TemplateslistComponent } from './serverside/templateslist/templateslist.component';
import { AppSettings } from './shared/app.settings';
const myRoots: Routes = [
  {
    path: "dashboard/admin",
    component: AdminComponent,
    canActivate: [AuthGuard, RoleGuard]
  },
  {
    path: "dashboard/member",
    component: MemberComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "dashboard",
    component: Dashboard360ViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path:"email/unsubscribe",
    component:BouncedemailsComponent
  },
  { path: "access-denied", component: AccessdeniedComponent },
  { path: "task", component: TasklistComponent, canActivate: [AuthGuard,CustomGuard] },
  { path: "task1", component: DatatablesComponent },
  { path: "tasklist", component: TasklistComponent },
  { path: "task/:type", component: TasklistComponent, canActivate: [AuthGuard] },
  {
    path: "profile/company",
    component: CompanyprofileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "task/addtask",
    component: AddnewtaskComponent,
    canActivate: [AuthGuard]
  },
  { path: "logout", component: LogoutComponent },
 // { path: "opportunities", component: OpportunitiesComponent }, 
  { path: "opportunities", component: OpportunitieslistComponent }, 
  { path: "notfound", component: NotFoundComponent },
  { path: "ag-grid", component: AgGridComponent },  
  { path: "notfound", component: NotFoundComponent },
  {
    path: "contacts/addcontact",
    component: AddnewcontactComponent,
    canActivate: [AuthGuard]
  },

  {
    path: "tasks/viewtask/:id",
    component: ViewtaskComponent,
    //canActivate: [AuthGuard]
  },

  {
    path: "contacts/viewcontact/:id",
    component: ViewcontactComponent,
    canActivate: [AuthGuard]
  },{
    path: "accounts/addaccount",
    component: AddnewaccountComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "accounts/viewaccount/:id",
    component: ViewaccountsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "contacts/:contactid/viewsequence/:id",
    component: ViewcontactsequenceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "opportunities/viewopportunity/:id",
    component: ViewopportunityComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/reports",
    component: MastertypesComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/templates",
    component: TemplateslistComponent,
    //component: TemplatesComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/templateslist",
    component: TemplateslistComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/team/management",
    component: TeammanagementComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/team/performance",
    component: TeamperformanceComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/reports",
    component: MastertypesComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {
    path: "admin/sequencereports",
    component: SequencereportComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  { path: "datatables", component: TeamComponent },
  { path: "datatables1", component: DatatablesComponent },
  {
    path: "user/profile",
    component: UserprofileComponent,
    canActivate: [AuthGuard]
  },
  { path: "oauth-email", component: OauthEmailComponent },
  {
    path: "admin/sequence",
    component: SequenceComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  {path:"sequences",component:UsersequenceComponent},
  {
    path: "sequenceview/:id",
    component: ViewusersequenceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "emailcontent/:id",
    component: EmailcontentComponent,
   // canActivate: [AuthGuard]
  },
  {path:"activitieslist", component:ActivitiesComponent },
  { path: "activities", component: ActivitieslistComponent, canActivate: [AuthGuard] },
  { path: "resetpassword", component: ResetpasswordComponent },
  { path: "forgotpassword", component: ForgotpasswordComponent },
  {
    path: "viewsequence/:id",
    component: ViewsequenceComponent,
    canActivate: [AuthGuard]
  },  
  { path: "contactslist", component: ContactsComponent, canActivate: [AuthGuard] },
  { path: "contacts", component: ContactswithaggridComponent ,canActivate: [AuthGuard]},
  { path: "contacts/:type", component: ContactswithaggridComponent, canActivate: [AuthGuard] },
  { path: "accounts/:type", component: AccountslistComponent, canActivate: [AuthGuard] },
 // { path: "contacts/:type", component: ContactsComponent, canActivate: [AuthGuard] },
  { path: "accountslist", component: AccountsComponent, canActivate: [AuthGuard] },
  { path: "accounts", component: AccountslistComponent, canActivate: [AuthGuard] },
  // { path: '', component: LoginComponent,pathMatch: "full" },
  {
    path: "admin/users",
    component: UsermanagementComponent,
    canActivate: [AuthGuard,RoleGuard]
  },
  { path: "changepassword", component: ChangepasswordComponent },
  { path: "libraries", component: LibrariesComponent },
  { path: "emails/unsubscribe/:id", component: UnsubscribeComponent },
  {path:"sequence/initiate",component:SequenceinitiateComponent},
  {path:"sequence/step-contacts/:seqid/:stepid/:type",component:ViewsequencestepcontactsComponent},
  { path: "", redirectTo: "/task", pathMatch: "full" },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AdminComponent,
    MemberComponent,
    HeaderComponent,
    FooterComponent,
    LogoutComponent,
    AccessdeniedComponent,
    HeaderwithoutmenuComponent,
    ContactsComponent,
    ActivitiesComponent,
    TaskComponent,
    SequenceComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    ViewsequenceComponent,
    CompanyprofileComponent,
    AccountsComponent,
    AgGridComponent,
    ContactswithaggridComponent,
    CellCustomComponent,
    NotFoundComponent,
    OpportunitiesComponent,
    AccountsActionsComponent,
    ViewcontactComponent,
    TasksActionsComponent,
    TeamComponent,
    TemplatesComponent,
    Dashboard360ViewComponent,
    TasksComponent,
    UsermanagementComponent,
    ChangepasswordComponent,
    UsersActionsComponent,
    TeammanagementComponent,
    DatatablesComponent,
    NgbdSortableHeader,
    ViewaccountsComponent,
    AdminheaderComponent,
    ViewopportunityComponent,
    OauthEmailComponent,
    AddtaskComponent,    
    UserprofileComponent,
    ViewcontactsequenceComponent,
    LibrariesComponent,
    UnsubscribeComponent,
    UsersequenceComponent,
    ViewusersequenceComponent,
    EmailcontentComponent,
    ContactsPophoverComponent,    
    ActivitiesChartComponent, SequenceinitiateComponent, TitleCaseDirective,
    BouncedemailsComponent, ContactsPophoverComponent, ViewsequencestepcontactsComponent, 
    ContactsActionsComponent, AccountslistComponent, AccountsActionItemsComponent,
    ActivitieslistComponent,AddnewcontactComponent, TemplategridactionsComponent, AddnewaccountComponent, AddnewtaskComponent, TeamperformanceComponent, MastertypesComponent, SequencereportsComponent, ViewtaskComponent, SequencereportComponent, OpportunitieslistComponent, TasklistComponent, TemplateslistComponent,
  ],
  imports: [   
    NgSelectModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgbModalModule,
    FormsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PopoverModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    AgGridModule.withComponents([ContactsPophoverComponent]),
    OrgChartModule,
    NgxSpinnerModule,
    NgbPaginationModule,
    NgbAlertModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(
      myRoots,
      { enableTracing: true } // <-- debugging purposes only
    ),
    ModalModule.forRoot()
  ],
  providers: [
    UserService,
    SequenceApiService,
    ContactApiService,
    AccountApiService,
    ProfileApiService,
    TaskApiService,
    NotesApiService,
    DataService,
    EmailProcessApiService,
    CallsApiService,
    templateService,
    teammanagementservice,
    CountryService,
    DecimalPipe,
    CountryService1,
    DatePipe,
    TaskService,
    RoleApiService,
    dashboardAPIService,
    UtilitiesService,
    OpportunityApiService,
    CountryService,
    DecimalPipe,
    RoleApiService,
    dashboardAPIService,
    EmailApiService,
    MeetingApiService, 
    CommonFieldsService,    
    teamPerformanceService,
    reportsService,
    AppSettings,
    OpportunityService1,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    CellCustomComponent,
    AccountsActionsComponent,
    TasksActionsComponent,
    UsersActionsComponent,
    ContactsActionsComponent,
    AccountsActionItemsComponent, 
    TemplategridactionsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
