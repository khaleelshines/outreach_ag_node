import { Component, OnInit, ElementRef, ViewChild, OnDestroy, TemplateRef } from "@angular/core";
import { ContactApiService } from "src/services/contacts.services";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { SequenceComponent } from "../sequence/sequence.component";
import { AccountApiService } from "src/services/accounts.services";
import { GridOptions, CellMouseOverEvent } from "ag-grid-community";
import { tap } from "rxjs/operators";
import { CellCustomComponent } from "../custom_components/cell-custom/cell-custom.component";
import { DataService } from "src/services/DataService";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { UserService } from "src/services/userservice.services";
import { teammanagementservice } from "src/services/teamsmanagement.services";
import { AuthService } from "../guards/authguard/auth.service";
import { UtilitiesService } from "src/services/utilities.services";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFieldsService } from 'src/services/commonfields.services';
import { Subscription } from 'rxjs';
import { ContactsPophoverComponent } from "../custom_components/contacts-pophover/contacts-pophover.component";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  providers: [SequenceComponent],
  selector: "app-contacts",
  templateUrl: "./contacts.component.html",
  styleUrls: ["./contacts.component.css"]
})
export class ContactsComponent implements OnInit,OnDestroy {
 
  contactForm: FormGroup;
  submitted = false;
  Sequences: any;
  Accounts: any;
  Users: any;
  contacts: any;
  selectedOption: any;
  selectedContact: any;
  contactid: any;
  filterType: string = "all";
  responseInfo:any;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  public stageInfo: any;
  selectedStage: string;
  selectedAccountId: string;
  selectedOwnerId: string;
  usersData: any;
  loggedInUserRole: string;
  IsMember: boolean = false;
  userInfo: any;
  IsChecked: boolean = false;
  accountintiderror: boolean = false;
  accountnameerror: boolean = false;
  clickedContactFilter:string;
  selectedDesignationId:number;
  selectedDesignationCat:string;
  designationsList:any;
  designationCategoryList:any;
  engSelect:string = "thisweek";
  crdSelect :string = "thisweek";
  stgSelect:string = "all";
  modalResponseRef:BsModalRef;
  public frameworkComponents;
  subscriptions: Subscription[] = [];
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;

  public getContextMenuItems(params) {
    let columnName = params.column.colDef.headerName;
    if (columnName === "Actions") {
      return [];
    } else {
      var result = ["copy", "paste", "export", "autoSizeAll"];
      return result;
    }
  }
  get f() {
    return this.contactForm.controls;
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ContactApiService,
    private modalBsService: BsModalService,
    private modalService: NgbModal,
    private sequences: SequenceComponent,
    private contactsService: ContactApiService,
    private accounts: AccountApiService,
    private userService: UserService,
    private teamService: teammanagementservice,
    private auth: AuthService,
    public utilities: UtilitiesService,
    private spinner: NgxSpinnerService,
    private route:ActivatedRoute,
    private commonfieldsService:CommonFieldsService
  ) {
    this.sideBar = {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ],
      defaultToolPanel: ""
    };
    this.columnDefs = [
      {
        headerName: "ID",
        field: "id",
        hide: true
      },
      {
        headerName: "AccountID",
        field: "accountintid",
        hide: true
      },
      {
        headerName: "ContactID",
        field: "contactid",
        hide: true
      },
      {
        headerName: "Contact Name",
        field: "contactname",
        cellRenderer: function(params) {
          return (
            '<a href="contacts/viewcontact/' +
            params.data.id +
            '""target="_blank">' +
            params.value +
            "</a>"
          );
        },
       tooltipField: "contactname",
       tooltipComponentParams: { color: "#ececec" }
      },
      {
        headerName: "Account Name",
        field: "AccountName",
        // cellRenderer: function(params) {
        //   return (
        //     '<span ngbPopover="You see, I show up on hover!" triggers="mouseenter" popoverTitle="Pop title">' +
        //     params.value +
        //     "</span>"
        //   );
        // },      
    },
     // {
      //   headerName: "Last Name",
      //   field: "lastname",

      // },
      {
        headerName: "Last Contacted Date",
        field: "LastContactedDate"
      },
      {
        headerName: "Last Contact Type",
        field: "LastContactType"
      },
      {
        headerName: "No Of Contacted",
        field: "NoOfContacted"
      },
       {
        headerName: "Email",
        field: "email",
      },
      {
        headerName: "Assigned To",
        field: "AssigneeName"
      },
      {
        headerName: "Stage",
        field: "stage",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: {
          values: [
            "Not Started",
            "Cold",
            "Replied",
            "Unresponsive",
            "do not contact",
            "Bad Contact Info",
            "Interested",
            "Not Interested"
          ]
        }
      },
      {
        headerName: "Created On",
        field: "SyncDate"
      },
      {
        headerName: "Actions",
        pinned: "right",
        //  width: 130,
        editable: false,
        cellStyle: { "padding-top": "2px" },
        cellRendererFramework: CellCustomComponent
      }
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      //  width: 200,
      field: "customername",
      valueGetter: function(params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,
      tooltipComponent: "customTooltip"
      // width: 200
    };
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "false";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
    this.frameworkComponents = { customTooltip: ContactsPophoverComponent };
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }else {
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }

   
  }
  gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52,
    getContextMenuItems: this.getContextMenuItems
  };
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);
      // this.gridOptions.api.onFilterChanged();
    }
  }

  // applystagefilter(clickedText) {
  //   clickedText = clickedText.toLowerCase();
  //   if (this.filterType == "all") {
  //     this.contactsService.getContactsrawdata().subscribe(data => {
  //       let ContactsMasterInfo = JSON.parse(data.Data);
  //       if (clickedText == "all") {
  //         this.rowData = ContactsMasterInfo.ListOfContactedContacts;
  //       } else {
  //         this.rowData = ContactsMasterInfo.ListOfContactedContacts.filter(
  //           a => a.stage.toLowerCase() == clickedText
  //         );
  //       }
  //     });
  //   } else {
  //     this.contactsService
  //       .getContactsByTimeFilter(this.filterType)
  //       .subscribe(data => {
  //         let contactedContactsMasterInfo: any = JSON.parse(data.Data);
  //         this.contactsData =
  //           contactedContactsMasterInfo.ListOfContactedContacts;
  //         if (clickedText == "all") {
  //           this.rowData = this.contactsData;
  //         } else {
  //           this.rowData = this.contactsData.filter(
  //             a => a.stage.toLowerCase() == clickedText
  //           );
  //         }
  //       });
  //   }
  // }

  mouseEnter(){
    alert("mouse entered");
  }

  selectDesignationChangeHandler(selectedValue: number) {  
    this.selectedDesignationId = selectedValue;
   }

   sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }

  StageChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();
    this.contactsService.getContactsrawdata().subscribe(data => {
            let ContactsMasterInfo = JSON.parse(data.Data);
            if (selectedValue == "all") {
              this.rowData = ContactsMasterInfo.ListOfContactedContacts;
            } else {
              this.rowData = ContactsMasterInfo.ListOfContactedContacts.filter(
                a => a.stage.toLowerCase() == selectedValue
              );
            }
    });
  }


  mainFilterTabsChanged(clickedText) {
    clickedText = clickedText.toLowerCase();
    if(clickedText === "all"){
      this.stgSelect = "all";
      this.rowData = this.contactsData;
    }else{
      var thisWeekDates= this.utilities.thisWeekDates();          
      let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
      let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
       if(clickedText === "thisweek-created"){ 
         this.crdSelect = "thisweek";
      this.rowData = this.contactsData.filter(        
        i=>new Date(i.SyncDate) >= new Date(thisWeekStartDate) && new Date(i.SyncDate) <= new Date(thisWeekEndDate)
      );
    }else{
      this.engSelect = "thisweek";
      this.rowData = this.contactsData.filter(
        i=>new Date(i.LastContactedDate) >= new Date(thisWeekStartDate) && new Date(i.LastContactedDate) <= new Date(thisWeekEndDate)
      );
    }
  }
  }

  EngagedChangedHandler(selectedValue:string){
    selectedValue = selectedValue.toLowerCase(); 
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();   
   
        if (selectedValue == "notengaged") {
          this.rowData = this.contactsData.filter(
            i=>i.LastContactedDate == "-"
          );
        }else if(selectedValue == "today") {
          let currentDate = new Date();
          let todayDate = this.utilities.dateConvertion(currentDate);      
          
          this.rowData = this.contactsData.filter(
            i=>i.LastContactedDate === todayDate
          );
        } else if(selectedValue == "all") {
          this.rowData = this.contactsData;
        }
        else if(selectedValue == "thisweek") {
          var thisWeekDates= this.utilities.thisWeekDates();          
          let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
          let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
          this.rowData = this.contactsData.filter(
            i=>new Date(i.LastContactedDate) >= new Date(thisWeekStartDate) && new Date(i.LastContactedDate) <= new Date(thisWeekEndDate)
          );
        } else if(selectedValue == "lastweek") {
          var lastWeekDates= this.utilities.lastWeekDates();          
          let lastWeekStartDate = this.utilities.dateConvertion(lastWeekDates.startDate);
          let lastWeekEndDate = this.utilities.dateConvertion(lastWeekDates.endDate); 
          this.rowData = this.contactsData.filter(
            i=>new Date(i.LastContactedDate) >= new Date(lastWeekStartDate) && new Date(i.LastContactedDate) <= new Date(lastWeekEndDate)
          );
        }else if (selectedValue === "thismonth"){
          var currentMonthFirstDay = new Date(y, m, 1);
          var currentMonthLastDay = new Date(y, m + 1, 0);
          var currentMonthStartDate = this.utilities.dateConvertion(currentMonthFirstDay);
          var currentMonthEndDate = this.utilities.dateConvertion(currentMonthLastDay);
          this.rowData = this.contactsData.filter(
            i=>new Date(i.LastContactedDate) >= new Date(currentMonthStartDate) && new Date(i.LastContactedDate) <= new Date(currentMonthEndDate)
          );
        }else if (selectedValue === "lastmonth"){
          var lastMonthFirstDay = new Date(y, m-1, 1);
          var lastMonthLastDay = new Date(y, m, 0);
          let lastMonthStartDate = this.utilities.dateConvertion(lastMonthFirstDay);
          let lastMonthEndDate = this.utilities.dateConvertion(lastMonthLastDay); 
     
          this.rowData = this.contactsData.filter(        
            i=>new Date(i.LastContactedDate) >= new Date(lastMonthStartDate) && new Date(i.LastContactedDate) <= new Date(lastMonthEndDate)
          );
        }else if (selectedValue === "last3months"){
          var last3MonthFirstDay = new Date(y, m-3, 1);
          var last3MonthLastDay = new Date(y, m, 0);
          let last3MonthStartDate = this.utilities.dateConvertion(last3MonthFirstDay);
          let last3MonthEndDate = this.utilities.dateConvertion(last3MonthLastDay); 
     
          this.rowData = this.contactsData.filter(        
            i=> new Date(i.LastContactedDate) < new Date(last3MonthStartDate) 
          );
        }
       
  } 

  onCellValueChanged(event) {
    let contactObj = event.data;
    let modifiedValue = event.newValue;
    let oldValue = event.oldValue;
    let key = this.getKeyByValue(contactObj, modifiedValue);
    let contactdata: any = {
      ContactId: contactObj.contactid,
      FieldName: key,
      FieldValue: modifiedValue
    };
    if (modifiedValue != "" && modifiedValue != oldValue) {
      this.contactsService.updateContactInfo(contactdata).subscribe(data => {
        if (data.Status === "Success") {
          console.log("Completed");
        }
        //  this.rowData = this.contactsData;
        // this.stageInfo = contactedContactsMasterInfo.ListOfContactStages;
      });
    }
  }

  // onCellMouseOver($event: CellMouseOverEvent) {
  //   let data = $event.data;
  //   console.log(data);
  //   // if($event.colDef.field == "Subject"){
  //   // this.singleEmailActivity = $event.data;
  //   // }
  // }

  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  filterForeCasts(filterVal: any) {
    let date: Date = new Date();
    var currentWeekDay = date.getDay();
    var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay - 1;
    var wkStart = new Date(new Date(date).setDate(date.getDate() - lessDays));
    var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate() + 6));

    let starttimestamp = Math.floor(wkStart.getTime() / 1000.0);
    let endtimestamp = Math.floor(wkEnd.getTime() / 1000.0);

    let startdate =
      wkStart.getFullYear() +
      "-" +
      wkStart.getMonth() +
      "-" +
      wkStart.getDate();
    let enddate =
      wkEnd.getFullYear() + "-" + wkEnd.getMonth() + "-" + wkEnd.getDate();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // this.gridOptions.api.sizeColumnsToFit(); // newly added for table full width
    // this.gridApi.sizeColumnsToFit();
    this.getContactsData();
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    this.getDesignationsList();

    this.getDesignationCategoryList();

    if (!window.localStorage.getItem("token")) {
      this.router.navigate(["login"]);
    }   

    this.route.params.subscribe(params=>{
      this.clickedContactFilter = params['type'];
     });

    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],
      TeamId: [],
      designationid:["",this.selectedDesignationId],
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: []
    });

    // this.getContacts();
    this.getAccounts();
   let teamSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
    this.subscriptions.push(teamSub);
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());   
  }

  // [this.validateIfChecked(true)]
  validateIfChecked(checkStatus: boolean): ValidatorFn {
    if (checkStatus === true) {
      return (
        control: AbstractControl
      ): {
        [key: string]: any;
      } | null => {
        if (this.IsChecked) {
          return control.value
            ? null
            : {
                err: true
              };
        }
        return null;
      };
    } else {
      return (
        control: AbstractControl
      ): {
        [key: string]: any;
      } | null => {
        if (!this.IsChecked) {
          return control.value
            ? null
            : {
                err: true
              };
        }
        return null;
      };
    }
  }

  getContactsData() {
  let contactsSub =  this.contactsService.getContactsrawdata().subscribe(data => {
    if(data.Status == "Success"){
      let masterContactsData = JSON.parse(data.Data);
      this.contactsData = masterContactsData.ListOfContactedContacts;
      switch (this.clickedContactFilter) {
        case 'en-thisweek' || undefined:
          this.EngagedChangedHandler("thisweek");
          break;
        case 'en-thismonth':
          this.EngagedChangedHandler("thismonth");
          this.engSelect = "thismonth";
          break;
        case 'en-today':
          this.EngagedChangedHandler("today");
          this.engSelect = "today";
          break;
        case 'en-lastweek':
          this.EngagedChangedHandler("lastweek");
          this.engSelect = "lastweek";
          break;
        default:
          this.EngagedChangedHandler("thisweek");
          break;
      }
      this.stageInfo = masterContactsData.ListOfContactStages;
    }
    });
    this.subscriptions.push(contactsSub);
  }

  onReset() {
    this.submitted = false;
    this.accountnameerror = false;
      this.accountintiderror = false;
    this.contactForm.reset();
  }

  public getContacts() {
    return new Promise((resolve, reject) => {
      this.contactsService.getContacts().subscribe(data => {
        if (data.Status === "Success") {
          this.contacts = JSON.parse(data.Data);
          resolve(this.contacts);
        }
      });
    });
  }

  selectStageChangeHandler(selectedValue: string) {
    this.selectedStage = selectedValue;
  }
  selectAccountChangeHandler(selectedValue: string) {
    this.selectedAccountId = selectedValue;
  }

  selectOwnerChangeHandler(selectedValue: string) {
    this.selectedOwnerId = selectedValue;
  }

  onSelect(contact: any): void {
    this.selectedContact = contact;
    this.contactid = this.selectedContact.contactid;
  }

  getSequences(contactid: string) {
    this.contactid = contactid;
    this.sequences.getSequences().then(result => {
      this.Sequences = result;
    });
  }

  getAccounts() {
   let accountsSub = this.accounts.getAccounts().subscribe(data => {
     if(data.Status == "Success"){
      let accounts = JSON.parse(data.Data);
      this.Accounts = accounts.ListOfAccounts;
     }
    });
    this.subscriptions.push(accountsSub);
  }

  CreatedChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();   
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(); 
        if (selectedValue == "all") {
          this.rowData = this.contactsData;
          
        }else if (selectedValue === "thismonth"){
          var currentMonthFirstDay = new Date(y, m, 1);
          var currentMonthLastDay = new Date(y, m + 1, 0);
          var currentMonthStartDate = this.utilities.dateConvertion(currentMonthFirstDay);
          var currentMonthEndDate = this.utilities.dateConvertion(currentMonthLastDay);
          this.rowData = this.contactsData.filter(
            i=>new Date(i.SyncDate) >= new Date(currentMonthStartDate) && new Date(i.SyncDate) <= new Date(currentMonthEndDate)
          );
        }else if (selectedValue === "thisweek"){
          var thisWeekDates= this.utilities.thisWeekDates();          
          let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
          let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
     
          this.rowData = this.contactsData.filter(        
            i=>new Date(i.SyncDate) >= new Date(thisWeekStartDate) && new Date(i.SyncDate) <= new Date(thisWeekEndDate)
          );
        }else if (selectedValue === "today"){
          let currentDate = new Date();
          let todayDate = this.utilities.dateConvertion(currentDate);      
          this.rowData = this.contactsData.filter(        
            i=>i.SyncDate === todayDate
          );
        }     
    
  }

  contactedFilter(filtertype: string) {
    // alert(filtertype);
    this.filterType = filtertype;
    if (filtertype != "all") {
      this.contactsService
        .getContactsByTimeFilter(filtertype)
        .subscribe(data => {
          let contactedContactsMasterInfo: any = JSON.parse(data.Data);
          this.contactsData =
            contactedContactsMasterInfo.ListOfContactedContacts;
          this.rowData = this.contactsData;
          this.stageInfo = contactedContactsMasterInfo.ListOfContactStages;
        });
    } else {     
      this.contactsService.getContactsrawdata().subscribe(data => {
        let contactedContactsMasterInfo = JSON.parse(data.Data);
        this.contactsData = contactedContactsMasterInfo.ListOfContactedContacts;
        this.rowData = this.contactsData;
        this.stageInfo = contactedContactsMasterInfo.ListOfContactStages;

        // this.contactsService.getContactsStageInfo()
        //   .subscribe(data => {
        //     this.stageInfo = JSON.parse(data.Data);
        //   });
      });
    }
  }

  accountChangeHandler(selectedValue:string){
  if(selectedValue != ""){
    this.accountintiderror = false;
  }
  }

  onAccountKeyup(txtAccountName:string){
    if(txtAccountName != ""){
      this.accountnameerror = false;
    }
  }

  addContact(template: TemplateRef<any>) {  
         
    this.submitted = true;
    if (
      this.IsChecked &&
      (this.contactForm.value.accountname === "" ||
        this.contactForm.value.accountname === null)
    ) {
      this.accountnameerror = true;
      this.accountintiderror = false;
      this.contactForm.patchValue({ accountintid: null });

      // this.contactForm.value.accountname = null;
    } else if (
      !this.IsChecked &&
      (this.contactForm.value.accountintid === "" ||
        this.contactForm.value.accountintid === null)
    ) {
      this.accountnameerror = false;
      this.accountintiderror = true;
    }

    if (this.contactForm.invalid) {
      return;
    }
   
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.contactForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });

    if (
      this.contactForm.value.accountintid != null ||
      this.contactForm.value.accountname != null
    ) {
      this.contactsService
        .AddContact(this.contactForm.value)
        .subscribe(data => {
          if (data.Status === "Success") {          
            this.getContactsData();
            this.responseInfo = `<div class="alert alert-success mb-0">     
            <h6>Contact added successfully..!</h6> 
          </div> `;                
                       
          }else{
            this.responseInfo = `<div class="alert alert-danger mb-0">     
            <h6>Failed to add contact..!</h6> 
          </div> `;             
          }
          this.onReset();
          this.closeAddContactModal.nativeElement.click();
          this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
          setTimeout(() => {
            this.modalResponseRef.hide();
          }, 2000);
        });

    }
  }

  getDesignationsList(){
    this.commonfieldsService.getCommonFields(2,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationsList = JSON.parse(data.Data);
      }
     });
  }

  getDesignationCategoryList(){
    this.commonfieldsService.getCommonFields(4,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationCategoryList = JSON.parse(data.Data);
      }
     });
  }

  assignsequence() {
    this.contactsService
      .assignSequence(this.contactid, this.selectedOption)
      .subscribe(data => {
        if (data.Status === "Success") {
          console.log("added successfully");
        }
      });
  }

  checkValue(event: any) {
    let isChecked = event.target.checked;
    if (isChecked === true) {
      this.IsChecked = true;
    } else {
      this.IsChecked = false;
      this.accountnameerror = false;
    }
    if(this.submitted){
      if(isChecked === true){
        this.IsChecked = true;
        this.accountnameerror = true;
        this.accountintiderror = false;
      }else{
        this.IsChecked = false;
        this.accountnameerror = false;
        this.accountintiderror = true;
      }
    }
  }

  accountsList() {
    this.accountintiderror = false;
  }
  newaccount() {
    this.accountnameerror = false;
  }

  onSelectionChanged() {
    var selectedRows = this.gridApi.getSelectedRows();
    var selectedRowsString = "";
    selectedRows.forEach(function(selectedRow, index) {
      if (index !== 0) {
        selectedRowsString += ", ";
      }
      selectedRowsString += selectedRow.athlete;
    });
    document.querySelector("#selectedRows").innerHTML = selectedRowsString;
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => {
      if (data.Status === "Success") {
        this.Users = JSON.parse(data.Data);
      } else {
      }
    });
  }

  parseDate1(input):any {
    let time = new Date(input);
    return time.getTime();
  }
}
