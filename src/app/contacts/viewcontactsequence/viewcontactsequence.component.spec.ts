import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcontactsequenceComponent } from './viewcontactsequence.component';

describe('ViewcontactsequenceComponent', () => {
  let component: ViewcontactsequenceComponent;
  let fixture: ComponentFixture<ViewcontactsequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcontactsequenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewcontactsequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
