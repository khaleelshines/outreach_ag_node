import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SequenceApiService } from '../../../services/sequenceservice.services';
import { AuthService } from '../../guards/authguard/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../../services/DataService';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-viewcontactsequence',
  templateUrl: './viewcontactsequence.component.html',
  styleUrls: ['./viewcontactsequence.component.css']
})
export class ViewcontactsequenceComponent implements OnInit,OnDestroy { 

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: SequenceApiService,
    private auth: AuthService, private modalService: NgbModal, private dataService: DataService, private route: ActivatedRoute) { }
  StepType: any;
  Days: any;
  Hours: any;
  Mins: any;
  sequenceSteps: any;
  isTaskViewable: any;
  isAutoViewable: any;
  isViewable: any;
  sequenceStepForm: FormGroup;
  contactid: any;
  templates: any;
  subscribers: any;
  responses: any;
  reached: any;
  id: any;
  opened: any;
  public sequenceid;
  public localsequenceid;
  subscriptions:Subscription[]=[];
  @ViewChild('closeAddSequenceStepModal', { static: false }) closeAddSequenceStepModal: ElementRef;
  ngOnInit() {
    this.sequenceStepForm = this.formBuilder.group({
      TaskTitle: [],
      StepType: [],
      Description: [],
      priority: [],
      purpose: [],
      Days: [],
      Mins: [],
      Hours: [],
      notes: [],
      Order: [],
      TemplateId: []
    });
    this.isViewable = false;
    this.isAutoViewable = false;
    this.isViewable = false;
    this.route.params.subscribe(params => {
      this.contactid = params["contactid"];
      this.id = params["id"];
    let sequenceStepsSub =  this.apiService.getSequenceStepWithStatus(this.contactid,this.id)
        .subscribe(data => {
          if (data.Code === 'SUC-200') {
            this.sequenceSteps = JSON.parse(data.Data);
          }
          else {
            this.sequenceSteps = [];
          }
        });
        this.subscriptions.push(sequenceStepsSub);
    });
 
    // this.getSubscribersCount();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onRowClick(event) {
    this.StepType = event.target.value;
    if (this.StepType == "Generic Task") {
      this.isTaskViewable = true;
      this.isViewable = false;
      this.isAutoViewable = false;
    }
    if (this.StepType == "Auto Email") {
      this.isAutoViewable = true;
      this.isTaskViewable = false;
      this.isViewable = false;
    }
    if (this.StepType == "Manual Email") {
      this.isAutoViewable = true;
      this.isTaskViewable = false;
      this.isViewable = false;
    }
    if (this.StepType == "Phone Call") {
      this.isViewable = true;
      this.isAutoViewable = false;
      this.isTaskViewable = false;
    }
  }

  onselectedhours(event) {
    this.Hours = event.target.value;
  }

  onselecteddays(event) {
    this.Days = event.target.value;
  }

  onselectedmins(event) {
    this.Mins = event.target.value;
  }

  // getSubscribersCount() {
  //   this.apiService.getSubscribersCount(this.id).subscribe(data => {
  //     let subscriberscount = JSON.parse(data.Data);
  //     this.subscribers = subscriberscount.Subscribers;
  //     this.reached = subscriberscount.Reached;
  //     this.responses = subscriberscount.Responses;
  //     this.opened = subscriberscount.Opened;
  //   })
  // }


  dataChanged(obj) {
    this.apiService.getTemplate("contacttemplate").subscribe(data => {
      if (data.Status === 'Success') {
        this.templates = JSON.parse(data.Data);
        console.log("Completed");
      }
    });
  }

  getSequencebyId() {
    this.apiService.getSequenceById(this.id)
      .subscribe(data => {
        if (data.Code === 'SUC-200') {
          this.sequenceSteps = JSON.parse(data.Data);
        }
        else {
          this.sequenceSteps = [];
        }
      });
  }


}
