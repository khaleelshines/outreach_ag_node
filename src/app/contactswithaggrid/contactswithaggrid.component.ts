import { Component, OnInit, OnDestroy, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import "ag-grid-enterprise";
import { ContactApiService } from '../../services/contacts.services';
import { IGetRowsParams, IDatasource, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContactsActionsComponent } from '../custom_components/contacts-actions/contacts-actions.component';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AccountApiService } from 'src/services/accounts.services';
import { UserService } from 'src/services/userservice.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { UtilitiesService } from 'src/services/utilities.services';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { AuthService } from '../guards/authguard/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AgGridAngular } from 'ag-grid-angular';
import { stringify } from 'querystring';
declare var jQuery: any;
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-contactswithaggrid',
  templateUrl: './contactswithaggrid.component.html',
  styleUrls: ['./contactswithaggrid.component.css'],
  providers: [DatePipe]
})
export class ContactswithaggridComponent implements OnInit,OnDestroy {
  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public cacheOverflowSize;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: any;
  public rowSelection;
  public recordscount: any;
  public contactsData: any;
  subscriptions:Subscription [] = [];
  pageOfItems: Array<any>;
  paginationPageSize: number;
  cacheBlockSize: number;
  mainTab:string = "created";
  periodType:string = "thisweek";
  engSelect:string = "thisweek";
  crdSelect :string = "thisweek";
  globalSearch:any="";
  selectedDesignationId:number;
  selectedDesignationCat:string;
  designationsList:any;
  designationCategoryList:any;
  contactForm: FormGroup;
  updatecontactForm: FormGroup;
  submitted = false;
  Accounts: any;
  Users: any;
  usersData: any;
  selectedStage: string;
  modalResponseRef:BsModalRef;  
  deletemodalResponseRef:BsModalRef; 
  IsChecked: boolean = false;
  accountintiderror: boolean = false;
  accountnameerror: boolean = false;
  responseInfo:string;
  deleteresponseInfo:string;
  selectedAccountId: string;
  selectedOwnerId: string;
  loggedInUserRole: string;
  IsMember: boolean = false;
  userInfo: any;
  accountsList:any = [];
  selAccountInfo:any;
  selDesigInfo:any;
  assigneeId:number;
  checkboxSelectionCount:number = 0;
  IsUpdateAssigneeEnabled = true;
  errorMessage:string;
  IsErrorOccurred:boolean = false;
  currentAssigneeIntId:number;
  defaultOwnerValue:string="";
  contactIntId:number = 0;
  contactIntIdsList:any;
  //#region dropdown list declaration
  campaigntypeList:any;
  selectedCampaign:string;
  //#endregion

  //#region declaration
  IsNewDesignChecked:boolean = false;
  IsDesignExists:boolean = false;
  IsContactExists:boolean = false; 
  IsAccountExists:boolean = false;
  recordUpdateData : any;
  deleteRecodeId:any;
  deleteContactmodalRef: BsModalRef;
  updateRecodeId:any;

  designationArr:any = [];
  accountNameArr:any = [];
  loading:boolean = false;

  upcdesignationArr:any = [];
  upcaccountNameArr:any = [];
  
  //#endregion

   //#region date filter drop down list
 todayDate:string;
 currentWeekfromto: string;
 lastWeekfromto: string;
 thisMonthfromto: string;
 
 //#endregion

  @ViewChild('agGrid', { static: false }) agGrid: AgGridAngular;
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;
  @ViewChild("closeUpdateContactModal", { static: false })
  closeUpdateContactModal: ElementRef;
  @ViewChild("closeUpdateAssigneeModal", { static: false })
  closeUpdateAssigneeModal: ElementRef;
  public getContextMenuItems(params) {
    let columnName = params.column.colDef.headerName;
    if (columnName === "Actions") {
      return [];
    } else {
      var result = ["copy", "paste", "export", "autoSizeAll"];
      return result;
    }
  }
  get f() {
    return this.contactForm.controls;
  }
  get uf() {
    return this.updatecontactForm.controls;
  }
  @ViewChild("successAlertModal", { static: false })  successAlertModal: ElementRef; 
 

  constructor(private contactsService: ContactApiService,
    private router:Router,
    private spinner: NgxSpinnerService,
    private accounts: AccountApiService,
    private userService: UserService,
    private teamService: teammanagementservice,
    private modalService: BsModalService,
    private auth: AuthService,
    public utilities: UtilitiesService,
    private route:ActivatedRoute,
    private formBuilder: FormBuilder,
    private modalBsService: BsModalService,
    private commonfieldsService:CommonFieldsService,
    public datepipe: DatePipe
    ) {
      this.loading = false;
    this.columnDefs = [
      {
        //<input type='checkbox' class='selectAllCheckBox' (click)='toggleSelectAll()'> 
        headerName: "Name", field: 'contactname', sortable: true,
         filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        headerCheckboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
       checkboxSelection: false,       
           cellRenderer: (data) => {
          if (data.value != undefined) 
          {                    
         
          return '<a href="javascript:void(0);">' + data.value + '</a>';
           }
        }
        },
        {
          headerName: "Account Name",
          field: "AccountName",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          }, 
        },
        {
          headerName: "Industry",
          field: "Industry",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          },  
        },
        {
          headerName: "Last Contact Date",
          field: "LastContactedDate",
          filter: 'agDateColumnFilter',
          filterParams: filterParams
        }, {
          headerName: "Last Contact Type",
          field: "LastContactType",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          },  
        },
        {
          headerName: "# Contacts",
          field: "NoOfContacted", 
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true           
          }
        },
         {
          headerName: "Email",
          field: "email",
          filter: 'agTextColumnFilter', 
          filterParams: {
            suppressAndOrCondition: true
          },
        },
        {
          headerName: "Campaign Type",
          field: "campaigntype",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          }, 
        },
        {
          headerName: "Assigned To",
          field: "AssigneeName",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          }, 
        },
        {
          headerName: "Stage",
          field: "stage",
          filter: 'agTextColumnFilter',
          filterParams: {
            suppressAndOrCondition: true
          }, 
          cellEditor: "agRichSelectCellEditor",
          cellEditorParams: {
            values: [
              "Not Started",
              "Cold",
              "Replied",
              "Unresponsive",
              "do not contact",
              "Bad Contact Info",
              "Interested",
              "Not Interested"
            ]
          }
        },
        {
          headerName: "Actions",
          pinned: "right",
          width: 160,
          suppressMenu: true,
          editable: false,
          sortable:false,
          cellStyle: { "padding-top": "2px" },
          cellRenderer: function(params){           
            return `<div class="d-flex justify-content-center">     
            <span class="view-badge d-inline-block" title="Update" data-action-type="crowupdate" data-toggle="modal" data-target='#updateContactModal'>
            <i class="fa fa-pencil-alt pointer" data-action-type="crowupdate1"></i>
          </span>
            <span class="view-badge d-inline-block" title="Delete" data-action-type="crowdelete" data-toggle="modal" data-target='#confirmationContactDeleteModal'>
              <i class="fa fa-trash" data-action-type="crowdelete1"></i>
            </span>             
            <span class="badge badge-rounded cinfo-badge mr-2 d-inline-block" data-toggle="modal" data-target="#updateAssigneeModal">
            <i class="fas fa-user-cog" data-placement="left" data-toggle="tooltip" title="Update Assignee"></i>
            </span>
          </div>`;
          }
         // cellRendererFramework: ContactsActionsComponent
        }
      // {
      //   headerName: 'Designation', field: 'designation', sortable: true, filter: 'agTextColumnFilter', filterParams: {
      //     suppressAndOrCondition: true
      //   }
      // },
      // {
      //   headerName: 'Email', field: 'email', sortable: true, filter: 'agTextColumnFilter', filterParams: {
      //     suppressAndOrCondition: true
      //   }
      // },
      // {
      //   headerName: 'Contact No', field: 'mobilenumber', sortable: true, filter: 'agTextColumnFilter', filterParams: {
      //     suppressAndOrCondition: true,
      //   }, suppressContextMenu: true
      // }
    ];
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,
     
      // width: 200
    };
   

    this.loggedInUserRole = this.auth.getRole();
   
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }else {
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }
  }
 
  gridOptions: GridOptions = {
    pagination: true,
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52, 
    rowSelection : 'multiple'
  }; 

 
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();
      this.spinner.show();
    let contactSub =  this.contactsService.
    getContactslist(params, this.gridOptions.paginationPageSize,this.globalSearch,
      this.mainTab,this.periodType)
        .subscribe(data => {
          if(data.Code == "SUC-200"){
          this.contactsData = JSON.parse(data.Data);
          params.successCallback(
            this.contactsData.ListOfContacts,
            this.contactsData.ContactsCount
          );          
          }
          this.spinner.hide();          
        });
      this.subscriptions.push(contactSub);
    }
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
   // this.gridApi.sizeColumnsToFit();
    this.deSelectAllSelected();
    this.gridApi.setDatasource(this.dataSource);
    this.deSelectAllSelected();
  }

  onPageSizeChanged(newPageSize) {
    this.deSelectAllSelected();   
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  }

  PaginationChangedHandler(event:any){
    this.gridApi.setDatasource(this.dataSource);
  }

  mainFilterTabsChanged(clickedText:string,mainTab:string) {
   // clickedText = clickedText.toLowerCase(); 
  
    this.mainTab = mainTab.toLowerCase();
    if(clickedText == "notengaged" && mainTab == "notengaged"){
      this.periodType = "notengaged";     
    }else{
      this.engSelect = this.crdSelect = this.periodType = "thisweek";
    }
    this.gridApi.setDatasource(this.dataSource);

  }
  EngagedChangedHandler(clickedText) {
    this.mainTab = "engaged";
    clickedText = clickedText.toLowerCase();
    this.periodType = clickedText;
    this.engSelect = clickedText;
    this.gridApi.setDatasource(this.dataSource);
  }
  CreatedChangedHandler(clickedText) {
    clickedText = clickedText.toLowerCase();
    this.mainTab = "created";
    this.periodType = clickedText;
    this.crdSelect = clickedText;
    this.gridApi.setDatasource(this.dataSource);
  }
  StageChangedHandler(clickedText) {
    clickedText = clickedText.toLowerCase();   
  }
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText.length > 1) {
      this.globalSearch = searchText;   
      
    } else{
      this.globalSearch ="";
    }
    this.gridApi.setDatasource(this.dataSource);
  }

  onSelectionChanged(event: any) {   
    var selectedRows = this.gridApi.getSelectedRows();
    var selectedRowsString = "";
    selectedRows.forEach(function (selectedRow, index) {
      if (index !== 0) {
        selectedRowsString += ", ";
      }
      selectedRowsString += selectedRow.athlete;
    });
    document.querySelector("#selectedRows").innerHTML = selectedRowsString;
  }

  desigSelectHandler(event:any, actionFrom){
    let desigArray =[];
    if(event != "NA" && (event || event.term.length >= 3)){
      
      let searchString = '';
      if(event.term && event.term.length >= 3 && actionFrom=='search'){
       searchString = event.term;
      }else if(event && actionFrom=='update') {
       searchString = event;
      }

    this.commonfieldsService.getCommonFields(2,searchString).subscribe(data=>{
      if (data.Status === "Success") {
        let designations = JSON.parse(data.Data);
        designations.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };       
         
            desigArray.push(singleItem);
        });
        this.designationsList = desigArray; 
        if(actionFrom=='update')   {
          this.selDesigInfo = desigArray[0];
        }   
      }
     });
    }
  }

  ngOnInit() {
    this.datesList();
    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],    
      TeamId: [],
      designationid:[],   
      designation:[],         
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: [],
      campaigntype:["",this.selectedCampaign]
    });

    this.updatecontactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],    
      TeamId: [],
      designationid:[],   
      designation:[],         
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: [],
      campaigntype:["",this.selectedCampaign],
      id:[]  
    });
    
    this.getDesignationCategoryList();
    this.getCampaignTypeList();
   
   let teamSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
    this.subscriptions.push(teamSub);

    this.route.params.subscribe(params=>{
      let dashboardFilter = params['type'];
      if(dashboardFilter != undefined && dashboardFilter != ""){
        switch (dashboardFilter) {
          case 'en-thisweek' || undefined:
            this.EngagedChangedHandler("thisweek");
            break;
          case 'en-thismonth':
            this.EngagedChangedHandler("thismonth");
            this.engSelect = "thismonth";
            break;
          case 'en-today':
            this.EngagedChangedHandler("today");
            this.engSelect = "today";
            break;
          case 'en-lastweek':
            this.EngagedChangedHandler("lastweek");
            this.engSelect = "lastweek";
            break;
          case 'cr-thisweek':
              this.CreatedChangedHandler("thisweek");
              this.crdSelect = "thisweek";
              break;
          case 'cr-lastweek':
              this.CreatedChangedHandler("lastweek");
              this.crdSelect = "lastweek";
              break;
          case 'cr-today':
              this.CreatedChangedHandler("today");
              this.crdSelect = "today";
              break;
          case 'cr-thismonth':
              this.CreatedChangedHandler("thismonth");
              this.crdSelect = "thismonth";
              break;            
          default:
            this.EngagedChangedHandler("thisweek");
            break;
        }
      }
     });
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s=>s.unsubscribe());
  }
  onCellClicked($event: CellClickedEvent) {
    this.currentAssigneeIntId = $event.data.assignedtointid;   
    let id = $event.data.id;
    this.contactIntId = id;
    if($event.colDef.field == "contactname"){    
    this.router.navigate(['/contacts/viewcontact/'+id]);
    }
  }  

  onRowSelected(event) { 
      let selectionStatus = event.node.selected;
      if(selectionStatus == true){ this.checkboxSelectionCount += 1; }
      else{ this.checkboxSelectionCount -= 1; }
       
      if(this.checkboxSelectionCount > 0){ this.IsUpdateAssigneeEnabled = false; }
      else{this.IsUpdateAssigneeEnabled = true;}
  }

  selectStageChangeHandler(selectedValue: string) {
    this.selectedStage = selectedValue;
  }
  selectAccountChangeHandler(selectedValue: string) {
    this.selectedAccountId = selectedValue;
  }

  selectOwnerChangeHandler(selectedValue: string) {
    this.selectedOwnerId = selectedValue;
  }


  //#region Assignee update
  AssigneeChangeHandler(selectedValue: string) {  
    let assingneeId = selectedValue;;

    if(assingneeId != ""){
      if(parseInt(assingneeId) == this.currentAssigneeIntId){
        this.IsErrorOccurred = false;
        this.errorMessage = "Already assingned with same name";
      }else{
      this.IsErrorOccurred = true;
    this.assigneeId = parseInt(assingneeId);
      }
    }else{
      this.IsErrorOccurred = false;
      this.errorMessage = "Please select assingnee";      
    }
   }

   updateAssignee(){    
     let assingeeIntId = this.assigneeId;
     let objectIdsArray = [];
     if(this.contactIntIdsList != null && this.contactIntIdsList.length > 0){
      this.contactIntIdsList.forEach(element => {
        objectIdsArray.push(element.id);
        });
     }else{
      let objectId = this.contactIntId;
      objectIdsArray.push(objectId);
     }
    
     let inputObject = {
      ObjectType: 'contact',
      ObjectIds:objectIdsArray,
      AssignedToId:assingeeIntId
     }
     this.contactsService.updateAssignee(inputObject).subscribe(data => {
      this.closeUpdateAssigneeModal.nativeElement.click();    
      this.gridApi.setDatasource(this.dataSource);
      this.contactIntIdsList = [];
      this.IsUpdateAssigneeEnabled = true;
      this.setDefaultOwner();
      jQuery("#contactsgridSelectAll").prop("checked", false);
      this.agGrid.api.deselectAll();
      this.deSelectAllSelected();
    });
   }

   setDefaultOwner(){
     this.defaultOwnerValue = "";
     this.IsErrorOccurred = true;
   }

   updateBulkAssignees(){
    const newSelectedNodes = this.agGrid.api.getRenderedNodes();  
    const selectedData = newSelectedNodes.map( node => node.data );
    //const selectedNodes = this.agGrid.api.getSelectedNodes();
    //const selectedData = selectedNodes.map( node => node.data );
    this.contactIntIdsList = selectedData;
   }

  //#endregion

   sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }

  
    selectDesignation(){
      this.accountintiderror = false;  
      this.accountnameerror = false; 
      this.IsDesignExists = false;
     this.updatecontactForm.markAsDirty();
      console.log(this.selDesigInfo);   
    } 

    addContact(template: TemplateRef<any>) {
      this.submitted = true;
      if (
        this.IsChecked &&
        (this.contactForm.value.accountname === "" ||
          this.contactForm.value.accountname === null)
      ) {
        this.accountnameerror = true;
        this.accountintiderror = false;
        this.contactForm.patchValue({ accountintid: null });
        return;
        // this.contactForm.value.accountname = null;
      } else if (
        !this.IsChecked &&
        (this.selAccountInfo == undefined || this.selAccountInfo.id == undefined ||
          this.selAccountInfo.id == null)
      ) {
        this.accountnameerror = false;
        this.accountintiderror = true;
        return;
      }

      if (this.contactForm.invalid) {
        return;
      }

      if(this.IsNewDesignChecked && this.contactForm.value.designation != null && this.contactForm.value.designation != undefined){
        this.designationArr = [];
        let newDesignationValue = this.contactForm.value.designation;
        let companyIntegerId = this.userInfo.companyIntId;
        let object ={
          typeid:2,
          labeltitle:newDesignationValue,
          companyIntId:companyIntegerId
        };    
        this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
          if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
            if(parseInt(isDesignExisting) > 0){
              this.IsDesignExists = true;
            }else {
              if(this.IsChecked && (this.contactForm.value.accountname != null || this.contactForm.value.accountname != undefined)){
                this.accountNameArr = [];
                  let newAccountValue = this.contactForm.value.accountname;
                  this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
                    if(data.Code == "SUC-200"){
                        let accountsList = JSON.parse(data.Data);
                        if(accountsList.length > 0){
                          this.IsAccountExists = true;
                        }else {                  
                          this.aftervalidaton(this.contactForm, template);              
                        }
                    }
                  });
              }else {
                this.aftervalidaton(this.contactForm, template);  
              }
            }
          }
        });
      }else {
        if(this.IsChecked && (this.contactForm.value.accountname != null || this.contactForm.value.accountname != undefined)){
          this.accountNameArr = [];
          let newAccountValue = this.contactForm.value.accountname;
          this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let accountsList = JSON.parse(data.Data);
                if(accountsList.length > 0){
                  this.IsAccountExists = true;
                }else {                  
                  this.aftervalidaton(this.contactForm, template);              
                }
            }
          });
        }else {
         this.aftervalidaton(this.contactForm, template);  
        }
      }
  
      if(this.IsContactExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Email already exist. Please try with another..!</h6> 
              </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 3000);
      return;
      }

      if(this.IsDesignExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Designation already exist. Please select from list..!</h6> 
      </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
      }
      if(this.IsAccountExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Account already exist. Please select from list..!</h6> 
      </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
      }   
           
     
    }

    aftervalidaton(contactForm,template){
      let emailValue = contactForm.value.email;  
      this.loading = true;

      if(emailValue){
        this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let isContactExisting = JSON.parse(data.Data);
              if(parseInt(isContactExisting) > 0){
                this.IsContactExists = true;    
                this.loading = false;    
              }else {
                console.log(" procedd to create contact ");
                this.proceedtoCreateContact(contactForm, template);
            }
          }else {
              this.proceedtoCreateContact(contactForm, template);
          }
        });
      } 
    }
    
    proceedtoCreateContact(contactForm, template: TemplateRef<any>){
      let designationId = "";
      let designationName = "";
      if(this.selDesigInfo == undefined || this.selDesigInfo == null){
        designationId = "";
        designationName = this.contactForm.value.designation;
      }else{
        designationId = this.selDesigInfo.id;
        designationName = this.selDesigInfo.name;
      }

      let accountIntId = 0;
      if(this.selAccountInfo == undefined || this.selAccountInfo == null){
        accountIntId = 0;
      }else{
        accountIntId = this.selAccountInfo.id;
      }
      if(this.IsNewDesignChecked){
        designationId = "";
      }

      let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
      this.contactForm.patchValue({ TeamId: TeamInfo.TeamId });
      this.contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
      this.contactForm.patchValue({ accountintid: accountIntId });
      this.contactForm.patchValue({ designationid: designationId });
      this.contactForm.patchValue({ designation: designationName });
      if (
        this.contactForm.value.accountintid != null ||
        this.contactForm.value.accountname != null 
      ) {

        console.log(" in procedd to create contact ");

        this.contactsService
          .AddContact(this.contactForm.value)
          .subscribe(data => {
            this.loading = false;
            if (data.Status === "Success") {
              this.gridApi.setDatasource(this.dataSource);
              this.responseInfo = `<div class="alert alert-success mb-0">     
              <h6>Contact added successfully..!</h6> 
            </div> `;                
                         
            }else{
              this.responseInfo = `<div class="alert alert-danger mb-0">     
              <h6>Failed to add contact..!</h6> 
            </div> `;             
            }
            this.onReset();
            this.closeAddContactModal.nativeElement.click();
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
            setTimeout(() => {
              this.resetaccounts();
              this.resetdesigs();
              this.modalResponseRef.hide();
            }, 2000);
          });      
      }
    }
    
    checkValue(event: any, actionFor:string) {
      let isChecked = event.target.checked;
      if (isChecked === true) {
        this.IsChecked = true;
        this.resetaccounts();
        
        if(actionFor === 'update' && this.updatecontactForm.value.accountname !== "" && this.updatecontactForm.value.accountname !== null){
          let newAccountValue = this.updatecontactForm.value.accountname;
          this.IsAccountExists = false;   

          this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let accountsList = JSON.parse(data.Data);
                if(accountsList.length > 0){
                  this.IsAccountExists = true;
                }
            }
          });

        }else if(this.contactForm.value.accountname !== "" && this.contactForm.value.accountname !== null){
          let newAccountValue = this.contactForm.value.accountname;
          this.IsAccountExists = false;   

          this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let accountsList = JSON.parse(data.Data);
                if(accountsList.length > 0){
                  this.IsAccountExists = true;
                }
            }
          });
        }
      } else {
        this.IsChecked = false;
        this.accountnameerror = false;
        this.IsAccountExists = false;       
      }
      /*
      if(this.submitted){
        if(isChecked === true){
          this.IsChecked = true;
          this.accountnameerror = true;
          this.accountintiderror = false;
        }else{
          this.IsChecked = false;
          this.accountnameerror = false;
          this.accountintiderror = true;
        }
      } */
    } 
  
   //#region  Designations code block
   checkDesignValue(event: any) {
    let isChecked = event.target.checked;
    if (isChecked === true) {
      this.IsNewDesignChecked = true;
      this.designationsList = [];
      this.selDesigInfo = null;      

      if(this.contactForm.value.designation != "" && this.contactForm.value.designation != null && this.contactForm.value.designation != undefined){
        let newDesignationValue = this.contactForm.value.designation;
        let companyIntegerId = this.userInfo.companyIntId;
        let object ={
          typeid:2,
          labeltitle:newDesignationValue,
          companyIntId:companyIntegerId
        };    
        this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
          if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
              if(parseInt(isDesignExisting) > 0){
                this.IsDesignExists = true;
              }
          }
        });
      }
    } else {
      this.IsNewDesignChecked = false;
      this.IsDesignExists = false;
    }    
  } 

  focusOutDesignTxt(event:any,template: TemplateRef<any>){
    let newDesignationValue = event.target.value;
    if(newDesignationValue != "" && newDesignationValue != undefined){
      this.IsDesignExists = false;
      let companyIntegerId = this.userInfo.companyIntId;
      let object ={
        typeid:2,
        labeltitle:newDesignationValue,
        companyIntId:companyIntegerId
      };    
      this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
            if(parseInt(isDesignExisting) > 0){
              this.IsDesignExists = true;
              this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Designation already exist. Please select from list..!</h6> 
              </div> `; 
              this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
              setTimeout(() => {
                this.modalResponseRef.hide();
              }, 2000);
            }
        }
      });
    }
  }
   //#endregion

 //#region Account selection
 selectAccount(){
  this.accountintiderror = false;  
  this.accountnameerror = false; 
  this.updatecontactForm.markAsDirty();
   console.log(this.selAccountInfo);
 }
  accountChangeHandler(selectedObject:any,actionType){
    let accountsArray =[];
    let searchString = '';
    if(selectedObject.term && selectedObject.term.length >= 3 && actionType=='search'){
    searchString = selectedObject.term;
    if(searchString.length >= 3){
      let accountsSub = this.accounts.getBasicAccounts(searchString).subscribe(data => {
       if(data.Status == "Success"){
        let accounts = JSON.parse(data.Data);
        accounts.forEach(element => {
         let singleItem:any;         
           singleItem ={
             id :element.Id,
             name:element.AccountName
           };       
        
           accountsArray.push(singleItem);
       });
       this.accountsList = accountsArray;  
       if(actionType=='update')   {
         this.selAccountInfo = accountsArray[0];
       }         
       }
      });
      this.subscriptions.push(accountsSub);
       }

    }else if(selectedObject && actionType=='update') {
    searchString = selectedObject; //
    if(searchString.length >= 3){
      let accountsSub = this.accounts.getExactAccounts(searchString).subscribe(data => {
       if(data.Status == "Success"){
        let accounts = JSON.parse(data.Data);
        accounts.forEach(element => {
         let singleItem:any;         
           singleItem ={
             id :element.Id,
             name:element.AccountName
           };       
        
           accountsArray.push(singleItem);
       });
       this.accountsList = accountsArray;  
       if(actionType=='update')   {
         this.selAccountInfo = accountsArray[0];
       }         
       }
      });
      this.subscriptions.push(accountsSub);
       }
    }

    
  // if(selectedValue != ""){
  //   this.accountintiderror = false;
  // }
  }

  focusOutAccountTxt(event:any,template: TemplateRef<any>){
    let newAccountValue = event.target.value;
    if(newAccountValue != "" && newAccountValue != undefined){
      this.IsAccountExists = false;       
      this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let accountsList = JSON.parse(data.Data);
            if(accountsList.length > 0){
              this.IsAccountExists = true;
              this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Account already exist. Please select from list..!</h6> 
              </div> `; 
              this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
              setTimeout(() => {
                this.modalResponseRef.hide();
              }, 2000);
            }
        }
      });
    }
  }
 //#endregion

   //#region contact checking
   focusOutEmailTxt(event:any,template: TemplateRef<any>, recordId:number){
    let emailValue = event.target.value;
    if(emailValue != undefined && emailValue.length > 0){
      this.IsContactExists = false;
      this.contactsService.isContactExists(emailValue,recordId).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let isContactExisting = JSON.parse(data.Data);
            if(parseInt(isContactExisting) > 0){ 
              this.IsContactExists = true;            
              this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Email already exist. Please try with another..!</h6> 
              </div> `; 
              this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
              setTimeout(() => {
                this.modalResponseRef.hide();
              }, 3000);
            }
        }
      });
    }
  }
   //#endregion
  //#region  dropdown values
  getDesignationCategoryList(){
    this.commonfieldsService.getCommonFields(4,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationCategoryList = JSON.parse(data.Data);
      }
    });
  }
  getCampaignTypeList(){
    this.commonfieldsService.getCommonFields(3,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.campaigntypeList = JSON.parse(data.Data);
      }
    });
  }

  compTypeChangeHandler(selectedValue: string) {
    this.selectedCampaign = selectedValue;    
  }
  //#endregion

  onReset() {
    this.submitted = false;
    this.accountnameerror = false;
    this.accountintiderror = false;
    this.IsContactExists = false;
    this.IsDesignExists = false;
    this.IsAccountExists = false;
    this.resetdesigs();
    this.resetaccounts();
    this.contactForm.reset();
  }

  OnUpdateReset(){
    this.submitted = false;
    this.IsChecked = false;
    this.accountnameerror = false;
    this.accountintiderror = false;
    this.IsContactExists = false;
    this.IsDesignExists = false;
    this.IsAccountExists = false;
    this.resetdesigs();
    this.resetaccounts();
    this.updatecontactForm.reset();
  }

  onPaginationChanged(event:any){
    let eventValue = event;
    if(eventValue.newPage == true){
    this.agGrid.api.deselectAll();
    this.IsUpdateAssigneeEnabled = true;
    jQuery("#contactsgridSelectAll").prop("checked", false);
    }
  }

  toggleSelectAll(){
    var checked: boolean;
    if (jQuery("#contactsgridSelectAll"). is(":checked")) { 
      checked = true;
     // this.gridApi.clearRangeSelection();
    }else {
      checked = false;
    }
    this.gridApi.forEachNode(function (node) {
      node.setSelected(checked);
    });
    this.IsUpdateAssigneeEnabled = true;
  }

  deSelectAllSelected(){
    this.gridApi.forEachNode(function (node) {
      node.setSelected(false);
    });
    jQuery("#contactsgridSelectAll").prop("checked", false);
  }

  resetaccounts(){
    this.accountsList = []; 
    this.selAccountInfo = null;
  }

  resetdesigs(){
    this.designationsList = []; 
    this.selDesigInfo = null;
    // this.selDesigInfo = {
    //   id :"",
    //   name:""
    // };
  }

  public onRowClicked(e) {
    if (e.event.target !== undefined || e.event.target !== null) {
      let actionType = e.event.target.getAttribute("data-action-type");
      this.recordUpdateData = e.node.data;

      if(actionType == 'crowupdate' || actionType == 'crowupdate1'){
        this.desigSelectHandler(this.recordUpdateData.designation, 'update');
        this.accountChangeHandler(this.recordUpdateData.AccountName, 'update');
        this.updateRecodeId = this.recordUpdateData.id;
        this.resetaccounts();
        this.resetdesigs();
         let formFields = {
          firstname:this.recordUpdateData.firstname,
          lastname:this.recordUpdateData.lastname,
          email:this.recordUpdateData.email,
          mobilenumber:this.recordUpdateData.mobilenumber,
          assignedtointid:parseInt(this.recordUpdateData.assignedtointid)>0?this.recordUpdateData.assignedtointid:null,  
          stage:this.recordUpdateData.stage,
          accountintid:parseInt(this.recordUpdateData.accountintid)>0?this.recordUpdateData.accountintid:null,  
          accountname:this.recordUpdateData.AccountName,
          TeamId:this.recordUpdateData.teamid,
          designationid:this.recordUpdateData.designationid,
          designation:this.recordUpdateData.designation,
          designation_category:this.recordUpdateData.designation_category,
          personallinkedinurl:this.recordUpdateData.personallinkedinurl,
          AssignedTeamIntId:this.recordUpdateData.assignedtointid,
          campaigntype : this.recordUpdateData.campaigntype,
          id:this.recordUpdateData.id
        };
        this.updatecontactForm.setValue(formFields);

      }else if(actionType == 'crowdelete' || actionType == 'crowdelete1'){
        this.deleteRecodeId = this.recordUpdateData.id;
      }
    }
  }

  deleteBtnAction(template: TemplateRef<any>){
    this.deleteContactmodalRef = this.modalService.show(template, { class: "modal-m",backdrop  : 'static' });
  }
  
  private delay(ms: number)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  updateContact(template: TemplateRef<any>) {
    this.submitted = true;
    if (this.IsChecked && (this.updatecontactForm.value.accountname === "" || this.updatecontactForm.value.accountname === null)) {
      this.accountnameerror = true;
      this.accountintiderror = false;
      this.updatecontactForm.patchValue({ accountintid: null });
      // this.updatecontactForm.value.accountname = null;
    } else if (!this.IsChecked && 
      (this.selAccountInfo == undefined || this.selAccountInfo.id == undefined ||
        this.selAccountInfo.id == null)) {
      this.accountnameerror = false;
      this.accountintiderror = true;
    }

    if (this.updatecontactForm.invalid || this.accountintiderror) {
      return;
    }

    if(this.IsNewDesignChecked && (this.updatecontactForm.value.designation != null 
      || this.updatecontactForm.value.designation != undefined || this.updatecontactForm.value.designation != "NA")){
      this.upcdesignationArr = [];
        let newDesignationValue = this.updatecontactForm.value.designation;
        let companyIntegerId = this.userInfo.companyIntId;
        let object ={
          typeid:2,
          labeltitle:newDesignationValue,
          companyIntId:companyIntegerId
        };

        this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
          if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
            if(parseInt(isDesignExisting) > 0){
              this.IsDesignExists = true;
            }else {

              if(this.IsChecked && !this.IsAccountExists && 
                (this.updatecontactForm.value.accountname != null || this.updatecontactForm.value.accountname != undefined)){
                this.accountNameArr = [];
                  let newAccountValue = this.updatecontactForm.value.accountname;
                  this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
                    if(data.Code == "SUC-200"){
                        let accountsList = JSON.parse(data.Data);
                        if(accountsList.length > 0){
                          this.IsAccountExists = true;
                          this.loading = false;
                        }else {                  
                          this.afterUpdatevalidaton(this.updatecontactForm, template);              
                        }
                    }
                  });
              }else {
                this.afterUpdatevalidaton(this.updatecontactForm, template);  
              }
            }
          }
        });
    }else {
      if(this.IsChecked && !this.IsAccountExists && 
        (this.updatecontactForm.value.accountname != null || this.updatecontactForm.value.accountname != undefined)){
        this.accountNameArr = [];
          let newAccountValue = this.updatecontactForm.value.accountname;
          this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let accountsList = JSON.parse(data.Data);
                if(accountsList.length > 0){
                  this.IsAccountExists = true;
                  this.loading = false;
                }else {                  
                  this.afterUpdatevalidaton(this.updatecontactForm, template);              
                }
            }
          });
      }else if(!this.IsAccountExists) {
        this.afterUpdatevalidaton(this.updatecontactForm, template);  
      }
    }

    if(this.IsDesignExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
          <h6>Designation already exist. Please select from list..!</h6> 
        </div> `; 
        this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
        setTimeout(() => {
          this.modalResponseRef.hide();
        }, 2000);
        return;
    }

    if(this.IsContactExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
              <h6>Email already exist. Please try with another..!</h6> 
            </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
    }

    if(this.IsAccountExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
          <h6>Account already exist. Please select from list..!</h6> 
        </div>`; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000); 
      return;
    } 
  }

  afterUpdatevalidaton(contactForm,template){
    let recordId = contactForm.value.id;
    let emailValue = contactForm.value.email;  
    this.loading = true;
    if(emailValue){
      this.contactsService.isContactExists(emailValue,recordId).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let isContactExisting = JSON.parse(data.Data);
            if(parseInt(isContactExisting) > 0){
              this.IsContactExists = true;    
              this.loading = false;    
            }else {
              this.proceedtoUpdateContact(contactForm, template);
          }
        }else {
            this.proceedtoUpdateContact(contactForm, template);
        }
      });
    } 
  }
  
  proceedtoUpdateContact(updatecontactForm, template: TemplateRef<any>){

    console.log(" PROCEED TO UPDATE ", updatecontactForm);

    let designationId = "";    
    let designationName = "";
    if(this.selDesigInfo == undefined || this.selDesigInfo == null){
      designationId = "";
      designationName = updatecontactForm.value.designation;
    }else{
      designationId = this.selDesigInfo.id;
      designationName = this.selDesigInfo.name;
    }

    let accountIntId = 0;
    if(this.selAccountInfo == undefined || this.selAccountInfo == null){
      accountIntId = 0;
    }else{
      accountIntId = this.selAccountInfo.id;
    }   

    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    updatecontactForm.patchValue({ TeamId: TeamInfo.TeamId });
    updatecontactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    updatecontactForm.patchValue({ accountintid: accountIntId });
    updatecontactForm.patchValue({ designationid: designationId });
    updatecontactForm.patchValue({ designation: designationName });
    
   // this.updatecontactForm.patchValue({ id: this.updateRecodeId });
    if (
      updatecontactForm.value.accountintid != null ||
      updatecontactForm.value.accountname != null
    ) {
      this.contactsService
        .UpdateContact(updatecontactForm.value)
        .subscribe(data => {
          this.loading = false;
          if (data.Status === "Success") {          
            this.gridApi.setDatasource(this.dataSource);
            this.responseInfo = `<div class="alert alert-success mb-0">     
                  <h6>Contact updated successfully..!</h6> 
                </div>`;                
          }else{
            this.responseInfo = `<div class="alert alert-danger mb-0">     
                    <h6>Failed to update contact..!</h6> 
                  </div>`;             
          }
          this.IsChecked = false;
          this.OnUpdateReset();
          this.closeUpdateContactModal.nativeElement.click();
          this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
          setTimeout(() => {
            this.modalResponseRef.hide();
          }, 1000);
        });      
    }
  }

  deleteContactYes(template: TemplateRef<any>) {
      let formData = new FormData();
      formData.append('record_id', this.deleteRecodeId);
      formData.append('table_name', 'contacts'); 
      formData.append('action', 'delete'); 

      
      let deleteObject = {
        "tablename":"contacts",
        "ids":[this.deleteRecodeId]
      };
      this.spinner.show();
      this.contactsService.DeleteContact(deleteObject).subscribe(data => {
      if (data.Status === 'Success') {
        this.deleteresponseInfo = `
        <div class="confirmation-content success">
          <div class="icon confirm text-center">
          <i class="far fa-2x fa-check-circle text-success"></i>
          <h4>Deleted Successfully!!</h4>
          </div>            
        </div> `;   
        this.spinner.hide();
        this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
        setTimeout(() => {
          this.modalResponseRef.hide();
          this.gridApi.setDatasource(this.dataSource);
        }, 2000);
      }
    }); 
  }

  //#region for start and end dates functions 
  datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 
}

var filterParams = {
  suppressAndOrCondition: true,
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
};