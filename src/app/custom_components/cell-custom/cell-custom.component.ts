import { Component, OnInit } from '@angular/core';
import {ContactsComponent} from '../../contacts/contacts.component';
import {ContactswithaggridComponent} from '../../contactswithaggrid/contactswithaggrid.component';


@Component({ 
  selector: 'app-cell-custom',
  templateUrl: './cell-custom.component.html',
  styleUrls: ['./cell-custom.component.css']
})
export class CellCustomComponent implements OnInit {
  data: any;
  params: any;
  Sequences: any;
  IsMobileNumberAdded:boolean=false;
  IslinkedToSequence:boolean = false;
  highlightedType:string;
  constructor( private contactdata: ContactsComponent) { }
  agInit(params) {
    this.params = params;
    this.data =  params.value;
     this.IslinkedToSequence = params.data.islinkedtosequence;
     this.highlightedType = params.data.LastContactType.toLowerCase();
   // this.IsMobileNumberAdded = params.data.mobilenumber == "" ? true : false;
    
    }
  ngOnInit() {
    
  }

  BindSequences(){
    // getSequences();
    let contactid = this.params.data.contactid;
    this.contactdata.getSequences(contactid);      
   
  }
  // getSequences() {
  //   this.sequences.getSequences().then(result => {
  //     this.Sequences = result;
  //   });
  // }

  sendEmail(){
    let rowData = this.params;
    let i = rowData.rowIndex;
    alert("Email request clicked..!");
  }
  editRow() {
    let rowData = this.params;
    let i = rowData.rowIndex;
    console.log("Edit row clicked");    
    }
    
    viewRow() {
    let rowData = this.params;
    console.log("view row clicked");
    }

}
