import { Component, OnInit, NgZone } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-activitieschart',
  templateUrl: './activitieschart.component.html',
  styleUrls: ['./activitieschart.component.css']
})
export class ActivitiesChartComponent implements OnInit {
  private chart: am4charts.XYChart;
  constructor(private zone: NgZone) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      // Create chart instance
      let chart = am4core.create("activitiesChart", am4charts.XYChart);

      // Add data
      chart.data = [{
        "year": "Khaleel",
        "europe": 2,
        "namerica": 5,
        "asia": 1

      }, {
        "year": "Shikhar",
        "europe": 6,
        "namerica": 2.7,
        "asia": 2.2

      }, {
        "year": "Deepthi",
        "europe": 8,
        "namerica": 9,
        "asia": 4

      }];

    //  chart.legend = new am4charts.Legend();
    //  chart.legend.position = "top";
      /* Create legend */
chart.legend = new am4charts.Legend();

/* Create a separate container to put legend in */
var legendContainer = am4core.create("legenddiv", am4core.Container);
legendContainer.width = am4core.percent(100);
legendContainer.height = am4core.percent(100);
chart.legend.parent = legendContainer;

chart.events.on("datavalidated", resizeLegend);
chart.events.on("maxsizechanged", resizeLegend);

function resizeLegend(ev) {
  document.getElementById("legenddiv").style.height = chart.legend.contentHeight + "px";
}

      // Create axes
      var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "year";
      categoryAxis.renderer.grid.template.opacity = 0;
     

      var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.renderer.grid.template.opacity = 0;
      valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
      valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
      valueAxis.renderer.ticks.template.length = 10;
      valueAxis.renderer.line.strokeOpacity = 0.5;
      valueAxis.renderer.baseGrid.disabled = true;
      valueAxis.renderer.minGridDistance = 40;

      // Create series
      function createSeries(field, name, color) {
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueX = field;
        series.dataFields.categoryY = "year";
        series.stacked = true;
        series.name = name;
        series.fill = am4core.color(color);
        series.stroke = am4core.color(color);
        series.columns.template.height = 70;

        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
        labelBullet.locationX = 0.5;
        labelBullet.label.text = "{valueX}";
        labelBullet.label.fill = am4core.color("#fff");
      }

      createSeries("europe", "Visits","#20a8d8");     
      createSeries("asia", "Sales","#f86c6b");
      createSeries("namerica", "Leads","#ffc107");
      this.chart = chart;
    });
  }



  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
