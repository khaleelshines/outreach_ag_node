import { Component, OnInit, NgZone } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { dashboardAPIService } from "src/services/dashboard.services";
import { DatePipe } from "@angular/common";
am4core.useTheme(am4themes_animated);

@Component({
  selector: "app-tasks",
  templateUrl: "./tasks.component.html",
  styleUrls: ["./tasks.component.css"]
})
export class TasksComponent implements OnInit {
  private chart: am4charts.XYChart;
  dataObjectCollection: any;
  IsChartDataAvailable: boolean = false;
  constructor(
    private zone: NgZone,
    private taskWeeklyService: dashboardAPIService,
    public datepipe: DatePipe
  ) {}
  ngOnInit() {
    var thisWeekDates = this.weekDates();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.getCurrentWeekTaskInfo(
      this.dateConvertion(currentweekfirstday),
      this.dateConvertion(currentweeklastday)
    );
  }

  chartCode() {
    let chart = am4core.create("taskActivitiesChart", am4charts.XYChart);
    // Add data
    chart.data = this.dataObjectCollection;
    // chart.dataSource.url = this.dataObjectCollection;
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "date";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    //categoryAxis.renderer.grid.template.disabled = true;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.min = 0;
    // valueAxis.renderer.grid.template.disabled = true;

    // Create series
    function createSeries(field, name, color) {
      // Set up series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.name = name;
      series.dataFields.valueY = field;
      series.dataFields.categoryX = "date";
      series.sequencedInterpolation = true;

      // Make it stacked
      series.stacked = true;

      // Configure columns
      series.columns.template.width = am4core.percent(60);
      series.columns.template.tooltipText =
        "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
      series.columns.template.fill = am4core.color(color);
      series.columns.template.stroke = am4core.color(color);
      // Add label
      var labelBullet = series.bullets.push(new am4charts.LabelBullet());
      labelBullet.label.text = "{valueY}";
      labelBullet.label.fill = am4core.color("#fff");
      labelBullet.locationY = 0.5;

      labelBullet.label.adapter.add("textOutput", function(text, target) {
        // Hide labels with 0 value
        if (text === "0") {
          return "";
        }
        return text;
      });

      return series;
    }

    createSeries("emails", "Emails", "#ffa001");
    createSeries("calls", "Calls", "#3cb878");
    createSeries("others", "Others", "#57c1ff");

    // Legend
    chart.legend = new am4charts.Legend();
    /* Create a separate container to put legend in */
    var legendContainer = am4core.create("legenddiv", am4core.Container);
    legendContainer.width = am4core.percent(100);
    legendContainer.height = am4core.percent(100);
    chart.legend.parent = legendContainer;
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, "yyyy-MM-dd");
  }
  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  getCurrentWeekTaskInfo(startdate: string, enddate: string): any {
    this.taskWeeklyService
      .getCurrentWeekTaskInfo(startdate, enddate)
      .subscribe(data => {
        if (data.Status === "Success") {
          this.dataObjectCollection = JSON.parse(data.Data);

          this.chartCode();
        }
      });
  }
}
