import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskApiService } from '../../../services/taskservice.services';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { SequenceComponent } from '../../sequence/sequence.component';
import { GridOptions } from 'ag-grid-community';
import { TasksActionsComponent } from '../../custom_components/tasks-actions/tasks-actions.component';
import * as moment from 'moment';
import { dashboardAPIService } from 'src/services/dashboard.services';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';
import { UserService } from 'src/services/userservice.services';
import { object } from '@amcharts/amcharts4/core';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { UtilitiesService } from 'src/services/utilities.services';

@Component({
  selector: 'app-dashboard360-view',
  templateUrl: './dashboard360-view.component.html',
  styleUrls: ['./dashboard360-view.component.css'],
  providers: [ContactsComponent, SequenceComponent, DatePipe]
})
export class Dashboard360ViewComponent implements OnInit, OnDestroy {
  tasks: any;
  TaskForm: FormGroup;
  Contacts: any;
  taskPriorityInfo: any;
  timepickerVisible = false;
  mytime: Date = new Date();

  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  bsValue = new Date();
  currentWeek: string;
  lastWeek: string;
  currentWeekTaskInfo: any;
  LastWeekTaskInfo: any;
  DashboardMasterInfo: any;
  usersList: any;
  selUserInfo:any;
  subscriptions: Subscription[] = [];

  //#region task declaration
 upcomingTaskInfo:any;
 upcmgSelect = 'todays';
 periodTitle="Today's";
  //#endregion
  public getContextMenuItems(params) {
    let columnName = params.column.colDef.headerName;
    if (columnName === "Actions") {
      return [];
    } else {
      var result = [
        "copy",
        "paste",
        "export",
        "autoSizeAll"
      ];
      return result;
    }
  }
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private apiService: TaskApiService,
    private route: ActivatedRoute,
    private contacts: ContactsComponent,
    private dashboardService: dashboardAPIService,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    public auth: AuthService,
    public utilities:UtilitiesService,
    private taskService:TaskApiService,
    public datepipe: DatePipe) {

    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filter',
          toolPanel: 'agFiltersToolPanel',
        }
      ],
      defaultToolPanel: ''
    }
    this.columnDefs = [
      {
        headerName: "Contact Name",
        field: "FirstName",
        editable: false
      },
      {
        headerName: "Designation",
        field: "Designation",
        editable: false
      }, {
        headerName: "Title",
        field: "TaskTitle",
      }, {
        headerName: "Description",
        field: "TaskDescription",
      },
      {
        headerName: "Priority",
        field: "Priority",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: {
          values: ["Normal", "Low", "Medium", "High", "Urgent"]
        }
      },
      {
        headerName: "Due Date",
        field: "DueDate",

        cellRenderer: (data) => {
          return moment(data.data.DueDate * 1000).format('MM/DD/YYYY HH:mm');
        },

      },
      {
        headerName: 'Actions', pinned: 'right', width: 130, editable: false,
        cellStyle: { 'padding-top': '2px' },
        cellRendererFramework: TasksActionsComponent,
      }
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      width: 200,
      field: "customername",
      valueGetter: function (params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,
      width: 200,

    };
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "always";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
    
  }


  ngOnInit() {    
    this.spinner.show();    
    // let taskSub = this.apiService.getTasks()
    //   .subscribe(data => {
    //     if (data.Status === 'Success') {
    //       let taskMasterData = JSON.parse(data.Data);
    //       this.tasks = taskMasterData.ListOfTasks;
    //       this.taskPriorityInfo = taskMasterData.ListOfPriorities;
    //     }
    //   });
    // this.subscriptions.push(taskSub);
    
    this.getDashboardInfo(0);
    
    //#region upcoming tasks
    this.upcommingTaskService('today');
    //#endregion
    var thisWeekDates = this.weekDates();

    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeek = currentweekfirstday.toString().split(year.toString())[0] + " - " + currentweeklastday.toString().split(year.toString())[0];

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeek = lastweekfirstday.toString().split(year.toString())[0] + " - " + lastweeklastday.toString().split(year.toString())[0];

   this.TaskForm = this.formBuilder.group({
      ObjectId: [],
      Priority: [],
      Title: [],
      AssignedTo: [],
      DueDate: [],
      DueTime: [],
      Description: []

    });


   // this.getCurrentWeekTaskInfo(this.dateConvertion(currentweekfirstday), this.dateConvertion(currentweeklastday));
   // this.getLastWeekTaskInfo(this.dateConvertion(lastweekfirstday), this.dateConvertion(lastweeklastday));

   this.getUsers();
  }

  resetDashboard(){
    this.getDashboardInfo(0);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.apiService.getTasks()
      .subscribe(data => {
        if (data.Status === 'Success') {
          let taskMasterData = JSON.parse(data.Data);
          this.tasks = taskMasterData.ListOfTasks;
          this.taskPriorityInfo = taskMasterData.ListOfPriorities;
          this.rowData = this.tasks;
        }
      });
  }

  applypriorityfilter(clickedText) {
    clickedText = clickedText.toLowerCase();
    this.apiService.getTasks()
      .subscribe(data => {
        if (data.Status === 'Success') {
          let taskMasterData = JSON.parse(data.Data);

          if (clickedText == "all") {
            this.tasks = taskMasterData.ListOfTasks;
          } else {
            this.tasks = taskMasterData.ListOfTasks.filter(a => a.Priority.toLowerCase() == clickedText);
          }
        }
      });

  }

  onCellValueChanged(event) {
    let taskObj = event.data;
    let modifiedValue = event.newValue;
    let oldValue = event.oldValue;
    let key = this.getKeyByValue(taskObj, modifiedValue);
    let taskdata: any = {
      TaskId: taskObj.TaskId,
      FieldName: key,
      FieldValue: modifiedValue
    }
    if (modifiedValue != "" && modifiedValue != oldValue) {
      this.apiService.updateTaskInfo(taskdata).subscribe(data => {
        if (data.Status === 'Success') {
          console.log('Completed');
        }
      });
    }

  }
  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  selectMember() {
    console.log(this.selUserInfo);   
     this.getDashboardInfo(this.selUserInfo.id);
     this.upcommingTaskService('today');
  }  

  getUsers() {
    let usersArray =[];
    this.userService.getUsers().subscribe(data => {
      if (data.Status === "Success") {
        let userData = JSON.parse(data.Data);
 
        userData.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.IntUserId,
              name:element.Name,
            };       
         
          usersArray.push(singleItem);
        });
        this.usersList = usersArray;
       }
    });
  }
  

  // getCurrentWeekTaskInfo(startdate: string, enddate: string) {
  //   this.dashboardService.getTaskWeeklyCountInfo(startdate, enddate)
  //     .subscribe(data => {
  //       if (data.Status === "Success" && data.Data != "") {
  //         let taskWeeklyData = JSON.parse(data.Data);
  //         this.currentWeekTaskInfo = taskWeeklyData;
  //       }
  //     });
  // }

  // getLastWeekTaskInfo(startdate: string, enddate: string) {
  //   this.dashboardService.getTaskWeeklyCountInfo(startdate, enddate)
  //     .subscribe(data => {
  //       if (data.Status === "Success" && data.Data != "") {
  //         this.LastWeekTaskInfo = JSON.parse(data.Data);
  //       }
  //     });
  // }

  // getCurrentWeekTaskData(startdate: string, enddate: string): any {
  //   this.dashboardService.getCurrentWeekTaskInfo(startdate, enddate)
  //     .subscribe(data => {
  //       if (data.Status === "Success") {
  //         return JSON.parse(data.Data);
  //       }
  //     });
  // }

  dbItemSelected(type: string, module:string) {
    this.router.navigate([
      "/"+module+"/" + type
    ]);
    // switch (type) {
    //   case 'en-today':
    //   this.router.navigate([
    //     "/contacts/" + type
    //   ]);
    //     break;

    //   default:
    //     break;
    // }
  }

  getDashboardInfo(memberId:number): any {
    this.spinner.show();
    let dashboardSub = this.dashboardService.getDashboardMasterInfo(memberId)
      .subscribe(data => {
        if (data.Status === "Success") {
          this.DashboardMasterInfo = JSON.parse(data.Data);
        }
        this.spinner.hide();
      });
    this.subscriptions.push(dashboardSub);
  }

  //#region getting task info

  tasksChangedHandler(selectedText:string){
    this.spinner.show();
    this.upcommingTaskService(selectedText);
  }
  upcommingTaskService(selectedPeriod:string){
      var thisWeekDates=this.weekDates();

    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeknextday = thisWeekDates.endDate;   
  
    
    var nextWeekDates=this.nextWeekDates();
    var nextweekfirstday = nextWeekDates.startDate; // 06-Jul-2014
    var nextweeknextday = nextWeekDates.endDate;

    let currentweekfirstdate = this.dateConvertion(currentweekfirstday);
    let currentweeknextdate = this.dateConvertion(currentweeknextday);

    let nextweekfirstdate = this.dateConvertion(nextweekfirstday);
    let nextweeknextdate = this.dateConvertion(nextweeknextday);
    let currentDate = this.dateConvertion(new Date());
    let startdueDate:string;
    let enddueDate:string;
    let taskStatus:string = "";
    this.periodTitle = selectedPeriod;
   
   switch (selectedPeriod) {
     case 'today':
      case 'todays':
       startdueDate = currentDate;
      enddueDate= currentDate;     
       break;
   case 'thisweek':
      startdueDate = currentweekfirstdate;
      enddueDate= currentweeknextdate;     
     break;
     case 'nextweek':
        startdueDate = nextweekfirstdate;
        enddueDate= nextweeknextdate; 
       break;
       case 'due':
        startdueDate = "";
        enddueDate= ""; 
        taskStatus= "overdue";
       break;
       case 'all':
        startdueDate = "";
       enddueDate= ""; 
       taskStatus= "all";    
        break;
     default:
        startdueDate = "";
      enddueDate= ""; 
      taskStatus= "all";    
       break;
   }
   let memberId = 0;
   if(this.selUserInfo != undefined){
   memberId = this.selUserInfo.id > 0 ? this.selUserInfo.id:0;
   }
      let inputObject = {
        "StartDueDate":startdueDate,
        "EndDueDate":enddueDate,
        "DueDateType":"range",
        "AssignedTo":memberId,
        "TeamId": 0,
        "FilterStatus":taskStatus,
        "Purpose":"dashboard"
      }
    console.log(inputObject);
      this.taskService.getTasksWithFilters(inputObject)
      .subscribe(data => {
        if (data.Status === 'Success') {
          if(data.Data != ""){
          let taskMasterData = JSON.parse(data.Data);       
          let tasksList = taskMasterData.ListOfTasks;    
          if(tasksList.length > 0){
            this.upcomingTaskInfo = tasksList.slice(0, 10);
          }    
          }else{
            this.upcomingTaskInfo = [];
          }
    } 
    if(selectedPeriod != 'today'){
      this.spinner.hide();
    }
  
});
    }

     //#endregion
  }
