import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailApiService } from 'src/services/emails.services';

@Component({
  selector: 'app-emailcontent',
  templateUrl: './emailcontent.component.html',
  styleUrls: ['./emailcontent.component.css']
})
export class EmailcontentComponent implements OnInit {
 urlId:string;
  constructor(private route: ActivatedRoute,private emailService:EmailApiService, private router:Router) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.urlId = params["id"];
      this.getClickedUrlInfo();
    });
  }

  getClickedUrlInfo(){
    this.emailService.getEmailURLContentInfo(this.urlId)
    .subscribe(data => {
      if (data.Status === 'Success') {
        let urlInfo = JSON.parse(data.Data);
        this.emailService.updateEmailURLContentClicks(this.urlId,urlInfo.clickscount)
        .subscribe(data=>{
          window.location.href =  urlInfo.targeturl;

        });
       
      }
    });
  }

}
