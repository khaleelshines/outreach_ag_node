import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EmailApiService } from 'src/services/emails.services';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.css']
})
export class UnsubscribeComponent implements OnInit {
  private routeSub: Subscription;
  contactId:string;
  isUnsubscribed:boolean= false;
  constructor(private route: ActivatedRoute, private emailService:EmailApiService,private router: Router) { }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {    
     this.contactId = params['id'];
   });
  }

  UnSubscribeEmails(){
    this.emailService.unSubscribeMails(this.contactId).subscribe(data=>{
      if (data.Code === 'SUC-200') {
          this.isUnsubscribed = true;
      }
    });
  }

  goBackToHome(){
    window.opener = self;
    window.close(); 
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

}
