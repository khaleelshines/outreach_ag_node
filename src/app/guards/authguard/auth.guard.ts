import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { UtilitiesService } from 'src/services/utilities.services';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate{
  constructor(private auth: AuthService,
    private utilities:UtilitiesService,
    private myRoute: Router){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.auth.isLoggedIn()){
        let loginTime = localStorage.getItem('logintime');
        let currentTime = new Date().getTime();
        let differenceTime = this.utilities.getDifference(currentTime,parseInt(loginTime));       
        if(differenceTime > 3){
          localStorage.setItem('logintime',"0");
            this.auth.logout();
        }else{
          let configCheck = this.auth.getEmailConfig();
          // if(configCheck == "true"){
          //   this.myRoute.navigate(["user/profile"]);
          // }else{
            return true;
         // }       
        }
      }else{
        this.myRoute.navigate(["login"]);
        return false;
      }
  }
}
