import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from 'src/services/userservice.services';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private myRoute: Router,private _location: Location,private userService:UserService) { }
  sendToken(token: string) {
    localStorage.setItem("LoggedInUser", token)
  }
  getToken() {
    return localStorage.getItem("LoggedInUser")
  }

  storeIntUserId(intuserid: string) {
    localStorage.setItem("LoggedInUserId", intuserid);
  }
  getIntUserId() {
    return localStorage.getItem("LoggedInUserId");
  }

  sendRole(role:string){
    localStorage.setItem("UserRole",role);
  }
  getRole(){
    return localStorage.getItem("UserRole");
  } 

  setEmailConfig(isConfigured:string){
    localStorage.setItem("IsConfigured",isConfigured);
  }
  getEmailConfig(){
    return localStorage.getItem("IsConfigured");
  } 
  setIsFirstTimeRedirection(isFirstTime:string){
    localStorage.setItem("IsFirstRedirection",isFirstTime);
  }
  getIsFirstTimeRedirection(){
    return localStorage.getItem("IsFirstRedirection");
  } 
  isLoggedIn() {
    return this.getToken() !== null;
  } 

  logout() {
    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("UserRole");
    
    this.myRoute.navigate(["login"]);
  }

  backClicked() {
    this._location.back();
  }
}
