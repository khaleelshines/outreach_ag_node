import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { UtilitiesService } from 'src/services/utilities.services';
declare var jQuery: any;
@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.css']
})
export class AdminheaderComponent implements OnInit {
  MenuInfo = JSON.parse(localStorage.getItem('menuInfo'));
  selectedItem: any;
  userInfo:any;
  logoClickedUrl:string="#";
  constructor(public auth: AuthService,public utilities:UtilitiesService) { }

  ngOnInit() {
    this.utilities.getRandomColor();
    let role = this.auth.getRole();
    let isLoggedIn = this.auth.isLoggedIn();
    let userDetails = localStorage.getItem('userInfo');
    if (userDetails != null && userDetails != "") {
      let userDetails1 = JSON.parse(userDetails);
      let loggedInUserRole = "";
      switch (userDetails1.ObjectType.toLowerCase()) {
        case 'ta':
          loggedInUserRole = "Team Admin";
          break;
        case 'admin':
          loggedInUserRole = "Admin";
          break;
        default:
          loggedInUserRole = "Member";
          break;
      }
      this.userInfo = { name: userDetails1.Name, role: loggedInUserRole }
    }
    if (isLoggedIn) {
      if (role === "admin") {
        this.logoClickedUrl = '/dashboard';
      } else {
        this.logoClickedUrl = '/dashboard/member';
      }
    }

    jQuery(document).ready(function ($) {
      /*Sidebar Navigation*/
      $("#toggle_nav_btn").on("click", function () {
        $(".wrapper").toggleClass('slide-nav-toggle');
      });
      $("#sbmenu").click(function () {
        $(".fixed-sidebar-left").toggleClass("expand");
        $(".page-wrapper").toggleClass("expand");
      });
    });

  }

}
