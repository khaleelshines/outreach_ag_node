import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderwithoutmenuComponent } from './headerwithoutmenu.component';

describe('HeaderwithoutmenuComponent', () => {
  let component: HeaderwithoutmenuComponent;
  let fixture: ComponentFixture<HeaderwithoutmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderwithoutmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderwithoutmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
