import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/userservice.services';
import { AuthService } from '../guards/authguard/auth.service';
import { timeout } from 'rxjs/operators';
import { UtilitiesService } from 'src/services/utilities.services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  invalidLogin = false;
  submitted = false;
  showbutton = false;
  showeye = false;
  ModuleInfo: any[] = [];
  loginResponseObject: any;
  loading = false;
  isSessionExpired:boolean = false;

  get f() { return this.loginForm.controls; }
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private auth: AuthService,
    private utilities:UtilitiesService) { 

  }
  showPassword() {
    this.showbutton = !this.showbutton;
    this.showeye = !this.showeye;
  }
  forgotPassword() {

  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    const loginPayload = {
      loginId: this.loginForm.controls.loginId.value,
      password: this.loginForm.controls.password.value
    };

    this.userService.login(this.loginForm.value).subscribe(data => {
      console.log(" DDDDDDDDDDDDDDD "+JSON.stringify(data));

      if (data.Code === 'SUC-200') {
        this.loginResponseObject = JSON.parse(data.Data);
        const userInfo = JSON.parse(data.Data);
        console.log(userInfo);
        let emailConfiguration = userInfo.IsEmailConfigured?"true":"false";
        this.auth.setEmailConfig(emailConfiguration);
        this.auth.setIsFirstTimeRedirection("yes");
        localStorage.setItem('token', this.loginResponseObject.UserToken);
        localStorage.setItem('logintime',new Date().getTime().toString());
        localStorage.setItem('menuInfo', JSON.stringify(this.loginResponseObject.MenuInfo));
        localStorage.setItem('teaminfo', JSON.stringify(this.loginResponseObject.TeamInfo));
        localStorage.setItem('userInfo', data.Data);
        if (userInfo.UserToken !== '' && userInfo.UserToken != null) {
          this.auth.sendToken(userInfo.UserToken);
          this.auth.storeIntUserId(userInfo.IntUserId);
          this.auth.sendRole(userInfo.ObjectType.toLowerCase());   
          this.userService.nodelogin(this.loginForm.value).subscribe(data => {
            if (data.Code === 'SUC-200') {
               let nodeLoginInfo = JSON.parse(data.Data);
               localStorage.setItem('nutoken', nodeLoginInfo.UserToken);
               console.log("node login response:"+nodeLoginInfo.UserToken);
               this.router.navigate(['/task']);
             }
          });   
        } else {
          this.loading = false;
          this.invalidLogin = true;
          this.router.navigate(['/login']);
        }
      } else {
        this.loading = false;
        this.invalidLogin = true;
        this.router.navigate(['/login']);
      }
    });
  }
  ngOnInit() {
    

    this.loginForm = this.formBuilder.group({
      loginId: ['', Validators.compose([Validators.required])],
      password: ['', [Validators.compose([Validators.required,Validators.maxLength(20)])]],
     
    });

    let loginTime = localStorage.getItem('logintime');
    if(parseInt(loginTime) < 1){
      this.isSessionExpired = true;
       setTimeout(() => {
        this.isSessionExpired = false;
        localStorage.setItem('logintime','1')
       }, 3000);
    }

    let isLoggedIn = this.auth.isLoggedIn();
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }
}


