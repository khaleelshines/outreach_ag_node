import { Component, OnInit, OnDestroy } from '@angular/core';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilitiesService } from 'src/services/utilities.services';
import { UserService } from 'src/services/userservice.services';

import "ag-grid-enterprise";
import { AgGridAngular } from 'ag-grid-angular';
import { IGetRowsParams, IDatasource, GridOptions, CellClickedEvent } from 'ag-grid-community';

@Component({
  selector: 'app-usersequence',
  templateUrl: './usersequence.component.html',
  styleUrls: ['./usersequence.component.css']
})

export class UsersequenceComponent implements OnInit,OnDestroy {
  Sequences: any;
  usersList:any;
  selUserInfo:any;
  IsMember:boolean;
  userInfo:any;
  subscriptions:Subscription [] = [];

  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public cacheOverflowSize;
  public rowData: any;
  public rowSelection;
  public recordscount: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  private params: any;
  selectedUserId = 0;
  
  mainTab:string = "active";

  constructor(
    private sequenceService: SequenceApiService,
    public auth: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public utilities:UtilitiesService,
    public userService:UserService)
    {
      this.columnDefs = [
        { 
          headerName: "Sequence Name", 
          field: 'SequenceName', 
          sortable: true, 
          filter: 'agTextColumnFilter',
          width:400,
          filterParams: { suppressAndOrCondition: true },
          cellRenderer: (data) => { if (data.value != undefined) { return '<a href="javascript:void(0);">' + data.value + '</a>'; } }
        },
        {
          headerName: "Steps", 
          field: "StepsCount", 
          sortable: true, 
          width:120, 
        },
        {
          headerName: "Status", 
          field: "SequenceStatus",
          filter:true,
          sortable: true, 
          width:120,
          cellStyle: function(params) {  
            if(params.value == "Active") {
              return { 'background-color': '#82d249','color':'#fff' };
            }else if(params.value == 'Completed'){
              return { 'background-color': '#ffb300','color':'#fff' };
            } else{
              return { 'background-color': '#fb7065','color':'#fff' };
            }   
          }
        },
        {
          headerName: "#Contacts", 
          field: "Subscribers", 
          sortable: true, 
          width:130, 
        },
        {
          headerName: "#Reached", 
          field: "Reached", 
          sortable: true, 
          width:120
        },
        {
          headerName: "#Opened", 
          field: "Opened", 
          sortable: true,
          width:120
        },
        { 
          headerName: "#Responses", 
          field: "Responses", 
          sortable: true, 
          width:150
        },
        {
          headerName: "Industry", 
          field: "Industry", 
          sortable: true, 
          width:150
        },
      ];

      let loggedInUserRole = this.auth.getRole();
   
      if (loggedInUserRole === "member") {
        this.IsMember = true;
        let userInfoData = localStorage.getItem("userInfo");
        let userDetails = JSON.parse(userInfoData);
        this.userInfo = {
          name: userDetails.Name,
          IntUserId: userDetails.IntUserId
        };
      }
    
      this.defaultColDef = {
        editable: true,
        enableRowGroup: true,
        enablePivot: false,
        enableValue: true,
        sortable: true,
        resizable: true,
        filter: true,
      };
      
      if(this.IsMember){
        this.selectedUserId = this.userInfo.IntUserId;
      }
    } 

  gridOptions: GridOptions = {
    pagination: true,
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 55,
    headerHeight: 52, 
    rowModelType: 'infinite',
    angularCompileHeaders: true
  }; 
  /*
  getUserSequence(userIntId:number){
    this.spinner.show();
    let userSeqSub =  this.sequenceService.getUserSequence(userIntId,this.mainTab).subscribe(data => {
        if (data.Code === "SUC-200") {       
          this.Sequences = JSON.parse(data.Data);
        }else{
          this.Sequences = [];
        }
        this.spinner.hide();
      });
      this.subscriptions.push(userSeqSub);
  } */

 
    dataSource: IDatasource = {
      getRows: (params: IGetRowsParams) => {
        const savedModel = this.gridApi.getFilterModel();
        this.spinner.show();
        this.sequenceService.getUserSequence(this.selectedUserId,this.mainTab)
          .subscribe(data => {
            if(data.Code == "SUC-200"){
            this.Sequences = JSON.parse(data.Data);         
            }else{
              this.Sequences = [];
            }
            this.spinner.hide();          
            // /this.subscriptions.push(userSeqSub);
          });
      
      }
    }
  
    agInit(params: any): void {
      this.params = params;
    }

    onGridReady(params: any) {
      this.gridApi = params.api;
      this.gridApi.setDatasource(this.dataSource);
    }

    onPageSizeChanged(newPageSize) {
      this.gridApi.paginationSetPageSize(Number(newPageSize));
      this.gridApi.setDatasource(this.dataSource);
    }

    ngOnInit() {
      this.getUsers();
    }

    ngOnDestroy(): void {
      this.subscriptions.forEach((subscription)=>subscription.unsubscribe());
    }

    onFilterTextBoxChanged(event: any) {
      let searchText = event.currentTarget.value;
      if (searchText != "") {
        this.gridOptions.api.setQuickFilter(searchText);
      } else {
        this.gridOptions.api.setQuickFilter(null);
      }
    }

  getUserSequence(userIntId:number){
    this.spinner.show();
    let userSeqSub =  this.sequenceService.getUserSequence(userIntId,this.mainTab).subscribe(data => {
        if (data.Code === "SUC-200") {       
          this.Sequences = JSON.parse(data.Data);
        }else{
          this.Sequences = [];
        }
        this.spinner.hide();
      });
      this.subscriptions.push(userSeqSub);
  }

  getUsers() {
    let usersArray =[];
    this.userService.getUsers().subscribe(data => {
      if (data.Status === "Success") {
        let userData = JSON.parse(data.Data);
 
        userData.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.IntUserId,
              name:element.Name,
            };         
          usersArray.push(singleItem);
        });
        this.usersList = usersArray;
       }
    });
  }

  selectMember() {
    this.selectedUserId =this.selUserInfo.id;
    this.gridApi.setDatasource(this.dataSource);
  }

  resetSequence(){
    this.selectedUserId = 0;
    this.gridApi.setDatasource(this.dataSource);
  }

  onCellClicked($event: CellClickedEvent) {
    let id = $event.data.SequenceIdInt;
    if($event.colDef.field == "SequenceName"){    
    this.router.navigate(['/sequenceview/'+id]);
    }
  } 

  
  mainFilterTabsChanged(clickedText:string,mainTab:string) {     
    this.mainTab = mainTab.toLowerCase();
    let userIntId = 0;
    if(this.IsMember){
      userIntId = this.userInfo.IntUserId;
    }
    this.getUserSequence(userIntId);
    this.getUsers();
  }

}