import { Component, OnInit, OnDestroy } from '@angular/core';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { GridOptions, CellMouseOverEvent } from "ag-grid-community";
import { ActivatedRoute } from '@angular/router';
import { ContactApiService } from 'src/services/contacts.services';
import { UtilitiesService } from 'src/services/utilities.services';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-viewusersequence',
  templateUrl: './viewusersequence.component.html',
  styleUrls: ['./viewusersequence.component.css']
})
export class ViewusersequenceComponent implements OnInit,OnDestroy {
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  paginationPageSize: number;

SequenceSteps:any;
SequenceContacts:any;
sequenceIntId:number;
sequencename:string;
sequenceIndustryName:string;
//#region common declaration
stepsList:any;
stepId:number;
stepSelect:string;
mainTab:string = 'overview';
//#endregion

subscriptions:Subscription [] = [];
  constructor(
    private sequenceService: SequenceApiService,
    private contactService:ContactApiService,
    private route:ActivatedRoute,
    public utilities: UtilitiesService,
    private spinner: NgxSpinnerService,
  ) {

    this.columnDefs = [
      {
        headerName: "Contact Name",
        field: "contactname",
        cellRenderer: function(params) {
          return (
            '<a href="contacts/viewcontact/' +
            params.data.id +
            '""target="_blank">' +
            params.value +
            "</a>"
          );
        },
       tooltipField: "contactname",
       tooltipComponentParams: { color: "#ececec" }
      },
      {
        headerName: "Account Name",
        field: "AccountName"
      },
      {
        headerName: "Step Subject",
        field: "StepSubject"
      },
      {
        headerName:"Scheduled Date",
        field:"ImplementationDate",
        sortable:true
      },      
      {
        headerName: "Step Type",
        field: "StepType"
      },
      {
        headerName: "Designation",
        field: "designation"
      },
      {
        headerName: "Email",
        field: "email"
      },
      {
        headerName: "Mobile Number",
        field: "mobilenumber"
      }
    ];
    
    this.paginationPageSize = 10;
   }
 
   gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52
  };
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);
      // this.gridOptions.api.onFilterChanged();
    }
  }


  ngOnInit() {
    // this.spinner.show();

    // setTimeout(() => {
    //   this.spinner.hide();
    // }, 3000);
    this.route.params.subscribe(params => {
      this.sequenceIntId = params["id"];
      
      this.getStepsInfo();
     // this.getSequenceContactsMethod();
    });
    this.getUserSequence();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;   
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  // getContacts(){
  //   alert("ok");
  //   //this.getSequenceContactsMethod();
  // }

  stepContacts(stepId:number){
    this.getSequenceContactsMethod(stepId);
    this.stepSelect = stepId.toString();
    this.mainTab = 'contacts';
  }
  mainFilterTabsChanged(selectedTabValue:string){
    if(selectedTabValue == "contacts"){
console.log(" STEPSLIST"+JSON.stringify(this.stepsList));

      this.getSequenceContactsMethod(this.stepsList[0].StepId);
      this.stepSelect = this.stepsList[0].StepId;
    }else{
      this.mainTab = 'overview';
    }
  }

  getUserSequence(){
   this.spinner.show();
  let userSeqSub =  this.sequenceService.getUserSequenceSteps(this.sequenceIntId).subscribe(data => {
      if (data.Code === "SUC-200") {       
       let masterData = JSON.parse(data.Data);
       this.SequenceSteps = masterData.ListOfStepsData;
       this.sequencename = masterData.SequenceName;
       this.sequenceIndustryName = masterData.Industry;
      }
      this.spinner.hide();
    });
    this.subscriptions.push(userSeqSub);
    
  }

  StepChangedHandler(selectedId:number){
    this.getSequenceContactsMethod(selectedId);
  }
  
  getStepsInfo(){
    let seqStepsInfoSub = this.sequenceService.getSequenceStepsInfo(this.sequenceIntId).subscribe(data => {
       if (data.Code === "SUC-200") {       
        this.stepsList = JSON.parse(data.Data);
       }
     });
     this.subscriptions.push(seqStepsInfoSub);
   }

  getSequenceContactsMethod(stepId:number){
    this.spinner.show();
   let seqContactsSub = this.contactService.getStepContacts(this.sequenceIntId,stepId).subscribe(data => {
      if (data.Code === "SUC-200") {       
       this.SequenceContacts = JSON.parse(data.Data); 
      }else{
        this.SequenceContacts = [];
      }
      this.spinner.hide();
    });
    this.subscriptions.push(seqContactsSub);
  }

}
