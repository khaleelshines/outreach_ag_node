import { Injectable, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { DecimalPipe, DatePipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { OpportunityApiService } from 'src/services/opportunities.services';


interface Opportunity {
  OpportunityId: string;
  CompanyName: string;
  ContactName: string;
  ContactDesignation: string;
  AccountName: string;
  ShortContactName: string;
  Title: string;
  Stage: string;
  Status: string;
  TargetDate: string;
  Value: number;
  CreatedDate: string;
}
interface stage {
  stage: string;
  stagecount: number;
}
interface SearchResult {
  opportunities: Opportunity[];
  total: number;
  stageList1: stage[];
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: string;
  stageSearch: string;
}

function matches(opportunity: Opportunity, term: string, pipe: PipeTransform) {
  return opportunity.ContactName.toLowerCase().includes(term.toLowerCase())
    || opportunity.ContactDesignation.toLowerCase().includes(term.toLowerCase())
    || opportunity.AccountName.toLowerCase().includes(term.toLowerCase())
    || opportunity.Title.toLowerCase().includes(term.toLowerCase())
    || opportunity.Stage.toLowerCase().includes(term.toLowerCase())
    || opportunity.Status.toLowerCase().includes(term.toLowerCase());
}


@Injectable()
export class OpportunityService1 {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _opportunities$ = new BehaviorSubject<Opportunity[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private _stageList = new BehaviorSubject<stage[]>([]);
  private opportunityList: any;
  private stageList: any;
  private masterOpportunityData: any;
  private loggedInUserRole: string;
  private loggedInIntUserId: string;
  IsDataAvailable: boolean = true;
  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: '',
    stageSearch: ''
  };


  constructor(private pipe: DecimalPipe, private http: HttpClient, private opportunityService: OpportunityApiService,
    public datepipe: DatePipe, private auth: AuthService) {


  }



  addOpportunityReload() {
    this.TimePeriodFilter('thisweek');
    // this.opportunityService.GetOpportunities()
    //   .subscribe(data => {
    //     if (data.Status === 'Success') {
    //       let opportunityMasterData = JSON.parse(data.Data);
    //       this.opportunityList = opportunityMasterData.ListOfOpportunities;
    //       this.stageList = opportunityMasterData.ListOfOpportunityStages;
    //       this.masterOpportunityData = opportunityMasterData;
    //       this._search$.pipe(
    //         tap(() => this._loading$.next(true)),
    //         debounceTime(0),
    //         switchMap(() => this._search()),
    //         delay(0),
    //         tap(() => this._loading$.next(false))
    //       ).subscribe(result => {
    //         this._opportunities$.next(result.opportunities);
    //         this._total$.next(result.total);
    //         this._stageList.next(result.stageList1);
    //       });
    //       this._search$.next();
    //     }
    //   });
  }

  TeamAdminOpportunityReload() {
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    let inputObject = {
      "TeamId": TeamInfo.Id
    }

    this.opportunityService.GetOpportunitiesWithFileters(inputObject)
      .subscribe(data => {
        if (data.Status === 'Success') {
          if (data.Data != "") {
            let opportunityMasterData = JSON.parse(data.Data);
            this.opportunityList = opportunityMasterData.ListOfOpportunities;
            this.stageList = opportunityMasterData.ListOfOpportunityStages;
            this.masterOpportunityData = opportunityMasterData;
          } else {
            this.opportunityList = [];
            this.stageList = [];
          }
          this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(0),
            switchMap(() => this._search()),
            delay(0),
            tap(() => this._loading$.next(false))
          ).subscribe(result => {
            this._opportunities$.next(result.opportunities);
            this._total$.next(result.total);
            this._stageList.next(result.stageList1);
          });
          this._search$.next();
        }
      });
  }

  MemberOpportunityReload() {
    this.loggedInIntUserId = this.auth.getIntUserId();
    let inputObject = {
      "AssignedTo": this.loggedInIntUserId
    }

    this.opportunityService.GetOpportunitiesWithFileters(inputObject)
      .subscribe(data => {
        if (data.Status === 'Success') {
          if (data.Data != "") {
            let opportunityMasterData = JSON.parse(data.Data);
            this.opportunityList = opportunityMasterData.ListOfOpportunities;
            this.stageList = opportunityMasterData.ListOfOpportunityStages;
            this.masterOpportunityData = opportunityMasterData;
          } else {
            this.opportunityList = [];
            this.stageList = [];
          }
          this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(0),
            switchMap(() => this._search()),
            delay(0),
            tap(() => this._loading$.next(false))
          ).subscribe(result => {
            this._opportunities$.next(result.opportunities);
            this._total$.next(result.total);
            this._stageList.next(result.stageList1);
          });
          this._search$.next();
        }
      });
  }

  stageSearchClick(clickedText) {
    clickedText = clickedText.toLowerCase();

    let opportunityMasterData = this.masterOpportunityData;
    let allCount = opportunityMasterData.ListOfOpportunities.length;
    if (clickedText == "all") {
      this.opportunityList = opportunityMasterData.ListOfOpportunities;
      this.stageList = opportunityMasterData.ListOfOpportunityStages;
    } else {
      opportunityMasterData = opportunityMasterData.ListOfOpportunities.filter(a => a.Stage.toLowerCase() == clickedText);
      this.opportunityList = opportunityMasterData;
      this.stageList = [{ 'stage': 'all', 'stagecount': allCount }, { 'stage': clickedText, 'stagecount': opportunityMasterData.length }];
    }


    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._opportunities$.next(result.opportunities);
      this._total$.next(result.total);
      this._stageList.next(result.stageList1);
    });
    this._search$.next();

  }

  TimePeriodFilter(clickedText) {
    clickedText = clickedText.toLowerCase();
    this.loggedInUserRole = this.auth.getRole();
    if (clickedText === "all" && this.loggedInUserRole != 'member' && this.loggedInUserRole != 'employee' && this.loggedInUserRole != 'ta') {
      this.opportunityService.GetOpportunities()
        .subscribe(data => {
          if (data.Status === 'Success') {
            let opportunityMasterData = JSON.parse(data.Data);
            this.opportunityList = opportunityMasterData.ListOfOpportunities;
            this.stageList = opportunityMasterData.ListOfOpportunityStages;
            this.masterOpportunityData = opportunityMasterData;
            this._search$.pipe(
              tap(() => this._loading$.next(true)),
              debounceTime(0),
              switchMap(() => this._search()),
              delay(0),
              tap(() => this._loading$.next(false))
            ).subscribe(result => {
              this._opportunities$.next(result.opportunities);
              this._total$.next(result.total);
              this._stageList.next(result.stageList1);
            });
            this._search$.next();
          }
        });
    }

    var thisWeekDates = this.weekDates();

    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeknextday = thisWeekDates.endDate;


    var nextWeekDates = this.nextWeekDates();
    var nextweekfirstday = nextWeekDates.startDate; // 06-Jul-2014
    var nextweeknextday = nextWeekDates.endDate;

    let currentweekfirstdate = this.dateConvertion(currentweekfirstday);
    let currentweeknextdate = this.dateConvertion(currentweeknextday);

    let nextweekfirstdate = this.dateConvertion(nextweekfirstday);
    let nextweeknextdate = this.dateConvertion(nextweeknextday);
    let currentDate = this.dateConvertion(new Date());

    var thisMonth = this.thismonthDates();
    var thismonthfirstday = this.dateConvertion(thisMonth.startDate); 
    var thismonthlastday = this.dateConvertion(thisMonth.endDate);

    let startdueDate: string;
    let enddueDate: string;
    switch (clickedText) {
      case 'today':
        startdueDate = currentDate;
        enddueDate = currentDate;
        break;
      case 'thisweek':
        startdueDate = currentweekfirstdate;
        enddueDate = currentweeknextdate;
        break;
      case 'nextweek':
        startdueDate = nextweekfirstdate;
        enddueDate = nextweeknextdate;
        break;
     case 'thismonth':
         startdueDate = thismonthfirstday;
         enddueDate = thismonthlastday; 
        break;
      default:
        startdueDate = "";
        enddueDate = "";
        break;
    }
    let teamId = 0;
    if (this.loggedInUserRole === 'admin') {
      this.loggedInIntUserId = "0";
    } else if (this.loggedInUserRole === 'ta') {
      let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
      teamId = TeamInfo.Id;
      this.loggedInIntUserId = "0";
    }
    else {
      this.loggedInIntUserId = this.auth.getIntUserId();
    }
    let inputObject = {
      "StartCreatedDate": startdueDate,
      "EndCreatedDate": enddueDate,
      // "DueDateType":"range",
      "AssignedTo": this.loggedInIntUserId,
      "TeamId": teamId
    }

    this.opportunityService.GetOpportunitiesWithFileters(inputObject)
      .subscribe(data => {
        if (data.Status === 'Success') {
          if (data.Data != "") {
            let opportunityMasterData = JSON.parse(data.Data);
            this.opportunityList = opportunityMasterData.ListOfOpportunities;
            this.stageList = opportunityMasterData.ListOfOpportunityStages;
            this.masterOpportunityData = opportunityMasterData;
          } else {
            this.opportunityList = [];
            this.stageList = [];

          }
          this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(0),
            switchMap(() => this._search()),
            delay(0),
            tap(() => this._loading$.next(false))
          ).subscribe(result => {
            this._opportunities$.next(result.opportunities);
            this._total$.next(result.total);
            this._stageList.next(result.stageList1);
          });
          this._search$.next();
        }
      });
  }

  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 8);
    EndDate.setDate(today.getDate() - day + 14);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }
  
  thismonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  dataStatus() {
    return this.IsDataAvailable;
  }

  get opportunities$() { return this._opportunities$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }
  get stageSearch() { return this._state.stageSearch; }
  get stageCollection$() { return this._stageList.asObservable(); }
  set page(page: number) { this._set({ page }); }
  set pageSize(pageSize: number) { this._set({ pageSize }); }
  set searchTerm(searchTerm: string) { this._set({ searchTerm }); }
  set stageSearch(stageSearch: string) { this._set({ stageSearch }); }


  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm, stageSearch } = this._state;

    this.IsDataAvailable = true;
    let stageList1 = this.stageList;
    let opportunities = this.opportunityList;

    // 2. filter
    opportunities = opportunities.filter(opportunity => matches(opportunity, searchTerm, this.pipe));
    const total = opportunities.length;

    // 3. paginate
    opportunities = opportunities.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    this.IsDataAvailable = total < 0 ? false : true;
    return of({ opportunities, total, stageList1 });
  }


}
