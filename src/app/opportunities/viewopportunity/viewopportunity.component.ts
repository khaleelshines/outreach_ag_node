import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { OpportunityApiService } from 'src/services/opportunities.services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-viewopportunity',
  templateUrl: './viewopportunity.component.html',
  styleUrls: ['./viewopportunity.component.css']
})
export class ViewopportunityComponent implements OnInit {
  subscriptions:Subscription [] = [];
  public opportunityInfo:any;
  opportunityid:number;
  nextStepmodalRef: BsModalRef;
  nxtActionSelect:string;
  nxtActionDate:any;
  bsValue = new Date();
  //#region userInfo
  userInfo: any;
  loggedInUserRole: string;
  IsMember: boolean;
  companyintid:number;
  userintid:number;
  //#endregion
  //#region update next action
  updateNxtActionForm: FormGroup;
  submitted = false;
  activeTab: any = 'contacts';
  //#endregion
  get f() {
    return this.updateNxtActionForm.controls;
  }
  constructor(private opportunityService:OpportunityApiService,
              private route:ActivatedRoute,
              private location: Location,
              private formBuilder: FormBuilder,
              private modalService: BsModalService) {

    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name: userDetails1.Name,
        UserId: userDetails1.UserId,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    } else {
     
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name: userDetails1.Name,
        UserId: userDetails1.UserId,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }
    let loginInfo = localStorage.getItem('userInfo');
    let loginInfoData = JSON.parse(loginInfo);
    this.companyintid = loginInfoData.CompanyInfo[0].CompanyIntId;
    this.userintid = loginInfoData.IntUserId;
   }

  ngOnInit() {
    this.route.params.subscribe(params => { 
      this.opportunityid = params["id"];
      this.getOpportunityInfo(this.opportunityid);
     });
     this.updateNxtActionForm = this.formBuilder.group({
      NextActivityDate: [],
      NextAction: []
    });

  }

    getOpportunityInfo(oppoIntId:number): any {    
    let inputObject = {
     // "companyintid": companyIntId,
      "opportunityintid": oppoIntId      
    }
    let opportunitySub =  this.opportunityService.GetOpportunityInfoData(oppoIntId).subscribe(data => {
        if (data.Status === "Success") {        
          this.opportunityInfo = data.Data[0];
           this.nxtActionSelect = this.opportunityInfo.nextaction;
           
           if(this.opportunityInfo.nextactivitydate == "NA"){
            this.nxtActionDate = this.bsValue;
           }else{
            this.nxtActionDate = this.opportunityInfo.nextactivitydate;
           }
        } else {
         this.opportunityInfo = undefined;
        }
      });
      this.subscriptions.push(opportunitySub);
    }

     //#region Next Step Change
     ChangeNxtStpBtnClick(template: TemplateRef<any>, oppoId: number) {
     // this.opportunityid = oppoId;
     // alert(oppoId);
      this.nextStepmodalRef = this.modalService.show(template, { class: 'second' });
    }
    updateNextStepConfirmClick() {
      this.submitted = true;
      let date = new Date(this.bsValue); 
     // let fullDate = date.setHours(0,0,0);
      let nextActionDate = Math.floor(new Date(date).getTime() / 1000.0);
      let nextAction = (<HTMLSelectElement>document.getElementById('nextActionSelector')).value;
      let opportunityObj = {
        companyintid:this.companyintid,
        opportunityintid: this.opportunityid,
        nextaction: nextAction,
        nextactiondate: nextActionDate
      };
   // alert(JSON.stringify(opportunityObj));
      this.opportunityService.UpdateOpportunityNxtActionData(opportunityObj).subscribe(data => {
        if (data.Status.toLowerCase() === 'success') {
          this.nextStepmodalRef.hide();
          this.getOpportunityInfo(this.opportunityid);
        }
      });
    
    }
    //#endregion

    previousPage(){
      this.location.back();
    }

    getNotes(){}
    getCalls(){}
    gettasks(){}
    getEmails(){}
    getMeetings(){}

}
