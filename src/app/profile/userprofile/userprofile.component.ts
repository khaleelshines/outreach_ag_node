import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileApiService } from 'src/services/profileservice.services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EmailProcessApiService } from 'src/services/emailintegration.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/services/userservice.services';


@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  userInfo: any;
  emails: any;
  userEmailFlag: any;
  submitted = false;
  responseObject: any;
  disableButton: any;
  loggedInUserRole: any;
  baseUrl: string;
  authCode: any;

  constructor(private modalService: NgbModal, private apiService: ProfileApiService,
    private emailapiService: EmailProcessApiService,private userService:UserService,
    private formBuilder: FormBuilder, public auth: AuthService, private route: ActivatedRoute) {
    let redirectUrl = "http://sales.outwork.in/";
    this.loggedInUserRole = this.auth.getRole();
    this.baseUrl = "https://accounts.google.com/o/oauth2/auth?redirect_uri=" + redirectUrl + "user/profile&response_type=code&client_id=724400070494-a8t7vdojq9iis8efugmr5t3ga3ejmo9h.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/gmail.send&approval_prompt=force&access_type=offline";
  }
  ngOnInit() {
    this.authCode = this.route.snapshot.queryParams.code;
    if (this.authCode !== undefined) {
      this.signIn();
    }
    let userDetails = localStorage.getItem('userInfo');
    let userDetails1 = JSON.parse(userDetails);
    this.userInfo = {
      name: userDetails1.Name,
      email: userDetails1.Email,
      role: userDetails1.ObjectType
    }
    this.userEmailFlag = userDetails1.IsEmailConfigured;
    if (this.userEmailFlag) {
      this.disableButton = true;
    }
  }

  signIn() {
    this.authCode = this.route.snapshot.queryParams.code;
    console.log(this.authCode);
    if (this.authCode !== undefined) {
      this.emailapiService.CreateEmailOauthDetails(this.authCode)
        .subscribe(data => {
          if (data.Status === 'Success' && data.Data != "") {
            console.log(data.Data);
            this.disableButton = true;        
             
              this.userService.getUserInfo().subscribe(data=>{
                if (data.Code === 'SUC-200') { 
                  let responseData = JSON.parse(data.Data);                 
                  let emailConfiguration = responseData.IsEmailConfigured?"true":"false";
                  this.auth.setEmailConfig(emailConfiguration);
                }
              });            
            
            alert('Email Configured Successfully')
          }
          else
            console.log(data.Data);
         // alert('Something went wrong while creating');
        });
    }
    else {
      alert('Something went wrong');
    }
  }

}
