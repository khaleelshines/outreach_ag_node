import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sequencereport',
  templateUrl: './sequencereport.component.html',
  styleUrls: ['./sequencereport.component.css'],
  providers: [DatePipe]
})
export class SequencereportComponent implements OnInit {
  public columnDefs;
  private gridApi;
  private gridColumnApi;
  public defaultColDef;
  paginationPageSize: number;
  cacheBlockSize: number;
  public subscriptions:Subscription[] =[];
  reportPeriodSelect:string = 'thismonth';
  sequenceReportList:any;
  sequenceReportDetails :any;
  reportSequenceName:string;
  sequenceReportDetailsArr = [];
  sequenceReportSteps = [];
  
  sequenceReportStepsdata = [];
  todayDate:string;
  currentWeekfromto: string;
  lastWeekfromto: string;
  thisMonthfromto: string;
  sequenceData : any;
  sequenceId:any;
  sequenceDetails:any;

  constructor(private spinner: NgxSpinnerService,
    private sequenceService:SequenceApiService,
    private router:Router,
    public datepipe: DatePipe
    ) { 
      this.buildColdefs();
    }

  ngOnInit() {
    this.datesList();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getTeamPerformanceData(this.reportPeriodSelect);
  }

  PeriodChangedHandler(selectedValue:string){
    this.getTeamPerformanceData(selectedValue.toLowerCase());
    this.reportPeriodSelect = selectedValue;
  }

  getTeamPerformanceData(timePeriod:string){
    this.spinner.show();
    let sequencesSub =  this.sequenceService.getSequenceReports(timePeriod).subscribe(data => {
     // if (data.Code === "SUC-200") {
        this.sequenceReportList = data;         
         this.spinner.hide();        
     // }
    });
    this.subscriptions.push(sequencesSub);
  }

  gridOptions: GridOptions = {
    pagination: true,
    rowHeight: 40,
    headerHeight: 47,   
    cacheBlockSize: 10,    
  };

  buildColdefs() {
   
    this.columnDefs = [
      {
        headerName: "Sequence Name",
        field: 'SequenceName',
        sortable: true,
        filter: 'agTextColumnFilter',
        width: 240, pinned: 'left',
        filterParams: { suppressAndOrCondition: true },
        cellRenderer: (data) => {
          if (data.value !== undefined) {
            return '<a href="javascript:void(0);" data-action-type="rowDetails" data-toggle="modal" data-target="#viewDetails">' + data.value + '</a>';
          }
        }
      },
      {
        headerName: "Last Email Date",
        field: 'CreatedDate',
        sortable: true, 
        filter: true,
        width: 145
      },
      {
        headerName: "# Accounts",
        field: 'Accounts',
        sortable: true,
        filter: true,
        width: 140
      },
      {
        headerName: "# Contacts",
        field: 'Subscribers',
        sortable: true,
        filter: true,
        width: 140
      },
      {
        headerName: "Emails",
        cellRenderer: function(params) {
          return (
            '<span title="Emails delivered">' +params.data.Reached +'</span>/<span title="Emails sent">'+params.data.Sent +
            '</span>'
          );
        },
        // field: 'Reached',
        sortable: true,
        filter: false,
        width: 140
      },
      {
        headerName: 'R/B/O/C', 
        cellRenderer: function(params) {
          return (
            '<span title="Replies">'+params.data.Responses+'</span>/<span title="Bounced">'+params.data.Bounced +
            '</span>/<span title="Opened">' +params.data.Opened +'</span>/<span title="Clicks">'+params.data.Clicks+'</span>'
          );
        },
        width: 160,
        sortable: true,
        filter: true
      },
      {
        headerName: 'Open rate (%)', 
        field: 'OpenRate',
        width: 130,
        sortable: true,
        filter: true
      },
      {
        headerName: 'Clickthrough Rate(%)',
        field: 'ClickRate',
        sortable: true,
        width: 150,
        suppressSizeToFit: true,
      }
      /*{
        headerName: "Opened",
        field: 'Opened',
        sortable: true,
        filter: true,
        width: 140
      },
      {
        headerName: "Responses",
        field: 'Responses',
        sortable: true,
        filter: true,
        width: 140
      },
      {
        headerName: "Bounced",
        field: 'Bounced',
        sortable: true,
        filter: true,
        width: 140
      } ,
      {
        headerName: "",
        pinned: "right",
        width: 185,
        suppressMenu: true,
        editable: false,
        sortable:false,
        cellStyle: { "padding-top": "2px" },
        cellRenderer: function(params){
        return `<div class="">    
        <span class="view-badge d-inline-block" title="Update" data-action-type="rowDetails" data-toggle="modal" data-target='#viewDetails'>
            <i class="fa fa-eye pointer" data-action-type="rowDetails1"></i>
          </span>
        </div>`;
        }
      } */
    ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;

  }

  onCellClicked($event: CellClickedEvent) {
    let id = $event.data.SequenceIdInt;
    if($event.colDef.field == "SequenceName"){    
   // this.router.navigate(['/viewsequence/'+id]);

    if(id){
      this.spinner.show();
      this.sequenceService.getSequenceDetailedReports(this.reportPeriodSelect, id).subscribe(data => {
        this.sequenceReportDetails = JSON.parse(data.Data); 
        this.reportSequenceName = this.sequenceReportDetails[0].SequenceName;
        this.spinner.hide();        
      });  
  }

    }
  }

  
  //#region for start and end dates functions 
  datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 


  /*
  public onRowClicked(e) {
    if (e.event.target !== undefined || e.event.target !== null) {
      let actionType = e.event.target.getAttribute("data-action-type");
      this.sequenceData = e.node.data;

      if(actionType == 'rowDetails' || actionType == 'rowDetails1'){
        this.sequenceId = this.sequenceData.SequenceIdInt;
        if(this.sequenceId){
            this.spinner.show();
            this.sequenceService.getSequenceDetailedReports(this.reportPeriodSelect, this.sequenceId).subscribe(data => {
              this.sequenceReportDetails = JSON.parse(data.Data); 
              this.reportSequenceName = this.sequenceReportDetails[0].SequenceName;
              this.spinner.hide();        
            });  
        }
      }
    }
  } */

  
}