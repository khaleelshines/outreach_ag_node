import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { Subscribable, Subscription } from 'rxjs';
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { array } from '@amcharts/amcharts4/core';
import { DataService } from "src/services/DataService";
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sequencereports',
  templateUrl: './sequencereports.component.html',
  styleUrls: ['./sequencereports.component.css'],
  providers: [DatePipe]
})
export class SequencereportsComponent implements OnInit {
  public columnDefs;
  private gridApi;
  private gridColumnApi;
  public defaultColDef;
  paginationPageSize: number;
  selectedSequence: any;
  cacheBlockSize: number;
  //#region reports
  public sequenceList = [];
  reportPeriodSelect:string = 'thismonth'; //'thisweek';
  //#endregion
  public subscriptions:Subscription[] =[];
  public stepsList = [];
  public stepsHeaders = [];
  todayDate:string;
  currentWeekfromto: string;
  lastWeekfromto: string;
  thisMonthfromto: string;

  constructor(
    private spinner: NgxSpinnerService,
    private sequenceService:SequenceApiService,
    private dataService: DataService,
    private router: Router,
    public datepipe: DatePipe
  ) {  
  }

  ngOnInit() {
    this.datesList();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.getSequences(this.reportPeriodSelect);
    //this.stepsList = ['Step 1','Step 2','Step 3','Step 4','Step 5'];
    //this.buildColdefs(); 
  }

  PeriodChangedHandler(selectedValue:string){   
    this.getSequences(selectedValue.toLowerCase());
  }

  public getSequences(timePeriod): any {
    this.spinner.show();
    let sequencesSub =  this.sequenceService.getSequenceReports(timePeriod).subscribe(data => {
      let sequenceData = data;      
      this.stepsList = sequenceData.headers;
      this.sequenceList = sequenceData.sequencereportlist;
      
      for(let sl=0; sl<this.sequenceList.length; sl++){
        for(let sr=0; sr < this.sequenceList[sl]['ReportStepDetails'].length; sr++){
          let steplableStepBouncedCount = this.sequenceList[sl]['ReportStepDetails'][sr]['StepLabel'].replace(" ", "_")+"_StepBouncedCount";
          let steplableStepOpenedCount = this.sequenceList[sl]['ReportStepDetails'][sr]['StepLabel'].replace(" ", "_")+"_StepOpenedCount";
          let steplableStepSentCount = this.sequenceList[sl]['ReportStepDetails'][sr]['StepLabel'].replace(" ", "_")+"_StepSentCount";
          
          this.sequenceList[sl][steplableStepSentCount] = this.sequenceList[sl]['ReportStepDetails'][sr]['StepSentCount'];
          this.sequenceList[sl][steplableStepOpenedCount] = this.sequenceList[sl]['ReportStepDetails'][sr]['StepOpenedCount'];
          this.sequenceList[sl][steplableStepBouncedCount] = this.sequenceList[sl]['ReportStepDetails'][sr]['StepBouncedCount'];
        }
      }
      this.buildColdefs(); 
    });
    this.spinner.hide();
    this.subscriptions.push(sequencesSub);
  }

  gridOptions: GridOptions = {
    pagination: true,
    rowHeight: 40,
    headerHeight: 47,   
    cacheBlockSize: 10,    
  };

  buildColdefs() {
       this.columnDefs = [
          {
            headerName: '',
            children: [
              { 
                headerName: "Sequence Name", 
                field: 'SequenceName', 
                sortable: true, 
                filter: 'agTextColumnFilter', 
                width:250,pinned: 'left',
                filterParams: { suppressAndOrCondition: true },
                cellRenderer: (data) => {
                  if (data.value !== undefined) { 
                    return '<a href="javascript:void(0);">' + data.value + '</a>'; 
                  }
                }
              }
            ]
          }, 
          {
            headerName: '',
            children: [
              {
                headerName: "Initiation Date",
                field: 'SequenceInitiatedOn',
                sortable: true,
                filter: true,
                width: 200
              }
            ]
          }
        ];
      
        let i;
      for (i = 0; i < this.stepsList.length; i++) {
        this.columnDefs.push({
          headerName:this.stepsList[i],
          children:[
            {
              headerName: "Sent",
              field: this.stepsList[i].replace(" ", "_")+'_StepSentCount',
              sortable: true,
              filter: true,
              width: 80
            },
            {
              headerName: "Open",
              field: this.stepsList[i].replace(" ", "_")+'_StepOpenedCount',
              sortable: true,
              filter: true,
              width: 100
            },
            {
              headerName: "Bounced",
              field: this.stepsList[i].replace(" ", "_")+'_StepBouncedCount',
              sortable: true,
              filter: true,
              width: 100
            }
          ]
        });      
      }

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
  }

  onCellClicked($event: CellClickedEvent) {
    let id = $event.data.SequenceIntId;
    if($event.colDef.field == "SequenceName"){    
    this.router.navigate(['/viewsequence/'+id]);
    }
  } 

  //#region for start and end dates functions 
  datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 
}