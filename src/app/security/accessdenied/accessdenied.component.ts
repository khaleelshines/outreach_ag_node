import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/guards/authguard/auth.service';

@Component({
  selector: 'app-accessdenied',
  templateUrl: './accessdenied.component.html',
  styleUrls: ['./accessdenied.component.css']
})
export class AccessdeniedComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
