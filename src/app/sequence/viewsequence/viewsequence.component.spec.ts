import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsequenceComponent } from './viewsequence.component';

describe('ViewsequenceComponent', () => {
  let component: ViewsequenceComponent;
  let fixture: ComponentFixture<ViewsequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsequenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
