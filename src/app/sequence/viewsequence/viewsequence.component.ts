import { Component, OnInit, ViewChild, ElementRef, OnDestroy, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SequenceApiService } from '../../../services/sequenceservice.services';
import { AuthService } from '../../guards/authguard/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../../services/DataService';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContactApiService } from 'src/services/contacts.services';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: 'app-viewsequence',
  templateUrl: './viewsequence.component.html',
  styleUrls: ['./viewsequence.component.css']
})
export class ViewsequenceComponent implements OnInit,OnDestroy {
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  paginationPageSize: number;
  templateIderror :boolean = false;
  phonePurposeerror:boolean = false;
  phonePriorityerror:boolean = false;
  taskTitleerror:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
     private router: Router,
     private sequenceService: SequenceApiService,
    private bsModalService: BsModalService,
    private auth: AuthService,
    private modalService: NgbModal,
    private modalBsServiceRef:BsModalService,
    private dataService: DataService,
    private contactService:ContactApiService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute) {

      this.columnDefs = [
        {
          headerName: "Contact Name",
          field: "contactname",
          cellRenderer: function(params) {
            return (
              '<a href="contacts/viewcontact/' +
              params.data.id +
              '""target="_blank">' +
              params.value +
              "</a>"
            );
          },
         tooltipField: "contactname",
         tooltipComponentParams: { color: "#ececec" }
        },
        {
          headerName: "Account Name",
          field: "AccountName"
        },      
        // {
        //   headerName: "Industry",
        //   field: "Industry",
        //   filter:true
        // },    
        {
          headerName:"Scheduled Date",
          field:"ImplementationDate",
          sortable:true,
          filter:true
        },   
        {
          headerName: "Step Subject",
          field: "StepSubject"
        },{
          headerName: "Mail Status",
          field: "EmailStatus",
          filter:true,
          cellRenderer: function(params) {
            return params.value == "sent"?             
              '<span class="alert alert-success custom-status">' +
              params.value +
              "</span>":
              '<span class="alert alert-danger custom-status">' +
              params.value +
              "</span>"
            ;
          },
        },   
        {
          headerName: "Step Type",
          field: "StepType"
        },
        {
          headerName: "Designation",
          field: "designation"
        },
        {
          headerName: "Email",
          field: "email"
        },
        {
          headerName: "Mobile Number",
          field: "mobilenumber"
        }
      ];
      
      this.paginationPageSize = 10;
     }

  StepType: any;
  Days: any;
  Hours: any;
  Mins: any;
  sequenceSteps: any;
  isTaskViewable: any;
  isAutoViewable: any;
  isViewable: any;
  sequenceStepForm: FormGroup;
  sequenceStepEditForm:FormGroup;
  id: any;
  templates: any;
  subscribers: any;
  responses: any;
  reached: any;
  opened: any;
  public sequenceid;
  public localsequenceid;
  submitted = false;
  sequenceStepsDays : any;
  sequenceStepsHours : any;
  sequenceStepsMins : any;
  updateStepmodalRef:BsModalRef;
  modalResponseRef:BsModalRef;
  //#region modal declaration
  responseInfo:string;  
  //#endregion
  //#region  delete code
   deleteStepId:number;
   deleteStepmodalRef:BsModalRef;
  //#endregion
  //#region  common declarations
  sequencename:string;
  sequenceIndustryName:string;
  SequenceContacts:any;
  SequenceStepsCount:number=0;
  stepsList:any;
  stepId:number;
  stepSelect:string;
  mainTab:string = 'overview';
  //#endregion
  etSelect:string = "";
  stepid:number;
  subscriptions:Subscription [] = [];
  @ViewChild('closeAddSequenceStepModal', { static: false }) closeAddSequenceStepModal: ElementRef;
  get f() { return this.sequenceStepForm.controls; }
  get Ef() { return this.sequenceStepEditForm.controls; }

  gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52
  };
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);      
    }
  }
  ngOnInit() {

    // this.spinner.show();

    // setTimeout(() => {
    //   this.spinner.hide();
    // }, 4000);
    this.sequenceStepForm = this.formBuilder.group({
      TaskTitle: [],
      StepType: ['', Validators.compose([Validators.required])],
      Description: [],
      priority: [],
      purpose: [],
      Days: [],
      Mins: [],
      Hours: [],
      notes: [],
      Order: [],
      TemplateId: []
    });

   
    this.sequenceStepEditForm = this.formBuilder.group({
      TaskTitle: [],
      StepType: ['', Validators.compose([Validators.required])],
      Description: [],
      priority: [],
      purpose: [],
      Days: [],
      Mins: [],
      Hours: [],
      notes: [],
      Order: [],
      TemplateId: [],
      SequenceId: [],
      StepId:[]
    });

    this.isViewable = false;
    this.isAutoViewable = false;
    this.route.params.subscribe(params => {
      this.id = params["id"];
      this.getSequenceStepsById();
    });
    this.getStepsInfo();

   // this.getSubscribersCount();
    this.getMonthDays();
    this.getHours();
    this.getMins();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  getSequenceStepsById(){
    this.spinner.show();
  let sequenceInfoSub =  this.sequenceService.getSequenceById(this.id)
    .subscribe(data => {
      if (data.Code === 'SUC-200') {
        let masterData = JSON.parse(data.Data);
        this.sequenceSteps = masterData.ListOfStepsData;
        this.sequencename = masterData.SequenceName;
        this.sequenceIndustryName = masterData.Industry;       
      }
      else {
        this.sequenceSteps = [];
      }
      this.spinner.hide();
    });
    this.subscriptions.push(sequenceInfoSub);
  }

  //#region  contacts
  StepChangedHandler(selectedId:number){
    this.getSequenceContactsMethod(selectedId);
  }
  stepContacts(stepId:number){
    this.getSequenceContactsMethod(stepId);
    this.stepSelect = stepId.toString();
    this.mainTab = 'contacts';
  }
  mainFilterTabsChanged(selectedTabValue:string){
    if(selectedTabValue == "contacts"){   
      if(this.stepsList.length > 0){  
      this.getSequenceContactsMethod(this.stepsList[0].StepId);
      this.stepSelect = this.stepsList[0].StepId;
      }else{this.SequenceContacts = [];}
    }else{
      this.mainTab = 'overview';
    }
  }
  getStepsInfo(){
    let seqStepsInfoSub = this.sequenceService.getSequenceStepsInfo(this.id).subscribe(data => {
       if (data.Code === "SUC-200") {       
        this.stepsList = JSON.parse(data.Data);
       }else{
         this.stepsList = [];
       }
     });
     this.subscriptions.push(seqStepsInfoSub);
   }

  getSequenceContactsMethod(stepId:number){
    this.spinner.show();
    let seqContactsSub = this.contactService.getStepContacts(this.id,stepId).subscribe(data => {
       if (data.Code === "SUC-200") {       
        this.SequenceContacts = JSON.parse(data.Data);            
       }else{
         this.SequenceContacts = [];
       }
       this.spinner.hide();   
     });
     this.subscriptions.push(seqContactsSub);
   }
  //#endregion

  onRowClick(event) {
    this.StepType = event.target.value;
    this.isTaskViewable = false;
      this.isViewable = false;
      this.isAutoViewable = false;
    if (this.StepType == "Generic Task") {
      this.isTaskViewable = true;
      this.isViewable = false;
      this.isAutoViewable = false;
    }
    if (this.StepType == "Auto Email") {
      this.isAutoViewable = true;
      this.isTaskViewable = false;
      this.isViewable = false;
    }
    if (this.StepType == "Manual Email") {
      this.isAutoViewable = true;
      this.isTaskViewable = false;
      this.isViewable = false;
    }
    if (this.StepType == "Phone Call") {
      this.isViewable = true;
      this.isAutoViewable = false;
      this.isTaskViewable = false;
    }
  }

  onselectedhours(event) {
    this.Hours = event.target.value;
  }

  onselecteddays(event) {
    this.Days = event.target.value;
  }

  onselectedmins(event) {
    this.Mins = event.target.value;
  }

  // getSubscribersCount() {
  //  let sequenceCountSub = this.sequenceService.getSubscribersCount(this.id).subscribe(data => {
  //     let subscriberscount = JSON.parse(data.Data);
  //     this.subscribers = subscriberscount.Subscribers;
  //     this.reached = subscriberscount.Reached;
  //     this.responses = subscriberscount.Responses;
  //     this.opened = subscriberscount.Opened;
  //   });
  //   this.subscriptions.push(sequenceCountSub);
  // }

  addsequencesteps(template: TemplateRef<any>) {  
    this.submitted = true;
   
    if((this.sequenceStepForm.value.StepType == "Manual Email" 
    || this.sequenceStepForm.value.StepType == "Auto Email") && 
    (this.sequenceStepForm.value.TemplateId === null || this.sequenceStepForm.value.TemplateId === "")){
      this.templateIderror = true;
      return;
    }else {
      this.templateIderror = false;
    }

    if(this.sequenceStepForm.value.StepType == "Phone Call" &&
     (this.sequenceStepForm.value.purpose === null || this.sequenceStepForm.value.purpose === ''
      || this.sequenceStepForm.value.priority === null || this.sequenceStepForm.value.priority === "")){
      if(this.sequenceStepForm.value.purpose === null || this.sequenceStepForm.value.purpose === ""){
        this.phonePurposeerror = true;
      }else {
        this.phonePurposeerror = false;
      }
      if(this.sequenceStepForm.value.priority === null || this.sequenceStepForm.value.priority === ""){
        this.phonePriorityerror = true;
      }else {
        this.phonePriorityerror = false;
      }
      return;
    }

    if(this.sequenceStepForm.value.StepType == "Generic Task" && this.sequenceStepForm.value.TaskTitle === null){
      this.taskTitleerror = true;
      return;
    }else {
      this.taskTitleerror = false;
    }
    
    /*
      StepType == "Manual Email", 
    */
    if (this.sequenceStepForm.invalid) {
      return;
    }

    let orderValue = this.sequenceSteps[this.sequenceSteps.length - 1];

    if (orderValue != undefined) {
      this.sequenceStepForm.patchValue({ Order: orderValue.Order + 1 });
    }
    else {
      this.sequenceStepForm.patchValue({ Order: 1 });
    }
    this.sequenceService.createSequenceSteps(this.sequenceStepForm.value, this.id).subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.responseInfo = `<div class="alert alert-success mb-0">     
      <h6>Step added successfully..!</h6> 
            </div> `;       
        this.getSequenceStepsById();  
        this.closeAddSequenceStepModal.nativeElement.click();
        this.onStepReset();    
      }else if(data.Code === 'FAL-401' && data.Data === 'duplicate'){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>You cann't add a step with same day..!</h6> 
              </div> `;       
      }      
    
    this.modalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
    setTimeout(() => {
      this.modalResponseRef.hide();
    }, 2000);
      
    });
  }

  onStepReset() {
    this.submitted = false;   
    this.isViewable = false;
    this.isTaskViewable = false;
    this.isAutoViewable  = false;
    this.sequenceStepForm.reset();
  }

  UpdateStepBtn(template: TemplateRef<any>,stepId:number) {  
    this.stepid = stepId; 
    this.sequenceService.getSequenceStepInfo(this.id,stepId).subscribe(data => {
      if (data.Code === "SUC-200") {
        let response = JSON.parse(data.Data);       
       
        let formFields = {
          TaskTitle: "",
          Description: "",
          priority: "",   
          Order: "",
          purpose:"",
          SequenceId: "",
          StepId:"",
          TemplateId: response.TemplateId,
          StepType:response.StepType,
          Days:response.Days,
          Hours :response.Hours,
          Mins:response.Mins,
          notes:response.Notes
            };
           
        this.sequenceStepEditForm.setValue(formFields);
        this.etSelect = formFields.TemplateId;
        this.dataChanged("");
        this.updateStepmodalRef = this.bsModalService.show(template, {
          class: "second"
        });
      }
    });
   
  }

  updatesequencesteps() {  
    this.submitted = true;
    if (this.sequenceStepEditForm.invalid) {
      return;
    }   
    this.sequenceStepEditForm.patchValue({ SequenceId: this.id });
    this.sequenceStepEditForm.patchValue({ StepId: this.stepid });
    this.sequenceService.updateSequenceSteps(this.sequenceStepEditForm.value).subscribe(data => {
      if (data.Code === 'SUC-200') {
       this.updateStepmodalRef.hide();
        this.getSequenceStepsById();      
      }
      this.onStepReset();
    });
  }

  deleteSequenceStep(clickedText,template: TemplateRef<any>) {  
    this.deleteStepId = clickedText;
    this.deleteStepmodalRef = this.bsModalService.show(template, {
      class: "second"
    });
  }

  deleteSequenceStepConfirmation(){
    this.spinner.show();
    this.sequenceService.deleteSequenceStep(this.deleteStepId, this.id).subscribe(data => {
      if (data.Code === "SUC-200") {
       // this.getSequencebyId(); 
       this.getSequenceStepsById();       
      }      
      this.deleteStepmodalRef.hide();
      this.spinner.hide();
    });
  }

  dataChanged(obj) {
    this.sequenceService.getTemplate("contacttemplate").subscribe(data => {
      if (data.Status === 'Success') {
        this.templates = JSON.parse(data.Data);       
      }
    });
  }

  getSequencebyId() {
    this.sequenceService.getSequenceById(this.id)
      .subscribe(data => {
        if (data.Code === 'SUC-200') {
          this.sequenceSteps = JSON.parse(data.Data);
         // this.SequenceStepsCount = this.sequenceSteps.length;
        }
        else {
          this.sequenceSteps = [];
         // this.SequenceStepsCount = 0;
        }
      });
  }

  getMonthDays(){
    var dayRange = [];
    for(var i=0;i<=30;i++) {
      dayRange.push(i);
    }
    this.sequenceStepsDays = dayRange;    
  }

  getHours(){
    var hoursRange = [];
    for(var i=0;i<24;i++) {
      hoursRange.push(i);
    }
    this.sequenceStepsHours = hoursRange;    
  }

  getMins(){
    var minsRange = [];
    for(var i=0;i<60;i++) {
      minsRange.push(i);
    }
    this.sequenceStepsMins = minsRange;    
  }

}
