import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CellClickedEvent, GridOptions, IDatasource, IGetRowsParams } from 'ag-grid-community';
import { OpportunityApiService } from 'src/services/opportunities.services';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { AccountApiService } from 'src/services/accounts.services';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-opportunitieslist',
  templateUrl: './opportunitieslist.component.html',
  styleUrls: ['./opportunitieslist.component.css']  
})
export class OpportunitieslistComponent implements OnInit {
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  subscriptions:Subscription [] = [];
  bsValue = new Date();
  bsValue1 = new Date();
  opportunityForm: FormGroup;
  submitted = false;
  Users: any;
  userInfo: any;
  loggedInUserRole: string;
  IsMember: boolean;
  UserIntId: string;
  //#region contacts dropdown
  contactsList:any;
  selContactInfo:any;
  contactObj:any;
  contactnamerequired:boolean = false;
  //#endregion
  mytime: Date = new Date();
  mytime1: Date = new Date();
  accountintiderror: boolean = false;
  accountnameerror: boolean = false;

  periodType:string = "thisweek";
  //#region account searh
  accountsList:any = [];
  selAccountInfo:any;
  //#region userInfo
  companyintid:number;
  userintid:number;
  //#endregion

  //#region date filter drop down list
 todayDate:string;
 currentWeekfromto: string;
 lastWeekfromto: string;
 thisMonthfromto: string;

 globalSearch:any="";
 mainTab:string = "createddate"; 
  public opportunityData: any;

  get f() {
    return this.opportunityForm.controls;
  }
  @ViewChild("closeAddOpportunityModal", { static: false })
  closeAddOpportunityModal: ElementRef;
  constructor(
    private opportunityService:OpportunityApiService,
    private formBuilder: FormBuilder,
    private accounts: AccountApiService,
    public auth: AuthService,
    private teamService: teammanagementservice,
    public datepipe: DatePipe,
    private router:Router,
    private spinner: NgxSpinnerService
  ) {
    this.loggedInUserRole = this.auth.getRole();
    this.columnDefs = [
      {
        headerName: "Opportunity Name",
        field: "title",     
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        }, 
        cellRenderer: (data) => {
          if (data.value != undefined) 
          { 
            return '<a href="javascript:void(0);">' + data.value + '</a>';
           }
        }
      },
      {
        headerName: "Account Name",
        field: "accountname",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Stage",
        field: "stage",
        width:120,
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Last Activity Date",
        field: "last_activity_date",
        filter: 'agDateColumnFilter',
        filterParams:  filterParams,
        width:150
      },
      {
        headerName: "Last Action",
        field: "last_action",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        width:110
      },
      {
        headerName: "Next Activity Date",
        field: "next_activity_date",
        filter: 'agDateColumnFilter',
        filterParams:  filterParams,
        width:150
      },
      {
        headerName: "Next Action",
        field: "next_action",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        width:110
      },
      {
        headerName: "Created On",
        field: "createddate",
        filter: 'agDateColumnFilter',
        filterParams:  filterParams,
        width:110
      },
      // {
      //   headerName: "Status",
      //   field: "status",
      //   width:100,
      //   filter: 'agTextColumnFilter',
      //   filterParams: {
      //     suppressAndOrCondition: true
      //   },
      //   cellRenderer: function(params) {       
      //        if(params.value == "Won"){
      //         return ('<span class="alert alert-success">' +params.data.status+ '</span>');
      //        }else if(params.value == "Lost"){
      //         return ('<span class="alert alert-danger">' +params.data.status+ '</span>');
      //        }else{
      //         return ('<span class="">NA</span>');
      //        }    
            
      //   },
      //},
      // {
      //   headerName: "Value",
      //   field: "value",
      //   width:100,
      // },     
      
      
      // {
      //   headerName: "Target Date",
      //   field: "dateoftarget"
      // }
    ];

    this.defaultColDef = {
      editable: false,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true

    };
 
    
   }

  gridOptions: GridOptions = {
    pagination: true,
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52,   
    angularCompileHeaders: true
  }; 
  

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setDatasource(this.dataSource);    
  }

  onPageSizeChanged(newPageSize) {    
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  } 

  ngOnInit() {
    this.datesList();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name: userDetails1.Name,
        UserId: userDetails1.UserId,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    } else {
     let teamMembersSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
        this.Users = data;
      });
      this.subscriptions.push(teamMembersSub);

      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name: userDetails1.Name,
        UserId: userDetails1.UserId,
        UserIntId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }
    this.opportunityForm = this.formBuilder.group({
      AccountIntId: [''],
      Title: ['', Validators.required],
     // Value: ['', Validators.required],     
      PhoneNo: [],
      Stage: ['', Validators.compose([Validators.required])],
      AssignedToId: ['', Validators.compose([Validators.required])],
    //  Status: ['', Validators.compose([Validators.required])],
      TargetDate: [],
      LastActivityDate: [this.bsValue,Validators.compose([Validators.required])],
      NextActivityDate: [this.bsValue1,Validators.compose([Validators.required])],
      LastAction: [],
      NextAction: [],
      AssignedToIntId: []
    });
    let loginInfo = localStorage.getItem('userInfo');
    let loginInfoData = JSON.parse(loginInfo);
    this.companyintid = loginInfoData.CompanyInfo[0].CompanyIntId;
    this.userintid = loginInfoData.IntUserId;
  //  this.getOpportunities(this.periodType);
  }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText.length > 0) {
      this.globalSearch = searchText;   
      this.gridApi.setDatasource(this.dataSource);
    } else{
      this.globalSearch ="";
      this.gridApi.setDatasource(this.dataSource);
    }    
  }

  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();
      this.spinner.show();
   this.opportunityService.
   GetOpportunitiesList(params, this.gridOptions.paginationPageSize,this.globalSearch,
      this.mainTab,this.periodType)
        .subscribe(data => {
          if(data.Code == "SUC-200"){
          this.opportunityData = JSON.parse(data.Data);
          
          params.successCallback(
            this.opportunityData.ListOfOpportunities,
            this.opportunityData.OpportunitiesCount
          );          
          }
          this.spinner.hide();          
        });
     
    }
  }
  //companyIntId:number,userIntId:number,
  // getOpportunities(periodType:string): any {    
  //   let inputObject = {
  //    // "companyintid": companyIntId,
  //    // "userintid": userIntId,
  //     "periodtype":periodType
  //   }
  //   let opportunitySub =  this.opportunityService.GetOpportunitiesList(inputObject).subscribe(data => {
  //       if (data.Status === "Success") {        
  //         let oppoCompleteData = JSON.parse(data.Data);
  //         this.rowData = oppoCompleteData[0];
          
  //       } else {
  //         this.rowData = undefined;
  //       }
  //     });
  //     this.subscriptions.push(opportunitySub);
  //   }
  
    addOpportunity() {
      this.submitted = true;
      let accountIntId = 0;
       if(this.selAccountInfo != undefined && this.selAccountInfo.id > 0){  
         accountIntId = this.selAccountInfo.id;       
       }else{
         this.contactnamerequired = true;
         return;
      }

      if (this.opportunityForm.invalid) {
        return;
      }
    
      this.opportunityForm.patchValue({ AccountIntId: accountIntId });
      let date = new Date(this.bsValue);
      let timestamp = Math.floor(date.getTime() / 1000.0); 
      let date1 = new Date(this.bsValue1);
      let timestamp1 = Math.floor(date1.getTime() / 1000.0); 
      
      if (this.loggedInUserRole === "ta" || this.loggedInUserRole === "admin") {
        let userObj = this.Users.find(
          y => y.UserId == this.opportunityForm.value.AssignedToId
        );
        this.UserIntId = JSON.stringify(userObj.IntUserId);
      } else {
        this.UserIntId = this.userInfo.UserIntId;
      }
     
      this.opportunityForm.patchValue({ LastActivityDate: timestamp });      
      this.opportunityForm.patchValue({ NextActivityDate: timestamp1 });           
      this.opportunityForm.patchValue({ AssignedToIntId: this.UserIntId });
      this.opportunityService
        .AddOpportunity(this.opportunityForm.value)
        .subscribe(data => {
          if (data.Status === "Success") {
            this.periodType = 'today';
            this.gridApi.setDatasource(this.dataSource);
          } else {
           
          }
          this.onReset();
          this.resetAccount();
          this.closeAddOpportunityModal.nativeElement.click();
        });
    }

    resetAccount(){
      this.selAccountInfo = null;
      this.accountsList = [];
    }
  
    onReset() {
      this.submitted = false;
      this.bsValue = new Date();
      this.bsValue1 = new Date();
      this.opportunityForm.reset();
      this.resetAccount();
      this.contactnamerequired = false;
      // this.contactsList = [];
      // this.selContactInfo = [];
  }
  onCellClicked($event: CellClickedEvent,) {
     let id = $event.data.id;   
    if($event.colDef.field == "title"){
     this.router.navigate(['/opportunities/viewopportunity/'+id]);
    }    
  }
  
  accountChangeHandler(selectedObject:any,actionType){
    let accountsArray =[];
    let searchString = '';
    if(selectedObject.term && selectedObject.term.length >= 3 && actionType=='search'){
      searchString = selectedObject.term;
    }else if(selectedObject && actionType=='update') {
      searchString = selectedObject;
    }

     if(searchString.length >= 3){
     let accountsSub = this.accounts.getBasicAccounts(searchString).subscribe(data => {
      if(data.Status == "Success"){
       let accounts = JSON.parse(data.Data);
       accounts.forEach(element => {
        let singleItem:any;         
          singleItem ={
            id :element.Id,
            name:element.AccountName
          };       
       
          accountsArray.push(singleItem);
      });
      this.accountsList = accountsArray;  
      if(actionType=='update')   {
        this.selAccountInfo = accountsArray[0];
      }         
      }
     });
     this.subscriptions.push(accountsSub);
      }
  // if(selectedValue != ""){
  //   this.accountintiderror = false;
  // }
  }

  CreatedChangedHandler(clickedText) {
    clickedText = clickedText.toLowerCase();  
    this.periodType = clickedText;
    this.gridApi.setDatasource(this.dataSource);
   // this.getOpportunities(this.periodType);
   // this.gridApi.setDatasource(this.dataSource);
  }

  
 //#region Account selection
 selectAccount(){
  this.accountintiderror = false;  
  this.accountnameerror = false; 
  this.opportunityForm.markAsDirty();
   console.log(this.selAccountInfo);
 }

 //#region for start and end dates functions 
 datesList(){
  var today = new Date();
  this.todayDate = "( "+this.dateConvertion(today)+" )";

  var thisWeekDates = this.weekDates();
  var year = new Date().getFullYear();
  var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
  var currentweeklastday = thisWeekDates.endDate;
  this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

  var lastWeekDates = this.lastWeekDates();
  var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
  var lastweeklastday = lastWeekDates.endDate;
  this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

  var thisMonthDates = this.currenMonthDates();
  var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
  var thisMonthlastday = thisMonthDates.endDate;
  this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
  
}


dateConvertion(inputDate: any): string {
  return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
}

weekDates(): any {
  var today = new Date();
  var day = today.getDay();

  var StartDate = new Date();
  var EndDate = new Date();
  StartDate.setHours(0, 0, 0, 0);
  EndDate.setHours(0, 0, 0, 0);
  StartDate.setDate(today.getDate() - day + 1);
  EndDate.setDate(today.getDate() - day + 7);
  return {
    startDate: StartDate,
    endDate: EndDate
  };
}

nextWeekDates():any{
  var today = new Date();
  var day = today.getDay();

  var StartDate = new Date();
  var EndDate = new Date();
  StartDate.setHours(0,0,0,0);
  EndDate.setHours(0,0,0,0);
  StartDate.setDate(today.getDate()-day+8);
  EndDate.setDate(today.getDate()-day+14);
  return {
      startDate:StartDate,
      endDate: EndDate
  };
}

lastWeekDates(): any {
  var today = new Date();
  var day = today.getDay();

  var StartDate = new Date();
  var EndDate = new Date();
  StartDate.setHours(0, 0, 0, 0);
  EndDate.setHours(0, 0, 0, 0);
  StartDate.setDate(today.getDate() - day - 6);
  EndDate.setDate(today.getDate() - day);
  return {
    startDate: StartDate,
    endDate: EndDate
  };
}

  currenMonthDates():any{
  var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
  var StartDate = new Date(y, m, 1);
  var EndDate = new Date(y, m + 1, 0);
  return {
    startDate: StartDate,
    endDate: EndDate
  };
}

//#engregion 
}

var filterParams = {
  suppressAndOrCondition: true,
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
};
