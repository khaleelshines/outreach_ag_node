import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CellClickedEvent, GridOptions, IDatasource, IGetRowsParams } from 'ag-grid-community';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { TaskApiService } from 'src/services/taskservice.services';
import { UtilitiesService } from 'src/services/utilities.services';

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css'],
  providers: [ContactsComponent, SequenceComponent],
})
export class TasklistComponent implements OnInit {
  //#region ag-grid
  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public autoGroupColumnDef;
  public cacheOverflowSize;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: any;
  public recordscount: any;
  public rowSelection;
  paginationPageSize: number;
  cacheBlockSize: number;
  //#endregion
  globalSearch:any="";
  mainTab:string = "duedate";
  periodType:string = "today";  
  public taskData: any;
public redirectUrl:boolean=false;
//#region date filter drop down list
 todayDate:string;
 currentWeekfromto: string;
 lastWeekfromto: string;
 thisMonthfromto: string; 
 //#endregion

  constructor(
    private spinner: NgxSpinnerService,
    private taskService:TaskApiService,
    public datepipe: DatePipe,
    private router:Router,
    private route:ActivatedRoute,
    private commonservice:UtilitiesService
  ) {

    this.columnDefs = [
     {
        headerName: "Title",
        field: "title",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        cellRenderer: function(params) {
          if (params.value != undefined) 
          { 
          return "<a href='javascript:void(0);'>"+params.value+"</a>";
          }
      }
    },
      {
        headerName: "Related To",
        field: "accountname",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Contact Name",
        field: "contactname",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Assignee Name",
        field: "assigneename",
        width:150,
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        // cellRenderer: (data) => {
        //   if (data.value != undefined) 
        //   {                 
         
        //   return '<a href="javascript:void(0);">' + data.value + '</a>';
        //    }
        // }
      },    
      {
        headerName: "Task Type",
        field: "tasktype",
        filter: 'agTextColumnFilter',
        width:110,
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Priority",
        field: "priority",
        width:100,
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        cellRenderer: function (params) {
          if (params.value == 'high' || params.value == 'High') {
            return "<span class='badge badge-soft-danger'>" + params.value + "</span>";
          }else if(params.value == 'medium' || params.value == 'Medium'){
            return "<span class='badge badge-soft-primary'>" + params.value + "</span>";
          }else if(params.value == 'low' || params.value == 'Low'){
            return "<span class='badge badge-soft-success'>" + params.value + "</sapn>";
          }else if(params.value){
            return "<span class=''>" + params.value + "</sapn>";
          }
        }
      },
      // {
      //   headerName: "Assigned To",
      //   field: "AssigneeName",
      //   filter: 'agTextColumnFilter',
      //   filterParams: {
      //     suppressAndOrCondition: true
      //   },
      // },
      {
        headerName: "Due Date",
        field: "duedate",
        filter: 'agDateColumnFilter',
        filterParams: this.commonservice.filterParams,
        width:100,
        
      },
      {
        headerName: "Status",
        field:"status",
        width:100,
        filter:false,
        
      },
      
      // {
      //   headerName: "Reply Status",
      //   field:"IsReplied",
      //   filter:false,
      //   cellRenderer: function(params) {
      //     if(params.value > 0){
      //       return "Replied";
      //     }else{
      //       return "Not Replied";
      //     }
           
      //    }
      // }
    ];
  
    this.defaultColDef = {
      editable: false,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true
      //  width: 250
    };
   }
  gridOptions: GridOptions = {
    pagination: true,
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52,   
    angularCompileHeaders: true
  }; 
  ngOnInit() {
    this.datesList();
    this.route.params.subscribe(params=>{
      let taskFilter = params['type'];
      if(taskFilter != undefined && taskFilter != ""){
        this.redirectUrl = true;
        switch (taskFilter) {
          case 'en-thisweek' || undefined:
            this.EngagedChangedHandler("thisweek");
            break; 
          case 'today':
              this.EngagedChangedHandler("today");            
              break;                  
          default:
            this.EngagedChangedHandler("thisweek");
            break;
        }
      }
     });
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setDatasource(this.dataSource);
    
  }

  onPageSizeChanged(newPageSize) {    
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  } 

  onCellClicked($event: CellClickedEvent) {
    let id = $event.data.id;   
   if($event.colDef.field == "title"){
    this.router.navigate(['/tasks/viewtask/'+id]);
   }
   
 }

 onFilterTextBoxChanged(event: any) {
  let searchText = event.currentTarget.value;
  if (searchText.length > 0) {
    this.globalSearch = searchText;   
    this.gridApi.setDatasource(this.dataSource);
  } else{
    this.globalSearch ="";
    this.gridApi.setDatasource(this.dataSource);
  }
  
}

 
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();
      this.spinner.show();
   this.taskService.
   getTasksList(params, this.gridOptions.paginationPageSize,this.globalSearch,
      this.mainTab,this.periodType)
        .subscribe(data => {
          if(data.Code == "SUC-200"){
          this.taskData = JSON.parse(data.Data);
          
          params.successCallback(
            this.taskData.ListOfTasks,
            this.taskData.TasksCount
          );          
          }
          this.spinner.hide();          
        });
     
    }
  }

  mainFilterTabsChanged(clickedText:string,mainTab:string) {     
    this.mainTab = mainTab.toLowerCase(); 
    if(this.mainTab == 'overdue'){
      this.periodType = 'overdue';
    }else{
      this.periodType = 'today';
    }  
    this.gridApi.setDatasource(this.dataSource);

  }
  EngagedChangedHandler(clickedText) {
   this.mainTab = "duedate";
    clickedText = clickedText.toLowerCase();
    this.periodType = clickedText;
    if(this.redirectUrl == true){
      this.redirectUrl = false;
      this.router.navigate(["/task"]);
    }
   
    this.gridApi.setDatasource(this.dataSource);
  }

  //#region for start and end dates functions 
  datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 

}
