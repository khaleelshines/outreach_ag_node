import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GridOptions, IDatasource, IGetRowsParams } from 'ag-grid-community';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { templateService } from 'src/services/templates.services';
import { UtilitiesService } from 'src/services/utilities.services';
import Froalaeditor from "froala-editor";
import "froala-editor/js/plugins/link.min.js";
import "froala-editor/js/plugins/image.min.js";
import "froala-editor/js/plugins/file.min.js";
import { AppSettings } from 'src/app/shared/app.settings';

@Component({
  selector: 'app-templateslist',
  templateUrl: './templateslist.component.html',
  styleUrls: ['./templateslist.component.css']
})
export class TemplateslistComponent implements OnInit {
  //#region ag-grid
  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public autoGroupColumnDef;
  public cacheOverflowSize;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: any;
  public recordscount: any;
  public rowSelection;
  paginationPageSize: number;
  cacheBlockSize: number;
  //#endregion
//#region date filter drop down list
todayDate:string;
currentWeekfromto: string;
lastWeekfromto: string;
thisMonthfromto: string; 
//#endregion
globalSearch:any="";
mainTab:string = "createddate";
periodType:string = "thismonth";
public templateData: any;
templateForm: FormGroup;
templateEditForm: FormGroup;
submitted = false;
options;
get f() {
  return this.templateForm.controls;
}
get f1() {
  return this.templateEditForm.controls;
}
@ViewChild("closeAddTemplateModal", { static: false })
closeAddTemplateModal: ElementRef;
@ViewChild("closeEditTemplateModal", { static: false })
closeEditTemplateModal: ElementRef;
 //#region template
 responseInfo:string;

 //#endregion
  //#region modal
  modalResponseRef:BsModalRef;
  deletemodalResponseRef:BsModalRef;
  updateTemplatemodalRef: BsModalRef;
  //#endregion
  //#region actions on template
  deleteRecodeId:string;
  templateIntId:number;
  templateBody:any;
  //#endregion
  constructor(
    private spinner: NgxSpinnerService,
    public datepipe: DatePipe,
    private commonservice:UtilitiesService,
    private templateservice:templateService,
    private formBuilder: FormBuilder,
    private modalBsServiceRef:BsModalService,
    private appsettings:AppSettings
  ) {

    this.columnDefs = [
      {
        headerName: "Template Name",
        field: "templatename",
        width: 450,
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Template Subject",
        field: "subject",
        width: 400,
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },    
      {
        headerName: "Created On",
        field: "createddate",
        width: 150,
        filter: 'agDateColumnFilter',
        filterParams: this.commonservice.filterParams
      },{
        headerName:'Actions',
        width: 150,
        editable: false,
          sortable:false,
          cellStyle: { "padding-top": "2px" },
          cellRenderer: function(params){
          return `<div class="">    
          <span class="view-badge d-inline-block" title="Update" data-action-type="updateTemplate" data-toggle="modal" data-target='#updateTemplateModal'>
              <i class="fa fa-pencil-alt pointer" data-action-type="updateTemplate"></i>
            </span>
            <span class="view-badge d-inline-block" title="Delete" data-action-type="deleteTemplate" data-toggle="modal" data-target='#confirmationTemplateDeleteModal'>
              <i class="fa fa-trash" data-action-type="deleteTemplate"></i>
            </span>
          </div> `;
        }
      }
    ];
   }
   gridOptions: GridOptions = {
    pagination: true,    
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52,   
    angularCompileHeaders: true
  }; 
  ngOnInit() {
    this.datesList();
    this.templateForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required],
      MailType: "contacttemplate"
    });
    this.templateEditForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required]
        });
    Froalaeditor.DefineIcon("custom-field", { NAME: "cog", SVG_KEY: "cogs" });
    Froalaeditor.RegisterCommand("custom-field", {
      title: "Personalize",
      type: "dropdown",
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        "{{Contact: First Name}}": "First Name",
        "{{Contact: Last Name}}": "Last Name",
        "{{Contact: Designation}}": "Designation",
        "{{Contact: PhoneNumber}}": "PhoneNumber",
        "{{Account: AccountName}}": "AccountName",
        "<a href>unsubscribe now</a>": "Unsubscribe"
      },
      callback: function(cmd, val) {
        this.html.insert(val);
      }
    });

    this.options = this.appsettings.editorOptions;
  }
  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setDatasource(this.dataSource);    
  }
  onPageSizeChanged(newPageSize) {    
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  } 
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText.length > 0) {
      this.globalSearch = searchText;   
      this.gridApi.setDatasource(this.dataSource);
    } else{
      this.globalSearch ="";
      this.gridApi.setDatasource(this.dataSource);
    }
    
  }
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();
      this.spinner.show();
   this.templateservice.
   getTemplatesList(params, this.gridOptions.paginationPageSize,this.globalSearch,
      this.mainTab,this.periodType)
        .subscribe(data => {
          if(data.Code == "SUC-200"){
          this.templateData = JSON.parse(data.Data);
          
          params.successCallback(
            this.templateData.ListOfTemplates,
            this.templateData.TemplatesCount
          );          
          }
          this.spinner.hide();          
        });
     
    }
  }

  AddTemplate(template: TemplateRef<any>) { 
    this.submitted = true;
    if (this.templateForm.invalid) {
      return;
    }
    this.templateForm.patchValue({ MailType: "contacttemplate" });
    this.templateservice
      .postTemplate(this.templateForm.value)
      .subscribe(data => {
        if (data.Status === "Success") {  
          this.gridApi.setDatasource(this.dataSource);
          this.onReset();          
        }else if(data.Code === 'ERR-4001' && data.Data === 'duplicate'){
          this.responseInfo = `<div class="alert alert-danger mb-0">     
          <h6>Template name is already exists, Please try with another one..!</h6> 
                </div> `;  
        this.modalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
        setTimeout(() => {
          this.modalResponseRef.hide();
        }, 3000);     
        }      
        this.closeAddTemplateModal.nativeElement.click();       
 
      });
  }

   //#region delete template
   public onRowClicked(e) {
    if (e.event.target !== undefined || e.event.target !== null) {
      let actionType = e.event.target.getAttribute("data-action-type");
      let recordUpdateData = e.node.data;
      if(actionType == 'deleteTemplate'){
        this.deleteRecodeId = recordUpdateData.templateid;           
      }else if(actionType == 'updateTemplate'){
        this.templateIntId = recordUpdateData.templateid;
        this.templateBody = recordUpdateData.customtext;
        console.log("update record date:"+JSON.stringify(recordUpdateData));
        let formFields = {
          TemplateName: recordUpdateData.templatename,
          Subject: recordUpdateData.subject,
          HtmlTemplateBody: recordUpdateData.customtext
        };
        
        this.templateEditForm.setValue(formFields);
      }
    }
  }
  deleteTemplateYes(template: TemplateRef<any>) {
    this.spinner.show();
    this.templateservice.deleteTemplate(this.deleteRecodeId).subscribe(data => {
    if (data.Status === 'Success') {
      this.responseInfo = `
      <div class="confirmation-content success">
        <div class="icon confirm text-center">
        <i class="far fa-2x fa-check-circle text-success"></i>
        <h4>Deleted Successfully!!</h4>
        </div>            
      </div> `;   
      this.spinner.hide();
      this.deletemodalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.deletemodalResponseRef.hide();
        this.gridApi.setDatasource(this.dataSource);
      }, 2000);
    }
  }); 
  }
  //#endregion

//#region  update template
UpdateTemplate() {
  this.submitted = true;
  if (this.templateEditForm.invalid) {
    return;
  }

  let editFormObject = {
    TemplateName: this.templateEditForm.value.TemplateName,
    Subject: this.templateEditForm.value.Subject,
    HtmlTemplateBody: this.templateEditForm.value.HtmlTemplateBody,
    TemplateId: this.templateIntId
  };
 // this.closeEditTemplateModal.nativeElement.click(); 
 // alert(JSON.stringify(editFormObject));
  this.templateservice.UpdateTemplate(editFormObject).subscribe(data => {
    if (data.Status === "Success") {     
      this.gridApi.setDatasource(this.dataSource);
    }
    this.closeEditTemplateModal.nativeElement.click(); 
    this.onResetUpdateModal();
  });
}
//#endregion
  onReset() {
    this.submitted = false;
    this.templateForm.reset();
  } 

  onResetUpdateModal() {   
    this.submitted = false;
    this.templateEditForm.reset();    
  }

  EngagedChangedHandler(clickedText) {
   this.mainTab = "createddate";
    clickedText = clickedText.toLowerCase();
    this.periodType = clickedText;
    this.gridApi.setDatasource(this.dataSource);
  }

   //#region for start and end dates functions 
   datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 

}
