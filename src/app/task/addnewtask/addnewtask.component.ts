import { Component, OnInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaskApiService } from 'src/services/taskservice.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { TaskService } from '../task.service';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { ContactApiService } from 'src/services/contacts.services';
import { ViewcontactComponent } from 'src/app/contacts/viewcontact/viewcontact.component';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

interface TaskInfo {
  TaskId: string;
  TaskTitle: string;
  TaskDescription: string;
  DueDate: number;
  CompletedDate: number;
  TaskType: string;
  AssignedTo: string;
  AssigneeName: string;
  ShortAssigneeName: string;
  Priority: string;
  FirstName: string;
  Designation: string;
  TaskStatus: string;
  SequenceDetailId: number;
  TaskIntId: number;
  ElapsedDuration: string;
  AccountName: string;
  ObjectId: string;
  ObjectIntId: number;
  ContactEmail: string;
}
interface priority {
  priority: string;
  prioritycount: number;
}

@Component({
  selector: 'app-addnewtask',
  templateUrl: './addnewtask.component.html',
  styleUrls: ['./addnewtask.component.css'],
  providers: [ContactsComponent,ViewcontactComponent, SequenceComponent]
})
export class AddnewtaskComponent implements OnInit {
  priorityCollection$: Observable<priority[]>;
  tasklist$: Observable<TaskInfo[]> = new BehaviorSubject<TaskInfo[]>([]);
  total$: Observable<number>;
  TaskForm: FormGroup;
  usersData: any;
  Contacts: any;
  submitted = false;
  bsValue = new Date();
  mytime: Date = new Date();
  loggedInUserRole: string;
  contactInfo:any;
  tasks: any;
  activeTab = 'sequences';
    //#region declaration
    contactsList:any;
    selContactInfo:any;
    contactObj:any;
    contactnamerequired:boolean = false;
    contactTextFieldRequired:boolean = true;
    //#endregion
  IsMember: boolean;
  subscriptions:Subscription [] = [];
  get f() { return this.TaskForm.controls; }
  @Input() contactClickedFrom;
  constructor(private formBuilder: FormBuilder,private contacts: ContactsComponent, private contactview:ViewcontactComponent,   
    private taskService: TaskApiService,private teamService: teammanagementservice,public service: TaskService,
    private router:Router,private contactService: ContactApiService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
   // this.getContacts();   
    this.TaskForm = this.formBuilder.group({
      ObjectId: [''],
      Priority: ['',Validators.required],
      Title:  [ '',Validators.required],
      TaskType:  [ '',Validators.required],
      AssignedTo: [''],
      DueDate: [],
     // DueTime: [],
      Description: [],
      ObjectIntId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      AssignedToIntId: ['',Validators.required],
      AccountId:[]   
    });

   let teamMembersSub = this.teamService.getTeamMembersWithoutTeamId()
    .subscribe(data => {
      this.usersData = data;
    });
   this.subscriptions.push(teamMembersSub);

 
  }



    //#region contacts dropdown
    selectContact(){
      // this.accountintiderror = false;  
      // this.accountnameerror = false; 
       console.log(this.selContactInfo);
     }
  
    contactChangeHandler(selectedObject: any) {
      let searchTerm = selectedObject.term;
    
      let contactsArray = [];
      if ((searchTerm != undefined && searchTerm.length >= 3) ) {
        this.contactnamerequired = false;
        let accountsSub = this.contactService.getContactsbyname(searchTerm,0).subscribe(data => {
          if (data.Status == "Success") {
            let accounts = JSON.parse(data.Data);
            accounts.forEach(element => {
              let singleItem: any;
              singleItem = {
                id: element.id,
                name: element.contactname,
                accountid: element.accountintid
              };
  
              contactsArray.push(singleItem);
            });
            this.contactsList = contactsArray;
            
          }
        });
        this.subscriptions.push(accountsSub);
      }
  
    }
  
      getContactInfoById(contactIntId:number):any{
       this.contactService.getContactView(contactIntId).subscribe(data => {
          if(data.Status == "Success"){
          this.contactObj = JSON.parse(data.Data);         
         // this.contactsList = contactsArray;         
          }
         });
      }
    //#endregion
  
    

    addTask() {
      this.submitted = true;
      let accountIntId = 0;
      let contactIntId = 0;
      if(this.selContactInfo != undefined && this.selContactInfo.id > 0){  
        accountIntId = this.selContactInfo.accountid;
        contactIntId = this.selContactInfo.id;
      }else{
        this.contactnamerequired = true;
        return;
      }
      if (this.TaskForm.invalid) {
        return;
      }
      this.spinner.show(); 
  
      console.log(this.contactClickedFrom);
      let date = new Date(this.bsValue);
      let timestamp = Math.floor(date.getTime() / 1000.0); 
      
      // let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
      // let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
      if(contactIntId > 0 && accountIntId > 0){
    
      this.TaskForm.patchValue({ DueDate: timestamp });
      this.TaskForm.patchValue({ ObjectIntId: contactIntId });
      this.TaskForm.patchValue({ AccountId: accountIntId });     
      let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
      this.TaskForm.patchValue({ TeamId: TeamInfo.TeamId });
      this.TaskForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
      this.taskService.addTask(this.TaskForm.value).subscribe(data => {
      
        if(this.loggedInUserRole === "ta"){
          this.service.TeamAdminTaskReload();
        }else if(this.loggedInUserRole === "member"){
          this.service.MemberTaskReload();
        }else{
          this.service.adminTaskReload();
        }
        this.tasklist$ = this.service.countries$;
        this.total$ = this.service.total$;
        this.priorityCollection$ = this.service.priorityCollection$;     
        this.mytime = new Date();      
        this.gettasks();      
        this.onReset();
        this.spinner.hide();
        this.router.navigate(['/task']);
      });
      }
    }

  onReset() {
    this.submitted = false;
    this.contactnamerequired = false;
    this.TaskForm.reset();
    this.router.navigate(['/task']);
  }

  gettasks() {
    this.taskService.getTaskByObjectId(this.contactClickedFrom).subscribe(data => {
      if (data.Status === 'Success') {
        let tasks1 = JSON.parse(data.Data);
        this.tasks = tasks1.ListOfTasks;
      }
    });
  }

}
