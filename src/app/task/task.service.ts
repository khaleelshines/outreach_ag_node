import {Injectable, PipeTransform} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {DecimalPipe, DatePipe} from '@angular/common';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';
import { ApiResponse } from 'src/model/apiresponse.model';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { TaskApiService } from 'src/services/taskservice.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';

interface TaskInfo { 
  TaskId:string;
  TaskTitle: string;
  TaskDescription: string;
  DueDate:number;
  CompletedDate:number;
  TaskType:string;
  AssignedTo:string;
  AssigneeName:string;
  ShortAssigneeName:string;
  Priority: string;
  FirstName: string;
  Designation:string;
  TaskStatus:string;
  SequenceDetailId:number;
  TaskIntId:number;
  ElapsedDuration:string; 
  AccountName:string;
  ObjectId:string;
  ObjectIntId:number;
  ContactEmail:string;
}
interface priority{
  priority:string;
  prioritycount:number;
}
interface SearchResult {
  countries: TaskInfo[];
  total: number;
  priorityList1:priority[];  
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: string; 
}

function matches(country: TaskInfo, term: string, pipe: PipeTransform) {
  return country.FirstName.toLowerCase().includes(term.toLowerCase())
    || country.Priority.toLowerCase().includes(term.toLowerCase())
    || country.TaskDescription.toLowerCase().includes(term.toLowerCase())
    || country.TaskTitle.toLowerCase().includes(term.toLowerCase())
    || country.TaskType.toLowerCase().includes(term.toLowerCase())
    || country.Designation.toLowerCase().includes(term.toLowerCase())
    || country.AccountName.toLowerCase().includes(term.toLowerCase());
}


@Injectable()
export class TaskService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _countries$ = new BehaviorSubject<TaskInfo[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private _priorityList= new BehaviorSubject<priority[]>([]);
private taskList:any;
private priorityList:any;
private masterTaskData:any;
private loggedInUserRole:string;
private loggedInIntUserId:string;
IsDataAvailable:boolean=true;
  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''   
  };  
 

  constructor(private pipe: DecimalPipe,private http:HttpClient,private taskService:TaskApiService,
    public datepipe: DatePipe,private auth:AuthService) {   
        

  }
 

  adminTaskReload(){       
    let teamId = 0;
    this.loggedInUserRole = this.auth.getRole();
   if(this.loggedInUserRole === 'admin'){
    this.loggedInIntUserId = "0";
   }else if (this.loggedInUserRole === 'ta'){
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));    
    teamId =TeamInfo.Id;
    this.loggedInIntUserId = "0";
   }
   else{
    this.loggedInIntUserId = this.auth.getIntUserId();
   }
   let currentDate = this.dateConvertion(new Date());
    let inputObject = {
      "StartDueDate":currentDate,
      "EndDueDate":currentDate,
      "DueDateType":"range",
      "AssignedTo":this.loggedInIntUserId,
      "TeamId": teamId
    }

    this.taskService.getTasksWithFilters(inputObject)
    .subscribe(data => {
      if (data.Status === 'Success') {
        if(data.Data != ""){
        let taskMasterData = JSON.parse(data.Data);       
        this.taskList = taskMasterData.ListOfTasks;
        this.priorityList = taskMasterData.ListOfPriorities;
        this.masterTaskData = taskMasterData;
        }else{
          this.taskList = [];
          this.priorityList = [];
         
        }
       this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.countries);
      this._total$.next(result.total);
      this._priorityList.next(result.priorityList1);
    });
    this._search$.next();
      }
    });
  }

  TeamAdminTaskReload(){    
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo')); 
    let currentDate = this.dateConvertion(new Date());
    
    let inputObject = {
      "StartDueDate":currentDate,
      "EndDueDate":currentDate,
      "DueDateType":"range",
      "TeamId":TeamInfo.Id        
    }
    
    this.taskService.getTasksWithFilters(inputObject)
    .subscribe(data => {
      if (data.Status === 'Success') {
        if(data.Data != ""){
        let taskMasterData = JSON.parse(data.Data);       
        this.taskList = taskMasterData.ListOfTasks;
        this.priorityList = taskMasterData.ListOfPriorities;
        this.masterTaskData = taskMasterData;
        }else{
          this.taskList = [];
          this.priorityList = [];             
        }
       this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.countries);
      this._total$.next(result.total);
      this._priorityList.next(result.priorityList1);
    });
    this._search$.next();
      }
    });
  }

  MemberTaskReload(){    
    this.loggedInIntUserId = this.auth.getIntUserId();
    let currentDate = this.dateConvertion(new Date());
    let inputObject = {
      "StartDueDate":currentDate,
      "EndDueDate":currentDate,
      "DueDateType":"range",
      "AssignedTo":this.loggedInIntUserId         
    }   
    
  
    this.taskService.getTasksWithFilters(inputObject)
    .subscribe(data => {
      if (data.Status === 'Success') {
        if(data.Data != ""){
        let taskMasterData = JSON.parse(data.Data);       
        this.taskList = taskMasterData.ListOfTasks;
        this.priorityList = taskMasterData.ListOfPriorities;
        this.masterTaskData = taskMasterData;
        }else{
          this.taskList = [];
          this.priorityList = [];             
        }
       this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.countries);
      this._total$.next(result.total);
      this._priorityList.next(result.priorityList1);
    });
    this._search$.next();
      }
    });
  }

  prioritySearchClick(clickedText){
    clickedText = clickedText.toLowerCase();
  
        let taskMasterData = this.masterTaskData; 
        let allCount = taskMasterData.ListOfTasks.length;
        if(clickedText == "all"){         
          this.taskList = taskMasterData.ListOfTasks;
          this.priorityList = taskMasterData.ListOfPriorities;
        }else{
          taskMasterData = taskMasterData.ListOfTasks.filter(a=>a.Priority.toLowerCase() == clickedText);  
          this.taskList = taskMasterData;
          this.priorityList = [{'priority':'all','prioritycount':allCount},{'priority':clickedText,'prioritycount':taskMasterData.length}];
        }  
       
       this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.countries);
      this._total$.next(result.total);
      this._priorityList.next(result.priorityList1);
    });
    this._search$.next();
     
  }

  TimePeriodFilter(clickedText){
    this._state.page = 1;
    clickedText = clickedText.toLowerCase();  
    this.loggedInUserRole = this.auth.getRole();       
    var thisWeekDates=this.weekDates();

    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeknextday = thisWeekDates.endDate;   
  

    var nextWeekDates=this.nextWeekDates();
    var nextweekfirstday = nextWeekDates.startDate; // 06-Jul-2014
    var nextweeknextday = nextWeekDates.endDate;

    let currentweekfirstdate = this.dateConvertion(currentweekfirstday);
    let currentweeknextdate = this.dateConvertion(currentweeknextday);

    let nextweekfirstdate = this.dateConvertion(nextweekfirstday);
    let nextweeknextdate = this.dateConvertion(nextweeknextday);
    let currentDate = this.dateConvertion(new Date());
    let startdueDate:string;
    let enddueDate:string;
    let taskStatus:string = "";
   switch (clickedText) {
     case 'today':
       startdueDate = currentDate;
      enddueDate= currentDate;     
       break;
   case 'thisweek':
      startdueDate = currentweekfirstdate;
      enddueDate= currentweeknextdate;     
     break;
     case 'nextweek':
        startdueDate = nextweekfirstdate;
        enddueDate= nextweeknextdate; 
       break;
       case 'due':
        startdueDate = "";
        enddueDate= ""; 
        taskStatus= "overdue";
       break;
       case 'all':
        startdueDate = "";
       enddueDate= ""; 
       taskStatus= "all";    
        break;
     default:
        startdueDate = "";
      enddueDate= ""; 
       break;
   }
  let teamId = 0;
   if(this.loggedInUserRole === 'admin'){
    this.loggedInIntUserId = "0";
   }else if (this.loggedInUserRole === 'ta'){
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));    
    teamId =TeamInfo.Id;
    this.loggedInIntUserId = "0";
   }
   else{
    this.loggedInIntUserId = this.auth.getIntUserId();
   }
    let inputObject = {
      "StartDueDate":startdueDate,
      "EndDueDate":enddueDate,
      "DueDateType":"range",
      "AssignedTo":this.loggedInIntUserId,
      "TeamId": teamId,
      "FilterStatus":taskStatus,
      "Purpose":""
    }
  console.log(inputObject);
    this.taskService.getTasksWithFilters(inputObject)
    .subscribe(data => {
      if (data.Status === 'Success') {
        if(data.Data != ""){
        let taskMasterData = JSON.parse(data.Data);       
        this.taskList = taskMasterData.ListOfTasks;
        this.priorityList = taskMasterData.ListOfPriorities;
        this.masterTaskData = taskMasterData;
        }else{
          this.taskList = [];
          this.priorityList = [];
         
        }
       this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.countries);
      this._total$.next(result.total);
      this._priorityList.next(result.priorityList1);
    });
    this._search$.next();
      }
    });
  }

  dateConvertion(inputDate:any):string{
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+1);
    EndDate.setDate(today.getDate()-day+7);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day-6);
    EndDate.setDate(today.getDate()-day);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  dataStatus(){
    return this.IsDataAvailable;
  }

  get countries$() { return this._countries$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }
  get priorityCollection$(){return this._priorityList.asObservable();}
  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
 

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;
    // 1. sort   
   this.IsDataAvailable = true;
    let priorityList1 = this.priorityList;
    let countries = this.taskList;
    // 2. filter
    countries = countries.filter(country => matches(country, searchTerm, this.pipe));
    const total = countries.length;

    // 3. paginate
    countries = countries.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    this.IsDataAvailable = total < 0?false:true;
    return of({countries, total,priorityList1});
  } 

 
}
