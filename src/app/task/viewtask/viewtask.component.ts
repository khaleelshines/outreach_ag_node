import { Component, OnInit, TemplateRef } from '@angular/core';
import { Location } from '@angular/common';
import { TaskApiService } from 'src/services/taskservice.services';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css']
})
export class ViewtaskComponent implements OnInit {

  constructor(private location: Location,
    private taskService:TaskApiService,
    private route: ActivatedRoute,
    private modalService: BsModalService) { }
  taskdata: any;
  taskid:number;
  //#region task action info
  bsValue = new Date();
  rescheduleTaskId: string;
  rescheduleTaskmodalRef: BsModalRef;
  //#endregion
  ngOnInit() {
    this.route.params.subscribe(params => {      
      this.taskid = params["id"];
    // this.taskid = 78773;
      this.gettaskinfo(this.taskid);
    });
  }

  gettaskinfo(taskid:number) {
    this.taskService.getTaskInfoByTaskId(taskid)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.taskdata = data.Data[0];           
        }
      });
  }

   //#region  task actions
 markAsCompleted(id: number, taskid: number) {

  this.taskService.updateTaskAsCompleted(id, taskid).subscribe(data => {
    if (data.Status.toLowerCase() === 'success') {
      this.gettaskinfo(this.taskid);
    }
  });
}

rescheduleTaskBtnClick(template: TemplateRef<any>, taskId: string) {
  this.rescheduleTaskId = taskId;
  this.rescheduleTaskmodalRef = this.modalService.show(template, { class: 'second',backdrop  : 'static' });
}
rescheduleConfirmClick() {
  let date = new Date(this.bsValue); 
  let fullDate = date.setHours(5,30,0);
  let rescheduleDate = Math.floor(new Date(fullDate).getTime() / 1000.0);

  let taskObj = {
    TaskId: this.rescheduleTaskId,
    FieldName: 'duedate',
    FieldValue: rescheduleDate
  };
  this.taskService.updateTaskInfo(taskObj).subscribe(data => {
    if (data.Status.toLowerCase() === 'success') {
      this.rescheduleTaskmodalRef.hide();
      this.gettaskinfo(this.taskid);
    }
  });

}
 //#endregion

  previousPage(){
    this.location.back();
  }
}
