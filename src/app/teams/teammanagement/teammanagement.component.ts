import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { teammanagementservice } from "src/services/teamsmanagement.services";

import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { UserService } from 'src/services/userservice.services';

@Component({
  selector: "app-teammanagement",
  templateUrl: "./teammanagement.component.html",
  styleUrls: ["./teammanagement.component.css"]
})
export class TeammanagementComponent implements OnInit,OnDestroy {
  modalRef: BsModalRef | null;
  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  nonTeamMembersmodalRef: BsModalRef;
  teamInfo: any;
  teamname: string;
  topEmployee: any;
  TeamMembers: any;
  NonTeamMembers: any;
  enableDelete: boolean = false;
  enableAddMembers: boolean = false;
  btnDeleteTeamMembers: boolean = false;
  clickedUsersArray: any;
  usersData: any;
  IsTeamAdminsListOpened: boolean = false;
  IsSubTeamNotMentioned: boolean = false;
  subscriptions:Subscription [] = [];
  constructor(
    private modalService: BsModalService,
    private teamsService: teammanagementservice,
    private spinner: NgxSpinnerService,
    private userService: UserService
  ) {}

  // topEmployee:any = this.teamsFromService;
  // topEmployee = {"name":"TeamA-Parent","cssClass":"","imageUrl":"","designation":"","subordinates":[{"name":"TeamA-Child","cssClass":"","imageUrl":"","designation":"","subordinates":[]},{"name":"TeamA-SubChild","cssClass":"","imageUrl":"","designation":"","subordinates":[]}]};

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    this.getTeams();
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  getUsers() {
  let teamUsersSub =  this.teamsService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
    // let teamUsersSub =  this.userService.getUsers().subscribe(data => {
    //   this.usersData = data;
    // });
    this.subscriptions.push(teamUsersSub);
  }

  getTeams() {
  let teamsSub =  this.teamsService.getteams().subscribe(data => {
      if (data.Code === "SUC-200") {
        let teamServiceData = JSON.parse(data.Data);
        this.topEmployee = teamServiceData;
        console.log("teams object:"+JSON.stringify(teamServiceData));
      }
    });
    this.subscriptions.push(teamsSub);
  }

  subteamtxt(event: any) {
    let subTeamName = event.target.value;
    if (subTeamName.length > 0 && subTeamName != undefined) {
      //  alert("khaleel..");
      this.IsSubTeamNotMentioned = false;
    }
  }

  addTeam() {
    let teamName = (<HTMLInputElement>document.getElementById("subteam")).value;
    let parentId = this.teamInfo.Id;
    if (teamName === "" || teamName === undefined) {
      this.IsSubTeamNotMentioned = true;
    } else {
      this.IsSubTeamNotMentioned = false;
      let postTeamObj = {
        TeamName: teamName,
        ParentTeamId: parentId
      };
      this.teamsService.addteam(postTeamObj).subscribe(data => {
        if (data.Status === "Success") {
          this.modalRef2.hide();
          this.getTeams();
        }
      });
    }
  }

  deleteTeamConfirmation() {
    let teamId = this.teamInfo.TeamId;
    let TeamObj = {
      TeamId: teamId
    };
    this.teamsService.deleteteam(TeamObj).subscribe(data => {
      if (data.Status === "Success") {
        this.modalRef3.hide();
        this.getTeams();
      }
    });
  }

  clickedTeam(event: any, template: TemplateRef<any>) {
    // this.enableDelete = true;
    // if (event.ParentTeamId === "0") {
    //   this.enableDelete = false;
    // }
     
    this.teamInfo = event;
    let teamMembersSub =  this.teamsService.getteammembersList(event.Id).subscribe(data => {
      if (data.Code === "SUC-200") {
        let teamServiceData = JSON.parse(data.Data);
        let teamMembers = teamServiceData.teamMembers;
        this.teamInfo.teamOwner = teamServiceData.teamOwner;
        this.teamInfo.ownerEmail = teamServiceData.ownerEmail;
        let usersArray = [];
        let usersInfo = [];
        if (teamMembers.length > 0) {
          for (let singleMember of teamMembers) {
            usersArray.push(singleMember.UserId);
            usersInfo.push(singleMember);
            this.clickedUsersArray = usersArray;
          }
        }
        this.teamInfo.teamMembers = usersInfo;
        this.modalRef = this.modalService.show(template, { class: "modal-m" });
      }
    });
    this.subscriptions.push(teamMembersSub);
    
  }

  addteamBtnClick(template: TemplateRef<any>, teamInfo: any) {
    this.modalRef.hide();
    this.modalRef2 = this.modalService.show(template, { class: "second" });
  }

  deleteteamBtnClick(template: TemplateRef<any>) {
    this.modalRef.hide();
    this.modalRef3 = this.modalService.show(template, { class: "second" });
  }

  viewteammembers(template: TemplateRef<any>) {
    this.TeamMembers = [];
    this.TeamMembersIdsArray = [];
    let teamIntId = this.teamInfo.Id;
    this.teamsService.getteammembers(teamIntId).subscribe(data => {
      this.TeamMembers = data;
      let usersArray = [];
      if (data.length > 0) {
        for (let singleMember of data) {
          usersArray.push(singleMember.UserId);
          this.clickedUsersArray = usersArray;
        }
        this.modalRef4 = this.modalService.show(template, { class: "second" });
      }
    });
  }

  TeamMembersIdsArray: any = [];
  onChangeTeamMembers(event, members: any) {
    // Use appropriate model type instead of any
    this.btnDeleteTeamMembers = true;

    if (event.currentTarget.checked === true) {
      this.TeamMembersIdsArray.push(members.UserId);
    } else {
      this.TeamMembersIdsArray.forEach((item, index) => {
        if (item === members.UserId) this.TeamMembersIdsArray.splice(index, 1);
      });
    }

    if (this.TeamMembersIdsArray.length < 1) {
      this.btnDeleteTeamMembers = false;
    }
  }

  deleteTeamMembers() {
    this.teamsService
      .deletemembersfromteam(this.TeamMembersIdsArray)
      .subscribe(data => {
        if (data.Status === "Success") {
          // this.TeamMembersIdsArray.forEach((item, index) => {
          //   if (item === members.UserId)
          //     this.TeamMembersIdsArray.splice(index, 1);
          // });
          this.btnDeleteTeamMembers = false;
          this.teamInfo.teamMembers = [];
          let teamIntId = this.teamInfo.Id;
          this.teamsService.getteammembers(teamIntId).subscribe(data => {
            let responseData = data;
            this.teamInfo.teamMembers = responseData;
            // this.TeamMembers = responseData;
            let usersArray = [];
            for (let singleMember of responseData) {
              usersArray.push(singleMember.UserId);
              this.clickedUsersArray = usersArray;
            }
          });
        }
      });
  }

  addMembersToTeamModalPopUp(template: TemplateRef<any>) {
    this.nonTeamMembersArray = [];
    this.teamsService
      .getnonteammembers(this.clickedUsersArray)
      .subscribe(data => {
        if (data.Status === "Success") {
          let collectionUsers = JSON.parse(data.Data);
          this.NonTeamMembers = collectionUsers;
        }
      });
    this.nonTeamMembersmodalRef = this.modalService.show(template, {
      class: "second"
    });
  }

  nonTeamMembersArray: any = [];
  onChangeCategory(event, members: any) {
    // Use appropriate model type instead of any
    this.enableAddMembers = true;
    let singleMember = {
      MemberId: members.UserId,
      MemberIntId: members.IntUserId,
      MemberType: members.UserType,
      TeamId: this.teamInfo.TeamId,
      TeamIntId: this.teamInfo.Id
    };
    if (event.currentTarget.checked === true) {
      this.nonTeamMembersArray.push(singleMember);
    } else {
      this.nonTeamMembersArray.forEach((item, index) => {
        if (item.MemberIntId === singleMember.MemberIntId)
          this.nonTeamMembersArray.splice(index, 1);
      });
    }

    if (this.nonTeamMembersArray.length < 1) {
      this.enableAddMembers = false;
    }
  }

  addMembersToTeam() {
    let selectedMembers = this.nonTeamMembersArray;
    this.teamsService.addmemberstoteam(selectedMembers).subscribe(data => {
      if (data.Status === "Success") {
        this.nonTeamMembersmodalRef.hide();
        this.teamInfo.teamMembers = [];
        let teamIntId = this.teamInfo.Id;
        this.teamsService.getteammembers(teamIntId).subscribe(data => {
          let responseData = data;
          this.teamInfo.teamMembers = responseData;
          // this.TeamMembers = responseData;
          let usersArray = [];
          for (let singleMember of responseData) {
            usersArray.push(singleMember.UserId);
            this.clickedUsersArray = usersArray;
          }
        });
      }
    });
  }

  openTeamAdminsList() {
    this.IsTeamAdminsListOpened = this.IsTeamAdminsListOpened ? false : true;
  }

  onChangeTeamAdmin(event, teamId) {
    let selectedUserIntId =
      event.target["options"][event.target["options"].selectedIndex].value;
    let selectedTeamIntId = teamId;
    this.teamsService
      .updateteamadmin(selectedUserIntId, selectedTeamIntId)
      .subscribe(data => {
        if (data.Status === "Success") {
          this.teamsService
            .getTeamInfoById(selectedTeamIntId)
            .subscribe(data => {
              if (data.Status === "Success") {
                this.teamInfo = JSON.parse(data.Data);
                this.IsTeamAdminsListOpened = false;
                // this.modalRef.hide();
              }
            });
        }
      });
  }
}
