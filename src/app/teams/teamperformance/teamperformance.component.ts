import { Component, OnInit } from '@angular/core';
import { templateService } from "src/services/templates.services";
import { Router } from "@angular/router";
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { teamPerformanceService } from 'src/services/teamperformance.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-teamperformance',
  templateUrl: './teamperformance.component.html',
  styleUrls: ['./teamperformance.component.css'],
  providers: [DatePipe]
})
export class TeamperformanceComponent implements OnInit {
  public columnDefs;
  private gridApi;
  private gridColumnApi;
  public defaultColDef;
  paginationPageSize: number;
  cacheBlockSize: number;
  //#region performance
  public performanceList:any;
  performancePeriodSelect:string = 'thismonth';
  //#endregion
  todayDate:string;
  currentWeekfromto: string;
  lastWeekfromto: string;
  thisMonthfromto: string;

  constructor(
    private templateApiService: templateService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private teamPerformance:teamPerformanceService,
    public datepipe: DatePipe
    ) {

      this.buildColdefs();  

     }

  ngOnInit() {
    this.datesList();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getTeamPerformanceData('thismonth');
  }

  PeriodChangedHandler(selectedValue:string){
    this.getTeamPerformanceData(selectedValue.toLowerCase());
  }

  getTeamPerformanceData(period:string){
    this.spinner.show();
    this.teamPerformance.getTeamPerformance(period).subscribe(data=>{
      if (data.Code === "SUC-200") {
        let teamPerformanceData = JSON.parse(data.Data);
         this.performanceList = teamPerformanceData;
         this.spinner.hide();
        
      }
    });
  }

  gridOptions: GridOptions = {
    pagination: true,
    rowHeight: 40,
    headerHeight: 47,   
    cacheBlockSize: 10,    
  };

  buildColdefs() {
   
    this.columnDefs = [
      {
        headerName: '',
        children: [
          {
            headerName: 'Owner',
            field: 'MemberName',
            sortable: true,
            filter: true,
            width: 140,
            pinned: 'left'
          }
        ]
      },
      {
        headerName: 'Market Research (New)',
        children: [
          {
            headerName: '# Contacts',
            field: 'ContactsAdded',
            sortable: true,
            width: 110,
            suppressSizeToFit: true,
          },
          {
            headerName: '# Accounts',
            field: 'AccountsAdded',
            sortable: true,
            width: 110,
            suppressSizeToFit: true,
          }
        ]
      },
      {
        headerName: 'Email / Sequence Metrics',
        children: [
          {
            headerName: '# Contacts',
           // field: 'ContactsEngaged',
            cellRenderer: function(params) {
              return (
                '<span title="Contacts Engaged">' +params.data.NewContactsEngaged +'</span>'
              );
            },
            sortable: true,
            width: 160,
            suppressSizeToFit: true
          },
          // {
          //   headerName: '# New Contacts Engaged',
          //   field: 'NewContactsEngaged',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true
          // },
          {
            headerName: '# Accounts',
          //  field: 'AccountsEngaged',
            cellRenderer: function(params) {
              return (
                '<span title="Accounts Engaged">' +params.data.NewAccountsEngaged +'</span>'
              );
            },
            sortable: true,
            width: 160,
            suppressSizeToFit: true,
            enableRowGroup: true
          },
          // {
          //   headerName: '# New Accounts Engaged',
          //   field: 'NewAccountsEngaged',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true,
          //   enableRowGroup: true
          // },
          {
            headerName: '# Emails',
           // field: 'EmailsSent',
            cellRenderer: function(params) {
              return (
                '<span title="Delivered">' +params.data.EmailsDelivered +'</span>/<span title="Total Emails">'+params.data.EmailsSent +
                '</span><span class="ml-1" title="Deliverability">('+params.data.Deliverability+'%)</span>'
              );
            },
            sortable: true,
            width: 150,
            suppressSizeToFit: true,
          },
          // {
          //   headerName: '# Emails Delivered',
          //   field: 'EmailsDelivered',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true,
          // },
          {
            headerName: '# R/B/O/C',
           // field: 'EmailsOpened',
            cellRenderer: function(params) {
              return (
                '<span title="Replies">' +params.data.EmailsReplied +'</span>/<span title="Bounced">'+params.data.EmailsBounced +'</span>/<span title="Opened">' +params.data.EmailsOpened 
                +'</span>/<span title="Clicks">' +params.data.EmailsClicked +'</span>'
              );
            },
            sortable: true,
            width: 150,
            suppressSizeToFit: true,
          },
          {
            headerName: 'Open Rate(%)',
            field: 'OpenRate',
            sortable: true,
            width: 120,
            suppressSizeToFit: true,
          },
          {
            headerName: 'Clickthrough Rate(%)',
            field: 'ClickRate',
            sortable: true,
            width: 150,
            suppressSizeToFit: true,
          }
          // {
          //   headerName: '# Clicks',
          //   field: 'EmailsClicked',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true,

          // },
          // {
          //   headerName: '# Replies',
          //   field: 'EmailsReplied',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true,
          // },
          // {
          //   headerName: '# Bounced',
          //   field: 'EmailsBounced',
          //   sortable: true,
          //   width: 200,
          //   suppressSizeToFit: true,
          // }
        ]
      },
      // {
      //   headerName: 'Analytics',
      //   children: [
      //     {
      //       headerName: 'Deliverability(%)',
      //       field: 'Deliverability',
      //       sortable: true,
      //       width: 200,
      //       suppressSizeToFit: true,

      //     },
      //     {
      //       headerName: 'Open Rate(%)',
      //       field: 'OpenRate',
      //       sortable: true,
      //       width: 200,
      //       suppressSizeToFit: true,

      //     },
      //     {
      //       headerName: 'Clickthrough Rate(%)',
      //       field: 'ClickRate',
      //       sortable: true,
      //       width: 200,
      //       suppressSizeToFit: true,

      //     },
      //     // {
      //     //   headerName: 'New Meetings Scheduled',
      //     //   field: 'ScheduledMeetings',
      //     //   sortable: true,
      //     //   width: 200,
      //     //   suppressSizeToFit: true,
      //     // },
      //     // {
      //     //   headerName: 'Meetings Conducted',
      //     //   field: 'ConductedMeetings',
      //     //   sortable: true,
      //     //   filter: 'agTextColumnFilter',
      //     //   width: 200,
      //     //   suppressSizeToFit: true,

      //     // }
      //   ]
      // }
    ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;

  }

  //#region for start and end dates functions

  
  datesList(){
    var today = new Date();
    this.todayDate = "( "+this.dateConvertion(today)+" )";

    var thisWeekDates = this.weekDates();
    var year = new Date().getFullYear();
    var currentweekfirstday = thisWeekDates.startDate; // 06-Jul-2014
    var currentweeklastday = thisWeekDates.endDate;
    this.currentWeekfromto = "( "+this.dateConvertion(currentweekfirstday) + " - " + this.dateConvertion(currentweeklastday)+" )";

    var lastWeekDates = this.lastWeekDates();
    var lastweekfirstday = lastWeekDates.startDate; // 06-Jul-2014
    var lastweeklastday = lastWeekDates.endDate;
    this.lastWeekfromto = "( "+this.dateConvertion(lastweekfirstday) + " - " + this.dateConvertion(lastweeklastday)+" )";

    var thisMonthDates = this.currenMonthDates();
    var thisMonthfirstday = thisMonthDates.startDate; // 06-Jul-2014
    var thisMonthlastday = thisMonthDates.endDate;
    this.thisMonthfromto = "( "+this.dateConvertion(thisMonthfirstday) + " - " + this.dateConvertion(thisMonthlastday)+" )";
    
  }

  
  dateConvertion(inputDate: any): string {
    return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
  }

  weekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day + 1);
    EndDate.setDate(today.getDate() - day + 7);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  nextWeekDates():any{
    var today = new Date();
    var day = today.getDay();
  
    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0,0,0,0);
    EndDate.setHours(0,0,0,0);
    StartDate.setDate(today.getDate()-day+8);
    EndDate.setDate(today.getDate()-day+14);
    return {
        startDate:StartDate,
        endDate: EndDate
    };
  }

  lastWeekDates(): any {
    var today = new Date();
    var day = today.getDay();

    var StartDate = new Date();
    var EndDate = new Date();
    StartDate.setHours(0, 0, 0, 0);
    EndDate.setHours(0, 0, 0, 0);
    StartDate.setDate(today.getDate() - day - 6);
    EndDate.setDate(today.getDate() - day);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

    currenMonthDates():any{
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();  
    var StartDate = new Date(y, m, 1);
    var EndDate = new Date(y, m + 1, 0);
    return {
      startDate: StartDate,
      endDate: EndDate
    };
  }

  //#engregion 

}