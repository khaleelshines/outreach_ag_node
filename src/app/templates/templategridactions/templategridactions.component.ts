import { Component, OnInit, TemplateRef, ElementRef,ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { templateService } from 'src/services/templates.services';
import Froalaeditor from "froala-editor";
import "froala-editor/js/plugins/link.min.js";
import "froala-editor/js/plugins/image.min.js";
import "froala-editor/js/plugins/file.min.js";
import { Subscription } from 'rxjs';
import { TemplatesComponent } from '../templates.component';
import { AppSettings } from 'src/app/shared/app.settings';

@Component({
  selector: 'app-templategridactions',
  templateUrl: './templategridactions.component.html',
  styleUrls: ['./templategridactions.component.css']
})
export class TemplategridactionsComponent implements OnInit {
  templates: any;
  data: any;
  params: any; 
  submitted = false;
  options;
  public rowData: any;f
  singleTemplateData: any;
  nonFilteredTemplates: any;
  templateForm: FormGroup;
  templateEditForm: FormGroup;
        
  deleteTemplatemodalRef: BsModalRef;
  updateTemplatemodalRef: BsModalRef;
  clickedTemplateId: string;
  subscriptions:Subscription [] = [];

  get f1() {
    return this.templateEditForm.controls;
  }
  @ViewChild("closeAddTemplateModal", { static: false })
  closeAddTemplateModal: ElementRef;

  constructor(private http: HttpClient, private router: Router,
    private modalService: BsModalService,
    private templateApiService: templateService,
    private formBuilder: FormBuilder,
     private templatesParent: TemplatesComponent,
     private appsettings:AppSettings) { 

  }

  agInit(params) {
    this.params = params;
    this.data = params.value;
  }

  ngOnInit() {

    this.templateForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required],
      MailType: "contacttemplate"
    });

    this.templateEditForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required]
    });

    this.getTemplates();

    Froalaeditor.DefineIcon("custom-field", { NAME: "cog", SVG_KEY: "cogs" });
    Froalaeditor.RegisterCommand("custom-field", {
      title: "Personalize",
      type: "dropdown",
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        "{{Contact: First Name}}": "First Name",
        "{{Contact: Last Name}}": "Last Name",
        "{{Contact: Designation}}": "Designation",
        "{{Contact: PhoneNumber}}": "PhoneNumber",
        "{{Account: AccountName}}": "AccountName",
        "<a href>unsubscribe now</a>": "Unsubscribe"
      },
      callback: function(cmd, val) {
          this.html.insert(val);
      }
    });

    this.options = this.appsettings.editorOptions;
  }
  
  getTemplates(): any {
    let templatesSub =  this.templateApiService.getTemplates().subscribe(data => {
        if (data.Status === "Success") {
          this.templates = JSON.parse(data.Data);
          this.rowData = JSON.parse(data.Data);
          this.nonFilteredTemplates = this.templates;
        } else {
          this.templates = undefined;
        }
      });
      this.subscriptions.push(templatesSub);
    }

  UpdateTemplateBtn(template: TemplateRef<any>) {
    let rowData = this.params;
    let templateId = rowData.data.TemplateId;
    this.clickedTemplateId = templateId;
    let templatesList = this.nonFilteredTemplates;

    this.singleTemplateData = templatesList.filter(
      a => a.TemplateId === templateId
    );

    let formFields = {Subject:this.singleTemplateData[0].Subject, TemplateName:this.singleTemplateData[0].TemplateName,  HtmlTemplateBody:this.singleTemplateData[0].HtmlTemplateBody};
    this.templateEditForm.setValue(formFields);

    this.updateTemplatemodalRef = this.modalService.show(template, {
      class: 'modal-lg', backdrop  : 'static'
    });
  }
  UpdateTemplate() {
    this.submitted = true;
    if (this.templateEditForm.invalid) {
      return;
    }

    let editFormObject = {
      TemplateName: (<HTMLInputElement>document.getElementById("templateName")).value,
      Subject: (<HTMLInputElement>document.getElementById("subject")).value,
      HtmlTemplateBody: (<HTMLInputElement>document.getElementById("body")).value,
      TemplateId: this.clickedTemplateId
    };
    // alert(JSON.stringify(editFormObject));
    this.templateApiService.UpdateTemplate(editFormObject).subscribe(data => {
      if (data.Status === "Success") {
        
        this.templateApiService.getTemplates().subscribe(data => {
          if (data.Status === "Success") {
            this.templates = JSON.parse(data.Data);
          }
          this.updateTemplatemodalRef.hide();
          this.templatesParent.getTemplates();
        });
      }
    });
  }

  deleteTemplateBtn(template: TemplateRef<any>) {
    let rowData = this.params;
    let templateId = rowData.data.TemplateId;
    this.clickedTemplateId = templateId;
    this.deleteTemplatemodalRef = this.modalService.show(template, {
      class: "second"
    });
  }

  deleteTemplateYes() {
    this.templateApiService
      .deleteTemplate(this.clickedTemplateId)
      .subscribe(data => {
        if (data.Status.toLowerCase() === "success") {
          this.deleteTemplatemodalRef.hide();
          this.templatesParent.getTemplates();
        }
      });
  }

  onReset() {
    this.submitted = false;
    this.templateForm.reset();
  } 

}
