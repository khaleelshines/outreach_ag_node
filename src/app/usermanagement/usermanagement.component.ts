import { Component, OnInit, ElementRef, ViewChild, OnDestroy, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, NavigationEnd } from "@angular/router";
import { UserService } from "src/services/userservice.services";
import { RoleApiService } from "src/services/roles.services";
import { CellCustomComponent } from "../custom_components/cell-custom/cell-custom.component";
import { GridOptions } from "ag-grid-community";
import { UsersActionsComponent } from "../custom_components/users-actions/users-actions.component";
import { UtilitiesService } from "src/services/utilities.services";
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AuthService } from '../guards/authguard/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: "app-usermanagement",
  templateUrl: "./usermanagement.component.html",
  styleUrls: ["./usermanagement.component.css"]
})
export class UsermanagementComponent implements OnInit,OnDestroy {
  userForm: FormGroup;
  submitted = false;
  selectedRoleIntId: Int32Array;
  Roles: any;
  UserData: any;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowData: any;
  public usersData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  selectedRoleName: string;
  ObjectType: string;
  event: object;
  interval: any;
  deleteRecodeId:string;
  deleteRecodeIntId:number;
  responseInfo:string;
  deletemodalResponseRef:BsModalRef; 
  modalResponseRef:BsModalRef;
  subscriptions:Subscription [] = [];
  @ViewChild("closeAddUserModal", { static: false })
  closeAddUserModal: ElementRef;

  loggedInUserRole: string;
  IsMember: boolean;
  userInfo: any;
  IsUserExists:boolean = false; 

  public getContextMenuItems(params) {
    let columnName = params.column.colDef.headerName;
    if (columnName === "Actions") {
      return [];
    } else {
      var result = ["copy", "paste", "export", "autoSizeAll"];
      return result;
    }
  }
  get f() {
    return this.userForm.controls;
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private roleApiService: RoleApiService,
    public utilities: UtilitiesService,
    private spinner: NgxSpinnerService,
    private auth:AuthService,
    private modalBsService: BsModalService,
  ) {
  
    this.columnDefs = [
      {
        headerName: "Name",
        field: "Name"
      },
      {
        headerName: "Login Id",
        field: "LoginId"
      },
      {
        headerName: "Role",
        field: "objectType"
      },
      // {
      //   headerName: "Status",
      //   field: "Status"
      // },
      {
        headerName: "Email",
        field: "Email"
      },
      {
        headerName: "Phone No",
        field: "MobileNumber"
      },
      {
        headerName: "Actions",
        pinned: "right",
        width: 120,
        editable: false,
        cellStyle: { "padding-top": "2px" },
        cellRenderer: function(params){           
          return `<div class="d-flex justify-content-center"> 
          <span class="view-badge d-inline-block" title="Delete" data-action-type="deleteUser" data-toggle="modal" data-target='#confirmationContactDeleteModal'>
            <i class="fa fa-trash" data-action-type="deleteUser"></i>
          </span>
        </div>`;
        }
       // cellRendererFramework: UsersActionsComponent
      }
    ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true     
    };
    this.rowSelection = "multiple";   
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;

    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }else {
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId,
        companyId :userDetails1.CompanyInfo[0].CompanyId
      };
    }
  }
 
  gridOptions: GridOptions = {
    rowHeight: 52,
    headerHeight: 44,
    getContextMenuItems: this.getContextMenuItems
  };

 

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;  
  }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);      
    }
  }

  ngOnInit() {
    this.loadUserList();
   
    this.userForm = this.formBuilder.group({
      FirstName: ["", Validators.compose([Validators.required])],
      LastName: [""],
      Email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
         // Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      MobileNumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      RoleId: [
        this.selectedRoleIntId,
        Validators.compose([Validators.required])
      ],
      // LoginId: [
      //   "",
      //   [
      //     Validators.required,
      //     Validators.email,
      //     Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      //   ]
      // ],
      ObjectType: []
    });

    this.getCompanyRoles();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }
  loadUserList() {
    this.spinner.show();
   let usersSub = this.userService.getUsers1(this.userInfo).subscribe(data => {
      this.usersData = JSON.parse(data.Data);
      this.spinner.hide();  
    });
    this.subscriptions.push(usersSub);
  }

  // selectRoleChangeHandler(selectedvalue:Int32Array) {
  //   this.selectedRoleIntId = selectedvalue;
  // }

  selectRoleChangeHandler(event) {
    let selectElementText =
      event.target["options"][event.target["options"].selectedIndex].text;
    this.selectedRoleIntId = event.target.RoleIntId;
    this.selectedRoleName = selectElementText;
  }

  addUser(template: TemplateRef<any>) {
    this.submitted = true;
    if(this.IsUserExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
      <h6>Email already exist. Please try with another..!</h6> 
            </div> `; 
    this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
    setTimeout(() => {
      this.modalResponseRef.hide();
    }, 3000);
    return;
    }
    if (this.userForm.invalid) {
      return;
    }
    this.userForm.value.ObjectType =
      this.selectedRoleName == "employee" ? "member" : this.selectedRoleName;
    this.userService.addUser(this.userForm.value).subscribe(data => {
      if (data.Status === "Success") {
        this.closeAddUserModal.nativeElement.click();
        this.onReset();
       this.loadUserList();
      } else {
      }
    });
  }

  getCompanyRoles() {
   let companyRolesSub = this.roleApiService.getcompanyroles().subscribe(data => {
      if (data.Status === "Success") {
        this.Roles = JSON.parse(data.Data);
      } else {
        alert(data.Message);
      }
    });
    this.subscriptions.push(companyRolesSub);
  }
  getUsers() {
    this.userService.getUsers1(this.userInfo).subscribe(data => {
      this.usersData = JSON.parse(data.Data);
    });
  }

  onReset() {
    this.submitted = false;  
    this.IsUserExists = false; 
    this.userForm.reset();
  }

  //#region delete user
  public onRowClicked(e) {
    if (e.event.target !== undefined || e.event.target !== null) {
      let actionType = e.event.target.getAttribute("data-action-type");
      let recordUpdateData = e.node.data;
      if(actionType == 'deleteUser'){
        this.deleteRecodeIntId = recordUpdateData.IntUserId; 
        this.deleteRecodeId = recordUpdateData.UserId;       
      }
    }
  }
  deleteContactYes(template: TemplateRef<any>) {  
   
    let deleteObject: any = {
      UserIntId : this.deleteRecodeIntId,
      UserId:this.deleteRecodeId,
      StatusType:"DELETED"
     } ;
    this.spinner.show();
    this.userService.userActions(deleteObject).subscribe(data => {
    if (data.Status === 'Success') {
      this.responseInfo = `
      <div class="confirmation-content success">
        <div class="icon confirm text-center">
        <i class="far fa-2x fa-check-circle text-success"></i>
        <h4>Deleted Successfully!!</h4>
        </div>            
      </div> `;   
      this.spinner.hide();
      this.deletemodalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.deletemodalResponseRef.hide();
        this.loadUserList();
      }, 2000);
    }
  }); 
  }
  //#endregion


  
    //#region contact checking
    focusOutEmailTxt(event:any,template: TemplateRef<any>){
      let emailValue = event.target.value;
      if(emailValue != "" && emailValue != undefined){
        this.IsUserExists = false;
        this.userService.isUserExists(emailValue).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let usersCount = JSON.parse(data.Data);
              if(parseInt(usersCount) > 0){ 
                this.IsUserExists = true;            
                this.responseInfo = `<div class="alert alert-danger mb-0">     
                  <h6>Email already exist. Please try with another..!</h6> 
                </div>`; 
                this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
                setTimeout(() => {
                  this.modalResponseRef.hide();
                }, 3000);
              }
          }
        });
      }
    }
     //#endregion


}
