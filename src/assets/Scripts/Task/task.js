       $(function () {

        $("#note_block").click(function () {
                $("#note-block-section").show();      
                $("#note_block").hide();  
        });

         $(".note-block-cancel").click(function () {
                $("#note-block-section").hide();              
                $("#note_block").show();        
          });

         $("#task_block").click(function () {
                $("#task-block-section").show();      
                $("#task_block").hide();  
        });

         $(".task-block-cancel").click(function () {
                $("#task-block-section").hide();              
                $("#task_block").show();        
          });

         $("#meeting_block").click(function () {
                $("#meeting-block-section").show();      
                $("#meeting_block").hide();  
        });

         $(".meeting-block-cancel").click(function () {
                $("#meeting-block-section").hide();              
                $("#meeting_block").show();        
          });

    });
  


$(document).ready(function () { // Script to toggle text when char count is greater than 55 
    var showChar = 35;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('.more').each(function () {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext +
                '</span><span class="morecontent"><span>' + h +
                '</span>&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

//     $(".pop").popover({ trigger: "manual" , html: true, animation:false})
//     .on("mouseenter", function () {
//         var _this = this;
//         $(this).popover("show");
//         $(".popover").on("mouseleave", function () {
//             $(_this).popover('hide');
//         });
//     }).on("mouseleave", function () {
//         var _this = this;
//         setTimeout(function () {
//             if (!$(".popover:hover").length) {
//                 $(_this).popover("hide");
//             }
//         }, 300);
// });   


// $(".vop_accordion .collapse.show").each(function(){
//   $(this).prev(".ac-header").find(".fa").addClass("fa-angle-down").removeClass("fa-angle-right");
// });

 
// $(".vop_accordion .collapse").on('show.bs.collapse', function(){
//   $(this).prev(".ac-header").find(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
// }).on('hide.bs.collapse', function(){
//   $(this).prev(".ac-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
// });


    $("#taskSearchInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#taskTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });

      $("#templateSearchInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#templateTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });

     

          // Script for Search Box
  $('#dropdownSearch').on("keyup",function () {
    var input, filter, ul, li, a, i;
    input = $(this).val();
    filter = input.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("li");
    for (i = 0; i < a.length; i++) {
      txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }    
    }
  });


  $('#myDropdown li').click(function(){
      var childElement = $(this).children("a");
      var $txt = jQuery("#froalaEditorContainer");
        var caretPos = $txt[0].selectionStart;
        var textAreaTxt = $txt.val();
        var txtToAdd = " <p>stuff</p>";
        $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
     // alert($(childElement).text());
});



    // Script for Search Box -- Dashboard
    $('#searchBox_Today').keyup(function () {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("searchBox_Today");
        filter = input.value.toUpperCase();
        ul = document.getElementById("tasksList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByClassName("cust-name")[0];
            if (a != undefined) {
                txtValue = a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    });

    // Script for Search Box
    $('#searchBox_Later').keyup(function () {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("searchBox_Later");
        filter = input.value.toUpperCase();
        ul = document.getElementById("laterTasksList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByClassName("cust-name")[0];
            if (a != undefined) {
                txtValue = a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    })

});

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }
