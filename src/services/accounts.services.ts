import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';


@Injectable()
export class AccountApiService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl;
  nodeBaseUrl = environment.nodebaseUrl;
  // baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/accounts';
  //baseUrl= 'http://outworkapi.useguild.com:9500/admin/v1/accounts';


  AddAccount(Accounts: any): Observable<ApiResponse> {
    //return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/accounts', Accounts);
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts', Accounts);
  }

  getAccounts(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts');
  }

  getaccountslist(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    let searchkey: any;  let searchvalue:any; let searchtype:any;let serverinputObj:any;
    let dateRange:any;
    if(searchcol != "" && (searchcol == 'LastContactedDate' || searchcol == 'SyncDate')){
      console.log(" DATE SEARCH "+JSON.stringify(params.filterModel[searchcol]));
      if(params.filterModel[searchcol].type == "inRange"){
        dateRange = params.filterModel[searchcol].dateFrom+"' AND '"+params.filterModel[searchcol].dateTo;
      }else {
        dateRange = params.filterModel[searchcol].dateFrom;
      }

       searchkey =  dateRange;
       searchvalue = params.filterModel[searchcol].filter;
       searchtype = params.filterModel[searchcol].type;
       let serverinputObj ={
        startvalue:params.startRow,
        limit:pagelimit,
        sortcol:sortcol,
        sortdir:sortdir,
        searchcol:searchcol,
        searchkey:searchkey,
        searchtype:searchtype,
        globalsearch:gSearchValue,
        periodcol:mainTab,
        periodtype:periodType
      };
      
      console.log(" DATE SEARCH "+JSON.stringify(serverinputObj));
    //return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/accounts/list',serverinputObj);  
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/list',serverinputObj); 
    }else {
      searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
      searchvalue = searchcol != "" ? params.filterModel[searchcol].filter:"";
      searchtype = searchcol != "" ? params.filterModel[searchcol].type:"";
      let serverinputObj ={
        startvalue:params.startRow,
        limit:pagelimit,
        sortcol:sortcol,
        sortdir:sortdir,
        searchcol:searchcol,
        searchkey:searchvalue,
        searchtype:searchtype,
        globalsearch:gSearchValue,
        periodcol:mainTab,
        periodtype:periodType
      };
      console.log(" NORMAL SEARCH "+serverinputObj);
    //return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/accounts/list',serverinputObj);  
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/list',serverinputObj); 
    }
  }

  getExactAccounts(term:string): Observable<ApiResponse> {
    //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/filter?term='+term);
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/filter?term='+term+"&act=exact");
  }
  getBasicAccounts(term:string): Observable<ApiResponse> {
    //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/filter?term='+term);
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/filter?term='+term);
  }
  IsAccountExists(name:string): Observable<ApiResponse> {
    //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/isaccountexists?accoutname='+name);
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/isaccountexists?accoutname='+name);
  }
  getAccountsStageInfo(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/stageinfo');
  }

  getAccountActivityCount(id: any): Observable<ApiResponse> {
    //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/activitycount?accountintid=' + id);
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/activitycount?accountintid=' + id);
  }

  getAccountActivityData(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/activitydata?accountintid=' + id);
  }

  getAccountsMasterTypes():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/accounts/mastertypes");
  }

  UpdateAccount(Accounts: any): Observable<ApiResponse> {
    //return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/accounts/update', Accounts);
    return this.http.put<ApiResponse>(this.nodeBaseUrl + 'admin/v1/accounts/update', Accounts);
  }
  DeleteAccount(inputObj: any): Observable<ApiResponse> {
    //return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/delete', inputObj);
    return this.http.put<ApiResponse>(this.nodeBaseUrl + 'admin/v1/contacts/delete', inputObj);
  }
}

