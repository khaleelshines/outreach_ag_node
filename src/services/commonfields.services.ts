import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/model/apiresponse.model';

@Injectable()
export class CommonFieldsService{
    constructor(private http: HttpClient) { }
    baseUrl = environment.baseUrl;
    nodeBaseUrl = environment.nodebaseUrl;

    getCommonFields(typeid:number,term:string):Observable<ApiResponse>{
      //return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/commonfields?typeid='+typeid+'&term='+term);
      return this.http.get<ApiResponse>(this.nodeBaseUrl+'admin/v1/commonfields?typeid='+typeid+'&term='+term);
    }

    checkMasterTypeExistance(inputObject:any):Observable<ApiResponse>{
     // return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/commonfields/checktype',inputObject);
      return this.http.post<ApiResponse>(this.nodeBaseUrl+'admin/v1/commonfields/checktype',inputObject);
    }
}