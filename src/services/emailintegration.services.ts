import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class EmailProcessApiService {

    constructor(private http: HttpClient) { }
    //baseUrl = 'http://localhost:60000/admin/v1/emailintegration';
    //baseUrl = 'http://outworkapi.useguild.com:8600/admin/v1/emailintegration';
    baseUrl = environment.baseUrl;
    setUpEmailForAuthorization(Emails: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/emailintegration/authorization', Emails);
    }
   
    CreateEmailOauthDetails(code:any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/emailintegration/credentials?code='+code);
    }
    
}