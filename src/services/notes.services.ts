import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { Register } from '../model/user.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class NotesApiService {
    constructor(private http: HttpClient) { }
    baseUrl = environment.baseUrl;

    addNotes(Notes: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/notes', Notes);
    }

    getNotes(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/notes?contactIntId=' + id);
    }

    getAllNotes(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/notes/all');
    }

    getNotesByAccountId(id: any): Observable<ApiResponse> {
        return this.http.get<any>(this.baseUrl + 'admin/v1/notes?accountintid='+id).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
    }
}
