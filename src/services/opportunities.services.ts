import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class OpportunityApiService {
  
  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl;
  nodeBaseUrl = environment.nodebaseUrl;

  AddOpportunity(Opportunity:any): Observable<ApiResponse> {
   // return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/opportunity', Opportunity);
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities/add', Opportunity);
  }
  GetOpportunities():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity');
  }

  // // node opportunity call
  // GetOpportunitiesList(inputObj:any):Observable<ApiResponse>{    
  //   return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities',inputObj);
  // }

  // node opportunity call
  GetOpportunitiesList(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    
    let searchkey: any;  let searchvalue:any; let searchtype:any;let dateRange:any; let serverinputObj:any;
    if(searchcol != "" && (searchcol == 'last_activity_date' || searchcol == "next_activity_date" || searchcol == "createddate")){
      if(params.filterModel[searchcol].type == "inRange"){
        dateRange = params.filterModel[searchcol].dateFrom+"' AND '"+params.filterModel[searchcol].dateTo;
      }else {
        dateRange = params.filterModel[searchcol].dateFrom;
      }

      searchkey =  dateRange;
      searchvalue = searchkey;
      //params.filterModel[searchcol].filter;
      searchtype = params.filterModel[searchcol].type;
      serverinputObj ={
        startvalue:params.startRow,
        limit:pagelimit,
        sortcol:sortcol,
        sortdir:sortdir,
        searchcol:searchcol,
        searchkey:searchvalue,
        searchtype:searchtype,
        globalsearch:gSearchValue,
        periodcol:mainTab,
        periodtype:periodType
      };
      return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities',serverinputObj);

    }else{
    let searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
    let searchvalue = searchkey != "" ? searchkey.filter : "";
    let searchtype = searchkey != "" ? searchkey.type : "";
    serverinputObj ={
      startvalue:params.startRow,
      limit:pagelimit,
      sortcol:sortcol,
      sortdir:sortdir,
      searchcol:searchcol,
      searchkey:searchvalue,
      searchtype:searchtype,
      globalsearch:gSearchValue,
      periodcol:mainTab,
      periodtype:periodType
    };
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities',serverinputObj);

    }
    
    }

   // node opportunity info call
   GetOpportunityInfoData(inputObj:any):Observable<ApiResponse>{    
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities/info?id='+inputObj);
  }

  // node opportunity info call
  UpdateOpportunityNxtActionData(inputObj:any):Observable<ApiResponse>{    
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/opportunities/update/nextaction',inputObj);
  }

  GetOpportunitiesWithFileters(inputObj):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/filters',inputObj);
  }
  GetOpportunityActivityCount(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/activitycount?opportunityid=' + id);
  }
  GetOpportunityByOpportunityId(id: any):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/id?opportunityId='+id);
  }
  GetOpportunityByObjectIntId(id: number):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity?objectintid='+id);
  }
  UpdateOpportunity(updateObj:any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/update', updateObj);
  }
}
