import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class SequenceApiService {

    constructor(private http: HttpClient) { }
    // baseUrl = 'http://localhost:60000/admin/v1/sequence';
    // baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/sequence';
    baseUrl = environment.baseUrl;
    nodeBaseUrl = environment.nodebaseUrl;
    getSequences(status): Observable<any> {
      /* return this.http.get<any>(this.baseUrl + 'admin/v1/sequence').pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        })); */
        
        return this.http.get<any>(this.nodeBaseUrl + 'admin/v1/sequence?sts='+status).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
    }

    getSequenceReports(timePeriod): Observable<any> {
        
        return this.http.get<any>(this.baseUrl +'admin/v1/sequence/sequencereport?type='+timePeriod).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
       // return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/sequencereport?type='+timePeriod);
    }

    getSequenceById(SequenceIdInt: number): Observable<ApiResponse> {
        //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/user-sequence/steps?sequenceid=' + SequenceIdInt);
        return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/user-sequence/steps?sequenceid=' + SequenceIdInt);
       // return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/steps?sequenceId=' + SequenceIdInt);
    }

    createSequence(Sequence: any): Observable<ApiResponse> {
        //return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence', Sequence);
        return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence', Sequence);
    }

    getSequenceMasterStepType(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/steptypes');
    }

    getTemplate(mailType: string): Observable<ApiResponse> {
        //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/template?mailType=' + mailType);
        return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/template?mailType=' + mailType);
    }

    createSequenceSteps(SequenceSteps: any, id: any): Observable<ApiResponse> {
        //return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/step?SequenceId=' + id, SequenceSteps);
        return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/step?SequenceId=' + id, SequenceSteps);
    }

    updateSequenceSteps(SequenceUpdateSteps: any): Observable<ApiResponse> {       
        return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/sequence/update/sequencestep', SequenceUpdateSteps);
    }

    getSequenceLinkedToContact(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence?objectId=' + id);
    }

    getSubscribersCount(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/subscribercount?sequenceId=' + id)
    }


    getSequenceDetailsToContact(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/details?sequenceDetailId=' + id);
    }

    sendEmailToContact(emailObject: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/sendmail', emailObject);
    }

    sendManualEmailToContact(emailObject: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/email/sendmail', emailObject);
    }

    deleteSequenceStep(stepId: any, sequenceId: any): Observable<ApiResponse> {
        //return this.http.delete<ApiResponse>(this.baseUrl + 'admin/v1/sequence/step?sequenceId=' + sequenceId + '&stepId=' + stepId);
        return this.http.delete<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/step?sequenceId=' + sequenceId + '&stepId=' + stepId);
    }

    getSequenceStepWithStatus(objectId: any,sequenceId:any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/sequence/steps?objectId=' + objectId+'&sequenceId='+sequenceId);
    }

    getSequenceStepInfo(sequenceId:number, stepId:number):Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/stepinfo?sequenceId=' + sequenceId + '&stepId=' + stepId);
    }

    getUserSequence(userIntId:number,status):Observable<ApiResponse>{
        //return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/user-sequence?id='+userIntId+"&sts="+status);
        return this.http.get<ApiResponse>(this.nodeBaseUrl+'admin/v1/sequence/user-sequence?id='+userIntId+"&sts="+status);
    }
    getUserSequenceSteps(sequenceId:number):Observable<ApiResponse>{
        //return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/user-sequence/steps?sequenceid='+sequenceId);
        return this.http.get<ApiResponse>(this.nodeBaseUrl+'admin/v1/sequence/user-sequence/steps?sequenceid='+sequenceId);
    }

    getSequenceStepsInfo(sequenceId:number):Observable<ApiResponse>{
        //return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/stepsinfo?sequenceid='+sequenceId);
        return this.http.get<ApiResponse>(this.nodeBaseUrl+'admin/v1/sequence/stepsinfo?sequenceid='+sequenceId);
    }

    initiateSequence(sequenceObj:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/initiatesequence', sequenceObj);
    }

    proceedSequence(sequenceObj:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/initiate/sequence', sequenceObj);
        //return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/initiate/sequence', sequenceObj);
    }

    getInitiateSequence(filterObj:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/initiatesequence/search', filterObj);
    }

    getSearchSequence(filterObj:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/search', filterObj);
       // return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/search', filterObj);
    }

    fetchContacts(filterObj1:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/fetchcontacts', filterObj1);
    }

    previewContacts(filterObj:any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/previewcontacts', filterObj);
        //return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/sequence/previewcontacts', filterObj);
    }

    initiatedContactsCount():Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/initiated/contactscount');
        //return this.http.get<ApiResponse>(this.nodeBaseUrl+'admin/v1/sequence/initiated/contactscount');
    }

    getSequenceDetailedReports(timePeriod, sequenceId){
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/sequence/stepreport?type='+timePeriod+'&id=' + sequenceId);
    }
}


