import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { BaseApiService } from './environment.servicebaseurls';
import { environment } from 'src/environments/environment';

@Injectable()
export class TaskApiService {

    constructor(private http: HttpClient) { }
    //  baseUrl = BaseApiService.BaseUrl;
    baseUrl = environment.baseUrl;
    nodeBaseUrl = environment.nodebaseUrl;
    //  baseUrl = 'http://localhost:60000/admin/v1/task';    
    // baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/task';


    getTasks(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task');
    }

    // get tasks node call
    getTasksList(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    let searchkey: any;  let searchvalue:any; let searchtype:any;let dateRange:any; let serverinputObj:any;
    if(searchcol != "" && (searchcol == 'duedate')){
      if(params.filterModel[searchcol].type == "inRange"){
        dateRange = params.filterModel[searchcol].dateFrom+"' AND '"+params.filterModel[searchcol].dateTo;
      }else {
        dateRange = params.filterModel[searchcol].dateFrom;
      }

      searchkey =  dateRange;
      searchvalue = searchkey;
      searchtype = params.filterModel[searchcol].type;
      serverinputObj ={
        startvalue:params.startRow,
        limit:pagelimit,
        sortcol:sortcol,
        sortdir:sortdir,
        searchcol:searchcol,
        searchkey:searchvalue,
        searchtype:searchtype,
        globalsearch:gSearchValue,
        periodcol:mainTab,
        periodtype:periodType
      };
      return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/list',serverinputObj);
    }else{
    searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
    searchvalue = searchkey != "" ? searchkey.filter : "";
    searchtype = searchkey != "" ? searchkey.type : "";
    serverinputObj ={
      startvalue:params.startRow,
      limit:pagelimit,
      sortcol:sortcol,
      sortdir:sortdir,
      searchcol:searchcol,
      searchkey:searchvalue,
      searchtype:searchtype,
      globalsearch:gSearchValue,
      periodcol:mainTab,
      periodtype:periodType
    };
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/list',serverinputObj);
        }
    }

    getTasksWithFilters(inputObj): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/task/filters', inputObj);
    }

    addTask(Tasks: any): Observable<ApiResponse> {       
        return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/add', Tasks);
       // return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/task', Tasks);
    }

    updateTaskInfo(taskdata: any): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/reschedule', taskdata);

       // return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/task/update/taskinfo', taskdata);
    }
    getTaskByObjectId(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task?objectintid=' + id);
    }
    getTaskByAccountId(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task?accountintid=' + id);
    }
    getTaskInfoByTaskId(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/info?id=' + id);
       // return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task/taskinfo?taskid=' + id);
    }
    updateTaskAsCompleted(id: any, taskintid: any): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.nodeBaseUrl + 'admin/v1/task/markascomplete?sequenceDetailId=' + id + "&taskintid=" + taskintid, null);
      //  return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/task/markascomplete?sequenceDetailId=' + id + "&taskintid=" + taskintid, null);
    }

}

