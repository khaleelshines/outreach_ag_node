import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/model/apiresponse.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class templateService{
constructor (private http:HttpClient){}
// baseUrl = BaseApiService.BaseUrl;
baseUrl = environment.baseUrl;
nodeBaseUrl = environment.nodebaseUrl;

getTemplates():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/templates?mailtype=contacttemplate");
}

 // get Template list - node call
 getTemplatesList(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    let searchkey: any;  let searchvalue:any; let searchtype:any;let dateRange:any; let serverinputObj:any;
    if(searchcol != "" && (searchcol == 'createddate')){
      if(params.filterModel[searchcol].type == "inRange"){
        dateRange = params.filterModel[searchcol].dateFrom+"' AND '"+params.filterModel[searchcol].dateTo;
      }else {
        dateRange = params.filterModel[searchcol].dateFrom;
      }

      searchkey =  dateRange;
      searchvalue = searchkey;
      searchtype = params.filterModel[searchcol].type;
      serverinputObj ={
        startvalue:params.startRow,
        limit:pagelimit,
        sortcol:sortcol,
        sortdir:sortdir,
        searchcol:searchcol,
        searchkey:searchvalue,
        searchtype:searchtype,
        globalsearch:gSearchValue,
        periodcol:mainTab,
        periodtype:periodType
      };
      return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/templates/list',serverinputObj);
    }else{
    searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
    searchvalue = searchkey != "" ? searchkey.filter : "";
    searchtype = searchkey != "" ? searchkey.type : "";
    serverinputObj ={
      startvalue:params.startRow,
      limit:pagelimit,
      sortcol:sortcol,
      sortdir:sortdir,
      searchcol:searchcol,
      searchkey:searchvalue,
      searchtype:searchtype,
      globalsearch:gSearchValue,
      periodcol:mainTab,
      periodtype:periodType
    };
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/templates/list',serverinputObj);
        }
    }

postTemplate(templateObj:any):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.nodeBaseUrl+"admin/v1/templates/add",templateObj);
   // return this.http.post<ApiResponse>(this.baseUrl+"admin/v1/templates/template",templateObj);
}

UpdateTemplate(templateObj:any):Observable<ApiResponse>{
  return this.http.put<ApiResponse>(this.nodeBaseUrl+"admin/v1/templates/update",templateObj);
   // return this.http.put<ApiResponse>(this.baseUrl+"admin/v1/templates/updatetemplate",templateObj);
}

deleteTemplate(templateId:string):Observable<ApiResponse>{
    return this.http.delete<ApiResponse>(this.nodeBaseUrl+'admin/v1/templates/delete?templateid='+templateId);
   // return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/templates/deletetemplate?templateid='+templateId);

}
}