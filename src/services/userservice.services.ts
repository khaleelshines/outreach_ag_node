import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { Register } from '../model/user.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class UserService {
  
  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl;
  nodeBaseUrl = environment.nodebaseUrl;
 //baseUrl ='http://localhost:60000/'; 
  // baseUrl = 'http://outworkapi.useguild.com:9500/';


  login(loginPayload: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/user/login', loginPayload);
  }

  nodelogin(loginPayload: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/user/login', loginPayload);
  }

  register(User: Register): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/user/signup', User);
  }

  resetpassword(newpassword: string, token: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/user/reset-password?newpassword=' + newpassword + '&token=' + token, null);
  }

  forgotpassword(loginid: string): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/user/forgot-password?loginId=' + loginid, null);
  }

  addUser(User:any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.nodeBaseUrl + 'admin/v1/users/add', User);
   // return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/user/createuser', User);
  }
  getUsers1(userInfo:any):Observable<ApiResponse>{
    //return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/user/all');
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/user/all');
  }

  isUserExists(email:string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.nodeBaseUrl + 'admin/v1/user/isuserexists?email='+email);
  }

  getUsers():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/user/all');
  }

  getUserInfo():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/user/userinfo');
  }

  userActions(Actions:any):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.nodeBaseUrl+'admin/v1/users/useractions',Actions);
   // return this.http.put<ApiResponse>(this.baseUrl+'admin/v1/user/useractions',Actions);
  }
}
